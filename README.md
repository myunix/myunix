# myunix

## Goals

- [ ] Network
- [ ] Become self-hosting using the Tiny C Compiler
- [ ] Implement support for ELF-Loading
- [x] Implement Userspace and Kernel Seperation
- [x] Implement a Virtual Filesystem Abstraction Layer
- [x] Paging on all supported architectures
- [x] Preemptive multitasking

## Supported Architectures

* i686
* x86_64 / amd64

## Building

### Requirements (debian)

Install the requirements:
```bash
sudo apt-get -y install make curl gpg xz-utils patch gcc g++ libmpfr-dev libmpc-dev grub-common grub-pc-bin xorriso zlib1g zlib1g-dev mtools
```

### Building the system

1. build everything (including a toolchain), this could take a long time
```bash
HOST_ARCH=i686 make -j $(nproc) --output-sync all
```

2. test using qemu
```sh
qemu-system-i386 -cdrom myunix.iso
```

# Licence

Copyright (C) 2019-2020 bauen1 (Jonathan Hettwer)

See COPYING for more details.

Please note that some files or components may have different licences.
