#include <kprintf.h>
#include <stacktrace.h>

#include <assert.h>
#include <boot.h>

noreturn void __assert_failed(const char * restrict msg, const char * restrict file, int line) {
    kprintf("%s:%i assertion failed: '%s'\n", file, line, msg);
    stacktrace_print();
    cpu_halt("assert failed");
}

noreturn void __attribute__((used)) __stack_chk_fail(void) {
    // XXX: consider everything on the stack to be a hot potato and don't touch it
    kprintf("STACK SMASHED!! %s()\n", __func__);
    cpu_halt("stack check failed");
}

/* libtcc1.a compat */
noreturn void __attribute__((used)) abort(void) {
    cpu_halt("abort");
}
