#include <kernel.h>
#include <kheap.h>
#include <list.h>

#include <assert.h>

list_t *list_init(void) {
    return kcalloc(1, sizeof(list_t));
}

void list_free(list_t *list) {
    assert(list->length == 0);
    assert(list->head == NULL);
    assert(list->tail == NULL);
    kfree(list);
}

/* assert that this list is valid */
void assert_list(const list_t *list) {
    size_t l = 0;

    list_node_t *last = NULL;
    list_node_t *prev = NULL;
    list_node_t *curr = list->head;
    while (curr != NULL) {
        assert(curr->prev == prev);
        assert(curr->owner == list);
        // assert(curr->value != NULL);;
        last = curr;
        prev = curr;
        curr = curr->next;
        l++;
    }

    assert(list->length == l);
    assert(list->tail == last);
}

list_node_t *list_node_init(void *value) {
    list_node_t *node = kcalloc(1, sizeof(list_node_t));

    if (node == NULL) {
        return NULL;
    }

    node->value = value;

    return node;
}

void *list_node_free(list_node_t *node) {
    assert(node != NULL);
    assert(node->next == NULL);
    assert(node->prev == NULL);
    assert(node->owner == NULL);

    void *value = node->value;

    kfree(node);

    return value;
}

void list_append(list_t *list, list_node_t *node) {
    assert(list != NULL);
    assert_list(list);
    assert(node != NULL);

    assert(node->next == NULL);
    assert(node->prev == NULL);
    assert(node->owner == NULL);

    node->owner = list;

    if (list->length == 0) {
        list->head = node;
        list->tail = node;
        node->prev = NULL;
        node->next = NULL;
    } else {
        list->tail->next = node;
        node->prev = list->tail;
        list->tail = node;
    }

    list->length++;

    assert_list(list);
}

list_node_t *list_append_value(list_t *list, void *value) {
    list_node_t *node = list_node_init(value);

    if (node == NULL) {
        return NULL;
    }

    list_append(list, node);

    return node;
}

list_node_t *list_remove(list_t *list, list_node_t *node) {
    assert(list != NULL);
    assert_list(list);
    assert(node != NULL);
    assert(node->owner == list);

    if (node == list->head) {
        list->head = node->next;
    }

    if (node == list->tail) {
        list->tail = node->prev;
    }

    if (node->prev != NULL) {
        node->prev->next = node->next;
    }

    if (node->next != NULL) {
        node->next->prev = node->prev;
    }

    node->prev = NULL;
    node->next = NULL;
    node->owner = NULL;

    list->length--;

    assert_list(list);

    return node;
}

void *list_remove_value(list_t *list, void *value) {
    assert(list != NULL);
    assert(value != NULL);

    for list_each(node, list) {
        if (node->value == value) {
            list_remove(list, node);
            list_node_free(node);
            return value;
        }
    }

    return NULL;
}

list_node_t *list_dequeue_node(list_t *list) {
    if (list->head == NULL) {
        return NULL;
    } else {
        return list_remove(list, list->head);
    }
}

void *list_dequeue(list_t *list) {
    list_node_t *node = list_dequeue_node(list);
    if (node == NULL) {
        return NULL;
    }

    void *v = node->value;
    list_node_free(node);
    return v;
}

list_node_t *list_queue(list_t *list, void *value) {
    return list_append_value(list, value);
}
