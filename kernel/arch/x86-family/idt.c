#include "pic.h"

#include <boot.h>
#include <cpu.h>
#include <irq.h>
#include <kernel.h>
#include <kprintf.h>
#include <mem/vmm.h>
#include <scheduler.h>
#include <string.h>
#include <syscalls.h>

#include <stacktrace.h>

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdnoreturn.h>

#define IRQ_MAX_HANDLERS 32
#define IRQ_MAX 16

/* _isr** symbols are defined in boot.s */
/* CPU exceptions */
extern void _isr0(void);
extern void _isr1(void);
extern void _isr2(void);
extern void _isr3(void);
extern void _isr4(void);
extern void _isr5(void);
extern void _isr6(void);
extern void _isr7(void);
extern void _isr8(void);
extern void _isr9(void);
extern void _isr10(void);
extern void _isr11(void);
extern void _isr12(void);
extern void _isr13(void);
extern void _isr14(void);
extern void _isr15(void);
extern void _isr16(void);
extern void _isr17(void);
extern void _isr18(void);
extern void _isr19(void);
extern void _isr20(void);
extern void _isr21(void);
extern void _isr22(void);
extern void _isr23(void);
extern void _isr24(void);
extern void _isr25(void);
extern void _isr26(void);
extern void _isr27(void);
extern void _isr28(void);
extern void _isr29(void);
extern void _isr30(void);
extern void _isr31(void);

/* PIC interrupts */
extern void _isr32(void);
extern void _isr33(void);
extern void _isr34(void);
extern void _isr35(void);
extern void _isr36(void);
extern void _isr37(void);
extern void _isr38(void);
extern void _isr39(void);
extern void _isr40(void);
extern void _isr41(void);
extern void _isr42(void);
extern void _isr43(void);
extern void _isr44(void);
extern void _isr45(void);
extern void _isr46(void);
extern void _isr47(void);

/* syscall */
extern void _isr128(void);

enum gate_type {
    I32_TASK_GATE = 0x5,
    I16_INT_GATE  = 0x6,
    I16_TRAP_GATE = 0x7,
    I32_INT_GATE  = 0xE,
    I32_TRAP_GATE = 0xF,
};

#if defined(__i386__)
typedef struct __attribute__((packed)) {
    uint16_t offset_low;
    uint16_t selector;
    uint8_t zero;
    uint8_t gate_type:4;
    uint8_t storage_segment:1;
    uint8_t dpl:2;
    uint8_t present:1;
    uint16_t offset_high;
} idt32_gate_t;
static_assert(sizeof(idt32_gate_t) == 0x8, "idt_gate_t size is incorrect");
#elif defined(__x86_64__)
typedef struct __attribute__((packed)) {
    uint16_t offset_low;
    uint16_t selector;
    uint8_t ist_offset:2;
    uint8_t zero:6;
    uint8_t gate_type:4;
    uint8_t storage_segment:1;
    uint8_t dpl:2;
    bool present:1;
    uint16_t offset_middle;
    uint32_t offset_high;
    uint32_t reserved;
} idt64_gate_t;
static_assert(sizeof(idt64_gate_t) == 0x10, "idt64_gate_t size is incorrect");
#else
#error "unsupported arch"
#endif

/* XXX: this needs to be mapped in userspace */
#if defined(__i386__)
static __attribute__((section("shared_text"))) idt32_gate_t idt32[256];
#elif defined(__x86_64__)
static __attribute__((section("shared_text"))) idt64_gate_t idt64[256];
#else
#error "unsupported arch"
#endif

#if defined(__i386__)
static void idt32_set_gate(idt32_gate_t *gate, uintptr_t isr, uint16_t selector, enum gate_type gate_type, uint8_t storage_segment, uint8_t dpl, bool present) {
    assert(gate != NULL);
    assert((gate_type & 0xF0) == 0);
    assert((storage_segment & 0xFE) == 0);
    assert((dpl & 0xFC) == 0);
    /* assert(selector) */

    gate->offset_low = (uint16_t)(isr & 0xFFFF);
    gate->selector = selector;
    gate->zero = 0;
    gate->gate_type = gate_type & 0xF;
    gate->storage_segment = storage_segment & 0x1;
    gate->dpl = dpl & 0x3;
    gate->present = present;
    gate->offset_high = (uint16_t)((isr >> 16) & 0xFFFF);
}
#elif defined(__x86_64__)
static void idt64_set_gate(idt64_gate_t *gate, uintptr_t isr, uint16_t selector, uint8_t ist_offset, enum gate_type gate_type, uint8_t dpl, bool present) {
    assert(gate != NULL);
    assert((gate_type & 0xF0) == 0);
    assert((dpl & 0xFC) == 0);

    gate->offset_low = (uint16_t)(isr & 0xFFFF);
    gate->selector = selector;
    gate->ist_offset = ist_offset;
    gate->zero = 0;
    gate->gate_type = gate_type & 0xF;
    gate->storage_segment = 0; // FIXME: ???
    gate->dpl = dpl & 0x3;
    gate->present = present;
    gate->offset_middle = (uint16_t)((isr >> 16) & 0xFFFF);
    gate->offset_high = (uint32_t)((isr >> 32) & 0xFFFFFFFF);
    gate->reserved = 0;
}
#else
#error "unsupported arch"
#endif

#if defined(__i386__)
#define register_isr(n, dpl) idt32_set_gate(&idt32[(n)], (uintptr_t)(void *)_isr##n, 0x8, I32_INT_GATE, 0, (dpl), true)
#elif defined(__x86_64__)
#define register_isr(n, dpl) idt64_set_gate(&idt64[(n)], (uintptr_t)(void *)_isr##n, 0x8, 0, I32_INT_GATE, (dpl), true)
#else
#error "unsupported arch"
#endif

void idt_init(void) {
#if defined(__i386__)
    memset(&idt32, 0, sizeof(idt32));
    struct __attribute__((packed)) {
        uint16_t limit;
        uint32_t base;
    } idtptr = {
        .limit = sizeof(idt32) - 1,
        .base = (uintptr_t)&idt32,
    };
    __asm__ __volatile__("lidt %0" : : "m"(idtptr));
#elif defined(__x86_64__)
    memset(&idt64, 0, sizeof(idt64));
    struct __attribute__((packed)) {
        uint16_t limit;
        uint64_t base;
    } idtptr = {
        .limit = sizeof(idt64) - 1,
        .base = (uintptr_t)&idt64,
    };
    __asm__ __volatile__("lidt %0" : : "m"(idtptr));
#else
#error "unsupported arch"
#endif

    /* CPU exceptions */
    register_isr( 0, 0);
    register_isr( 1, 0);
    register_isr( 2, 0);
    register_isr( 3, 0);
    register_isr( 4, 0);
    register_isr( 5, 0);
    register_isr( 6, 0);
    register_isr( 7, 0);
    register_isr( 8, 0);
    register_isr( 9, 0);
    register_isr(10, 0);
    register_isr(11, 0);
    register_isr(12, 0);
    register_isr(13, 0);
    register_isr(14, 0);
    register_isr(15, 0);
    register_isr(16, 0);
    register_isr(17, 0);
    register_isr(18, 0);
    register_isr(19, 0);
    register_isr(20, 0);
    register_isr(21, 0);
    register_isr(22, 0);
    register_isr(23, 0);
    register_isr(24, 0);
    register_isr(25, 0);
    register_isr(26, 0);
    register_isr(27, 0);
    register_isr(28, 0);
    register_isr(29, 0);
    register_isr(30, 0);
    register_isr(31, 0);

    /* PIC interrupts */
    register_isr(32, 0);
    register_isr(33, 0);
    register_isr(34, 0);
    register_isr(35, 0);
    register_isr(36, 0);
    register_isr(37, 0);
    register_isr(38, 0);
    register_isr(39, 0);
    register_isr(40, 0);
    register_isr(41, 0);
    register_isr(42, 0);
    register_isr(43, 0);
    register_isr(44, 0);
    register_isr(45, 0);
    register_isr(46, 0);
    register_isr(47, 0);

    /* syscall */
    register_isr(128, 3);

    /* remap IRQs */
    pic_init();
}

static irq_handler_t irq_handlers [IRQ_MAX][IRQ_MAX_HANDLERS];

void irq_add_handler(irq_handler_t handler, unsigned int irq) {
    assert(irq < IRQ_MAX);

    for (unsigned int i = 0; i < IRQ_MAX_HANDLERS; i++) {
        if (irq_handlers[irq][i] == NULL) {
            irq_handlers[irq][i] = handler;
            return;
        }
    }

    assert(0 /* we're out of space in irq_handlers */);
}

void irq_del_handler(irq_handler_t handler, unsigned int irq) {
    assert(irq < IRQ_MAX);

    for (unsigned int i = 0; i < IRQ_MAX_HANDLERS; i++) {
        if (irq_handlers[irq][i] == handler) {
            irq_handlers[irq][i] = NULL;
            return;
        }
    }

    assert(0 /* trying to remove non-existant irq handler */);
}

static void irq_isr(registers_t *regs) {
    assert((regs->isr_num >= 32));
    const unsigned int irq = regs->err_num;
    assert(irq < IRQ_MAX);

    /* XXX: we do not want to suddenly jump away from this thread
     * without executing every handler for this irq */
    scheduler_lock();

    bool claimed = false;
    for (unsigned int i = 0; !claimed && (i < IRQ_MAX_HANDLERS); i++) {
        if (irq_handlers[irq][i] != NULL) {
            claimed = irq_handlers[irq][i]();
        }
    }

    pic_send_eoi(irq);

    if (!claimed) {
        kprintf("%s: unhandled irq %u\n", __func__, irq);
    }

    scheduler_unlock();
}

static void dump_regs(const registers_t *regs) {
    kprintf("==== REGISTERS ====\n");
#if defined(__i386__)
    kprintf("pdir:    %#.8x\n", regs->pdir);
    kprintf("gs:      %#.8x fs:   %#.8x\n", regs->gs, regs->fs);
    kprintf("es:      %#.8x ds:   %#.8x\n", regs->es, regs->ds);
    kprintf("edi:     %#.8x esi:  %#.8x\n", regs->edi, regs->esi);
    kprintf("ebp:     %#.8x\n", regs->ebp);
    kprintf("ebx:     %#.8x edx:  %#.8x\n", regs->ebx, regs->edx);
    kprintf("ecx:     %#.8x eax:  %#.8x\n", regs->ecx, regs->eax);
    kprintf("isr_num: %#.8x\n", regs->isr_num);
    kprintf("err_num: %#.8x\n", regs->err_num);
    kprintf("eip:     %#.8x cs:   %#.8x\n", regs->eip, regs->cs);
    kprintf("eflags:  %#.8x\n", regs->eflags);
    kprintf("esp:     %#.8x ss:   %#.8x\n", regs->esp, regs->ss);
#elif defined(__x86_64__)
    kprintf("pdir:    %#.16lx\n", regs->pdir);
    kprintf("rdi:     %#.16lx rsi: %#.16lx\n", regs->rdi, regs->rsi);
    kprintf("rbp:     %#.16lx rcx: %#.16lx\n", regs->rbp, regs->rcx);
    kprintf("rbx:     %#.16lx rax: %#.16lx\n", regs->rbx, regs->rax);
    kprintf("r8:      %#.16lx r9:  %#.16lx\n", regs->r8, regs->r9);
    kprintf("r10:     %#.16lx r11: %#.16lx\n", regs->r10, regs->r11);
    kprintf("r12:     %#.16lx r13: %#.16lx\n", regs->r12, regs->r13);
    kprintf("r14:     %#.16lx r15: %#.16lx\n", regs->r14, regs->r15);
    kprintf("isr_num: %#.16lx\n", regs->isr_num);
    kprintf("err_num: %#.16lx\n", regs->err_num);
    kprintf("rip:     %#.16lx cs:  %#.16lx\n", regs->rip, regs->cs);
    kprintf("rflags:  %#.16lx\n", regs->rflags);
    kprintf("rsp:     %#.16lx ss:  %#.16lx\n", regs->rsp, regs->ss);
#else
#error "unsupported arch"
#endif
    kprintf("==== REGISTERS ====\n");
}

static const char *str_mode(const registers_t *regs) {
    switch (regs->cs & 0x3) {
        case 0x0:
            return "kernel";
        case 0x3:
            return "user";
        default:
            return "(unknown)";
    }
}

static void handle_page_fault(registers_t *regs) {
    assert(regs->isr_num == 14);

    uintptr_t cr2;
    __asm__ __volatile__ ("mov %%cr2, %0" : "=r"(cr2));

    const char *mode = str_mode(regs);

    const char *cause;
    if ((regs->err_num & 0x1) == 0) {
        cause = "non-present page";
    } else {
        cause = "protection violation";
    }

    const char *action;
    if ((regs->err_num & 0x2) == 0) {
        action = "read";
    } else {
        action = "write";
    }

    const char *reserved_bits;
    if ((regs->err_num & 0x8) != 0) {
        reserved_bits = " (reserved bits set in page directory)";
    } else {
        reserved_bits = "";
    }

    const char *cause_instruction;
    if ((regs->err_num & 0x10) != 0) {
        cause_instruction = " (instruction fetch)";
    } else {
        cause_instruction = "";
    }


#if defined(__i386__)
    kprintf("Encountered unhandled page fault in %s mode at eip=%p during %s%s caused by %s%s of address %p (err_num: 0x%.8x)\n", mode,
            (void *)regs->eip,
            action, cause_instruction, cause, reserved_bits, (void *)cr2, regs->err_num);
#elif defined(__x86_64__)
    kprintf("Encountered unhandled page fault in %s mode at eip=%p during %s%s caused by %s%s of address %p (err_num: 0x%.16lx)\n", mode,
            (void *)regs->rip,
            action, cause_instruction, cause, reserved_bits, (void *)cr2, regs->err_num);
#else
#error "unsupported arch"
#endif

    // TODO: dump page (directory)

    dump_regs(regs);
    stacktrace_print();

    cpu_halt("unhandled exception encountered!");
}

static const char *exception_names[] = {
    "division by zero",
    "debug",
    "non-maskable interrupt",
    "breakpoint",
    "into detected overflow",
    "out of bounds exception",
    "invalid opcode",
    "no coprocessor",
    "double fault",
    "coprocessor segment overrun",
    "invalid tss",
    "segment not present",
    "stack segment fault",
    "general protection fault",
    "page fault",
    "reserved",
    "coprocessor result",
    "alignment check",
    "machine check",
    "SIMD floating-point",
    "virtualization",
    "reserved",
    "reserved",
    "reserved",
    "reserved",
    "reserved",
    "reserved",
    "reserved",
    "reserved",
    "reserved",
    "reserved",
    "reserved",
};

static void exception_isr(registers_t *regs) {
    const char *mode = str_mode(regs);

    if (regs->isr_num < 32) {
        kprintf("Encountered unhandled %s exception in %s mode!\n", exception_names[regs->isr_num], mode);
    } else {
        kprintf("Encountered unhandled unknown exception in %s mode!\n", mode);
    }

#if defined(__i386__)
    kprintf("isr_num: %u\n", regs->isr_num);
#else
    kprintf("isr_num: %lu\n", regs->isr_num);
#endif

    dump_regs(regs);

    /* TODO: print stack trace */

    cpu_halt("unhandled exception encountered!");
}

typedef void (*isr_handler_t)(registers_t *regs);
static isr_handler_t isr_handlers [256] = {
    [0] = exception_isr,
    [1] = exception_isr,
    [2] = exception_isr,
    [3] = exception_isr,
    [4] = exception_isr,
    [5] = exception_isr,
    [6] = exception_isr,
    [7] = exception_isr,
    [8] = exception_isr,
    [9] = exception_isr,
    [10] = exception_isr,
    [11] = exception_isr,
    [12] = exception_isr,
    [13] = exception_isr,
    [14] = handle_page_fault,
    [15] = exception_isr,
    [16] = exception_isr,
    [17] = exception_isr,
    [18] = exception_isr,
    [19] = exception_isr,
    [20] = exception_isr,
    [21] = exception_isr,
    [22] = exception_isr,
    [23] = exception_isr,
    [24] = exception_isr,
    [25] = exception_isr,
    [26] = exception_isr,
    [27] = exception_isr,
    [28] = exception_isr,
    [29] = exception_isr,
    [30] = exception_isr,
    [31] = exception_isr,
    [32] = irq_isr,
    [33] = irq_isr,
    [34] = irq_isr,
    [35] = irq_isr,
    [36] = irq_isr,
    [37] = irq_isr,
    [38] = irq_isr,
    [39] = irq_isr,
    [40] = irq_isr,
    [41] = irq_isr,
    [42] = irq_isr,
    [43] = irq_isr,
    [44] = irq_isr,
    [45] = irq_isr,
    [46] = irq_isr,
    [47] = irq_isr,
    [48] = irq_isr,
    [128] = syscall_isr,
};

void __attribute__((used)) handle_isr(registers_t *regs);

void __attribute__((used)) handle_isr(registers_t *regs) {
    uint32_t isr_num = regs->isr_num;
    bool claimed = false;
    if (isr_num < array_size(isr_handlers)) {
        if (isr_handlers[isr_num] != NULL) {
            claimed = true;
            isr_handlers[isr_num](regs);
        }
    }

    if (!claimed) {
        kprintf("Ignoring unhandled interrupt: 0x%.8x\n", isr_num);
        exception_isr(regs);
    }
}
