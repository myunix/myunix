#include <boot.h>
#include <string.h>

#include <assert.h>
#include <stdint.h>

/* see boot.S */
extern void tss_flush(void);

#if defined(__i386__)
typedef struct __attribute__((packed)) {
    uint32_t link;
    uint32_t esp0;
    uint32_t ss0;
    uint32_t esp1;
    uint32_t ss1;
    uint32_t esp2;
    uint32_t ss2;
    uint32_t cr3;
    uint32_t eip;
    uint32_t eflags;
    uint32_t eax;
    uint32_t ecx;
    uint32_t ebx;
    uint32_t esp;
    uint32_t ebp;
    uint32_t esi;
    uint32_t edi;
    uint32_t es;
    uint32_t cs;
    uint32_t ss;
    uint32_t ds;
    uint32_t fs;
    uint32_t gs;
    uint32_t ldt;
    uint32_t trap;
    uint32_t iopb_offset;
} tss32_t;
static_assert(sizeof(tss32_t) == 0x68, "tss32_t size is incorrect");
#elif defined(__x86_64__)
typedef struct __attribute__((packed)) {
    uint32_t reserved0;
    uint64_t rsp[3];
    uint64_t reserved1;
    uint64_t ist[7];
    uint64_t reserved2;
    uint32_t iopb_offset;
} tss64_t;
static_assert(sizeof(tss64_t) == 0x68, "tss64_t size is incorrect");
#else
#error "unsupported arch"
#endif

typedef struct __attribute__((packed)) gdt_entry {
    uint16_t limit_low;
    uint16_t base_low;
    uint8_t base_middle;
    uint8_t access;
    uint8_t limit_high:4;
    uint8_t other:4;
    uint8_t base_high;
} gdt_entry_t;
static_assert(sizeof(gdt_entry_t) == sizeof(uint64_t), "gdt_entry_t size");

/* XXX: the TSS needs to be mapped in userspace */
#if defined(__i386__)
static __attribute__((section("shared_text"))) tss32_t tss32;
#elif defined(__x86_64__)
static __attribute__((section("shared_text"))) tss64_t tss64;
#else
#error "unsupported arch"
#endif

enum gdt_flags {
    GDT_FLAG_X86_64_CODE = 0x2,
    GDT_FLAG_32_PMODE    = 0x4,
    GDT_FLAG_4KIB_BLOCKS = 0x8,
};

enum gdt_access {
    GDT_ACCESS_ACCESSED  = 0x01, /* set when the gdt is accessed */
    GDT_ACCESS_RW        = 0x02,
    GDT_ACCESS_DC        = 0x04,
    GDT_ACCESS_EX        = 0x08,
    GDT_ACCESS_S         = 0x10,
    GDT_ACCESS_PRIV_USER = 0x60, /* ... */
    GDT_ACCESS_PRESENT   = 0x80, /* is the gdt valid */
};

#define GDT_ENTRY(base, limit, access, flags) \
    { \
        (uint16_t)((limit) & 0xFFFF), \
        (uint16_t)((base) & 0xFFFF), \
        (uint8_t)(((base) & 0xFF0000) >> 16), \
        (uint8_t)((access) & 0xFF), \
        (uint8_t)(((limit) & 0xF0000) >> 16), \
        (uint8_t)((flags) & 0xF), \
        (uint8_t)(((base) & 0xFF000000) >> 24) \
    }

__attribute__((section("shared_text"))) gdt_entry_t gdt[] = {
    /* XXX: we setup all gdts as accessed, so userspace doesn't have to write to them and we can map them read-only */

    /* 0: NULL */
    GDT_ENTRY(0, 0, 0, 0),

#if defined(__i386__)
    /* 1: kernel code */
    GDT_ENTRY(0, 0xFFFFF, GDT_ACCESS_ACCESSED | GDT_ACCESS_RW | GDT_ACCESS_EX | GDT_ACCESS_S | GDT_ACCESS_PRESENT, GDT_FLAG_4KIB_BLOCKS | GDT_FLAG_32_PMODE),

    /* 2: kernel data */
    GDT_ENTRY(0, 0xFFFFF, GDT_ACCESS_ACCESSED | GDT_ACCESS_RW | GDT_ACCESS_S | GDT_ACCESS_PRESENT, GDT_FLAG_4KIB_BLOCKS | GDT_FLAG_32_PMODE),

    /* 3: user code */
    GDT_ENTRY(0, 0xFFFFF, GDT_ACCESS_ACCESSED | GDT_ACCESS_RW | GDT_ACCESS_EX | GDT_ACCESS_S | GDT_ACCESS_PRIV_USER | GDT_ACCESS_PRESENT, GDT_FLAG_4KIB_BLOCKS | GDT_FLAG_32_PMODE),

    /* 4: user data */
    GDT_ENTRY(0, 0xFFFFF, GDT_ACCESS_ACCESSED | GDT_ACCESS_RW | GDT_ACCESS_S | GDT_ACCESS_PRIV_USER | GDT_ACCESS_PRESENT, GDT_FLAG_4KIB_BLOCKS | GDT_FLAG_32_PMODE),

    /* 5: TSS */
    GDT_ENTRY(0, 0, 0, 0),
#elif defined(__x86_64__)
    /* 1: kernel code */
    GDT_ENTRY(0, 0xFFFFF, GDT_ACCESS_ACCESSED | GDT_ACCESS_RW | GDT_ACCESS_EX | GDT_ACCESS_S | GDT_ACCESS_PRESENT, GDT_FLAG_4KIB_BLOCKS | GDT_FLAG_X86_64_CODE),

    /* 2: kernel data */
    GDT_ENTRY(0, 0xFFFFF, GDT_ACCESS_ACCESSED | GDT_ACCESS_RW | GDT_ACCESS_S | GDT_ACCESS_PRESENT, GDT_FLAG_4KIB_BLOCKS | GDT_FLAG_X86_64_CODE),

    /* 3: user code */
    GDT_ENTRY(0, 0xFFFFF, GDT_ACCESS_ACCESSED | GDT_ACCESS_RW | GDT_ACCESS_EX | GDT_ACCESS_S | GDT_ACCESS_PRIV_USER | GDT_ACCESS_PRESENT, GDT_FLAG_4KIB_BLOCKS | GDT_FLAG_X86_64_CODE),

    /* 4: user data */
    GDT_ENTRY(0, 0xFFFFF, GDT_ACCESS_ACCESSED | GDT_ACCESS_RW | GDT_ACCESS_S | GDT_ACCESS_PRIV_USER | GDT_ACCESS_PRESENT, GDT_FLAG_4KIB_BLOCKS | GDT_FLAG_X86_64_CODE),

    /* 5: TSS */
    GDT_ENTRY(0, 0, 0, 0),

    /* 6: TSS continued */
    GDT_ENTRY(0, 0, 0, 0),
#else
#error "unsupported arch"
#endif
};

static void __attribute__((nonnull)) gdt_set(gdt_entry_t *gdt, uint64_t base, uint32_t limit, enum gdt_access access, enum gdt_flags flags) {
    assert(gdt != NULL);
    assert((limit & 0xFFF00000) == 0);
    assert((flags & 0xF0) == 0);

    gdt->limit_low   = (uint16_t)(limit & 0xFFFF);
    gdt->base_low    = (uint16_t)(base & 0xFFFF);
    gdt->base_middle = (uint8_t)((base & 0xFF0000) >> 16);
    gdt->access      = (uint8_t)(access & 0xFF);
    gdt->limit_high  = (uint8_t)((limit & 0xF0000) >> 16);
    gdt->other       = (uint8_t)(flags & 0xF);
    gdt->base_high   = (uint8_t)((base & 0xFF000000) >> 24);
}

size_t gdt_size_minus_one = sizeof(gdt) - 1;

void tss_init(void) {
#if defined(__i386__)
    memset(&tss32, 0, sizeof(tss32));
    tss32.ss0 = 0x10; /* kernel data segment */
    tss32.esp0 = 0;
    tss32.iopb_offset = sizeof(tss32); /* we don't use the io bitmap */
    tss32.cs = 0x0b; /* kernel code segment + ring 3 */
    tss32.ss = 0x13; /* ^ */
    tss32.ds = 0x13; /* ^ */
    tss32.es = 0x13; /* ^ */
    tss32.fs = 0x13; /* ^ */
    tss32.gs = 0x13; /* ^ */

    gdt_set(&gdt[5], (uintptr_t)&tss32, sizeof(tss32), GDT_ACCESS_ACCESSED | GDT_ACCESS_EX | GDT_ACCESS_PRESENT, GDT_FLAG_4KIB_BLOCKS);

    /* flush the tss */
    __asm__ __volatile__("ltr %w0" : : "r"((uint32_t)0x28));
#elif defined(__x86_64__)
    memset(&tss64, 0, sizeof(tss64));

    tss64.rsp[0] = 0;
    tss64.iopb_offset = sizeof(tss64); /* we don't use the io bitmap */

    memset(&gdt[5], 0, sizeof(gdt_entry_t));
    memset(&gdt[6], 0, sizeof(gdt_entry_t));

    /* if confused, look at the intel manual volume 3 7.2.3 */
    uintptr_t tss64_addr = (uintptr_t)&tss64;
    gdt_set(&gdt[5], tss64_addr & 0xFFFFFFFF, sizeof(tss64), GDT_ACCESS_ACCESSED | GDT_ACCESS_EX | GDT_ACCESS_PRESENT, GDT_FLAG_4KIB_BLOCKS);
    gdt_set(&gdt[6], (tss64_addr >> (32 + 16)) & 0xFFFF, (tss64_addr >> 32) & 0xFFFF, 0, 0);

    /* flush the tss */
    __asm__ __volatile__("ltr %w0" : : "r"((uint32_t)0x28));
#else
#error "unsupported arch"
#endif
}

uintptr_t __attribute__((pure)) tss_get_kstack(void) {
#if defined(__i386__)
    return tss32.esp0;
#elif defined(__x86_64__)
    return tss64.rsp[0];
#else
#error "unsupported arch"
#endif
}

void tss_set_kstack(uintptr_t stack) {
#if defined(__i386__)
    tss32.esp0 = stack;
#elif defined(__x86_64__)
    tss64.rsp[0] = stack;
#else
#error "unsupported arch"
#endif
}
