#include "pic.h"
#include "cpu.h"

#include <stdint.h>

#define PIC1_COMMAND ((uint8_t) 0x20)
#define PIC1_DATA ((uint8_t) 0x21)
#define PIC2_COMMAND ((uint8_t) 0xA0)
#define PIC2_DATA ((uint8_t) 0xA1)

#define PIC_EOI 0x20

void pic_init(void) {
    /* remap PIC interrupts */

    cpu_outb(PIC1_COMMAND, 0x11);
    cpu_outb(PIC2_COMMAND, 0x11);

    cpu_outb(PIC1_DATA, 0x20); /* map IRQ0-7 to ISR32-39 */
    cpu_outb(PIC2_DATA, 0x28); /* map IRQ8-15 to ISR40-47 */

    /* cascade mode */
    cpu_outb(PIC1_DATA, 0x04);
    cpu_outb(PIC2_DATA, 0x02);

    cpu_outb(PIC1_DATA, 0x01);
    cpu_outb(PIC2_DATA, 0x01);

    /* unmask all IRQs */
    cpu_outb(PIC1_DATA, 0x00);
    cpu_outb(PIC2_DATA, 0x00);
}

void pic_send_eoi(unsigned int irq) {
    if (irq >= 8) {
        cpu_outb(PIC2_COMMAND, PIC_EOI);
    }

    cpu_outb(PIC1_COMMAND, PIC_EOI);
}
