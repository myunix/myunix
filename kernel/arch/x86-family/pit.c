#include "cpu.h"

#include <boot.h>
#include <irq.h>
#include <kprintf.h>
#include <scheduler.h>
#include <thread.h>
#include <timer.h>

#include <stdint.h>

#define PIT_A ((uint16_t) 0x40)
#define PIT_B ((uint16_t) 0x41)
#define PIT_C ((uint16_t) 0x42)
#define PIT_CMD ((uint16_t) 0x43)
#define PIT_CLOCKRATE 1193182

#define PIT_CHANNEL_0 ((uint8_t) 0x00)
#define PIT_ACCESS_LOHI ((uint8_t) 0x30)
#define PIT_OPMODE_SQUARE_WAVE ((uint8_t) 0x06)

static void pit_set_timer(unsigned int freq) {
    const unsigned int divisor = PIT_CLOCKRATE / freq;
    cpu_outb(PIT_CMD, PIT_CHANNEL_0 | PIT_ACCESS_LOHI | PIT_OPMODE_SQUARE_WAVE);
    cpu_outb(PIT_A, divisor & 0xFF);
    cpu_outb(PIT_A, (divisor >> 8) & 0xFF);
}

static bool pit_irq0(void) {
    scheduler_timer_wakeup();
    return true;
}

void pit_init(unsigned int freq) {
    irq_add_handler(pit_irq0, 0);
    pit_set_timer(freq);
}
