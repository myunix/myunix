#ifndef __ARCH_X86_FAMILY_PIC_H
#define __ARCH_X86_FAMILY_PIC_H 1

void pic_init(void);
void pic_send_eoi(unsigned int irq);

#endif
