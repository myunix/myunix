#ifndef __ARCH_X86_FAMILY_CPU_H
#define __ARCH_X86_FAMILY_CPU_H 1

#include <stdbool.h>
#include <stdint.h>

/* see boot.S isr_common_stub for the layout of the registers */
#if defined(__i386__)
typedef struct __attribute__((packed)) {
    uint32_t pdir;

    uint32_t gs;
    uint32_t fs;
    uint32_t es;
    uint32_t ds;

    uint32_t edi;
    uint32_t esi;
    uint32_t ebp;
    uint32_t ebx;
    uint32_t edx;
    uint32_t ecx;
    uint32_t eax;

    uint32_t isr_num;
    uint32_t err_num;

    uint32_t eip;
    uint32_t cs;
    uint32_t eflags;
    uint32_t esp;
    uint32_t ss;
} registers_t;
#elif defined(__x86_64__)
typedef struct __attribute__((packed)) {
    uint64_t pdir;
    /* TODO: gs, fs, es, ds, ... */
    uint64_t rdi;
    uint64_t rsi;
    uint64_t rbp;
    uint64_t rcx;
    uint64_t rbx;
    uint64_t rax;
    uint64_t r8;
    uint64_t r9;
    uint64_t r10;
    uint64_t r11;
    uint64_t r12;
    uint64_t r13;
    uint64_t r14;
    uint64_t r15;

    uint64_t isr_num;
    uint64_t err_num;

    uint64_t rip;
    uint64_t cs;
    uint64_t rflags;
    uint64_t rsp;
    uint64_t ss;
} registers_t;
#else
#error "unsupported arch"
#endif

/* see boot.S __switch_thread */
#if defined(__i386__)
typedef struct __attribute__((packed)) {
    uintptr_t edi;
    uintptr_t esi;
    uintptr_t ebx;
    uintptr_t ebp;
    uintptr_t eip;
} thread_frame_t;
#elif defined(__x86_64__)
typedef struct __attribute__((packed)) {
    uintptr_t r15;
    uintptr_t r14;
    uintptr_t r13;
    uintptr_t r12;
    uintptr_t rbx;
    uintptr_t rbp;
    uintptr_t rip;
} thread_frame_t;
#else
#error "unsupported arch"
#endif

/* I/O helpers */
static inline __attribute__((always_inline)) void cpu_outb(uint16_t port, uint8_t value) {
    __asm__ volatile ("outb %0, %1"
            :
            : "a"(value), "dN" (port)
            : "memory"
            );
}

static inline __attribute__((always_inline)) uint8_t cpu_inb(uint16_t port) {
    uint8_t value;
    __asm__ volatile ("inb %1, %0"
            : "=a" (value)
            : "dN" (port)
            : "memory"
            );
    return value;
}

static inline __attribute__((always_inline)) void cpu_outw(uint16_t port, uint16_t value) {
    __asm__ volatile ("outw %0, %1"
            :
            : "a" (value), "dN" (port)
            : "memory"
            );
}

static inline __attribute__((always_inline)) uint16_t cpu_inw(uint16_t port) {
    uint16_t value;
    __asm__ volatile ("inw %1, %0"
            : "=a" (value)
            : "dN" (port)
            : "memory"
            );
    return value;
}

static inline __attribute__((always_inline)) void cpu_outl(uint16_t port, uint32_t value) {
    __asm__ volatile ("outl %0, %1"
            :
            : "a" (value), "dN" (port)
            : "memory"
            );
}

static inline __attribute__((always_inline)) uint32_t cpu_inl(uint16_t port) {
    uint32_t value;
    __asm__ volatile ("inl %1, %0"
            : "=a" (value)
            : "dN" (port)
            : "memory"
            );
    return value;
}

/* implemented in boot.S */
extern void cpu_load_page_directory(uint32_t page_directory_physical);
extern void cpu_enable_paging(void);

/* invalidates the tlb */
static inline __attribute((always_inline)) void cpu_invalidate_page(uintptr_t addr) {
    __asm__ volatile ("invlpg (%0)"
            :
            : "r"(addr)
            : "memory");
}

/* returns true if interrupts where enabled */
static inline bool cpu_disable_interrupts(void) {
#if defined(__i386__)
    uint32_t eflags;
    __asm__ __volatile__(
            "pushf\n"
            "pop %0\n"
            "cli\n"
            : "=r"(eflags));

    return eflags & (1 << 9);
#elif defined(__x86_64__)
    uint64_t eflags;
    __asm__ __volatile__(
            "pushf\n"
            "pop %0\n"
            "cli\n"
            : "=r"(eflags));
    return eflags & (1 << 9);
#else
#error "unsupported arch"
#endif
}

static inline void cpu_enable_interrupts(void) {
    /* since interrupts are only enabled after the next instruction
     * we add a nop operation to ensure interrupts will be enabled, even if
     * this is inlined and the next instruction is a 'hlt' */
    __asm__ __volatile__("sti\nnop\nnop");
}

#endif
