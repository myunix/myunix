#include <assert.h>

#include <kernel.h>
#include <mem/vmm.h>

#include <paging.h>

void kernel_early_vmm_map_direct(uintptr_t addr, bool write) {
    return kernel_early_vmm_map(addr, addr, true, write);
}

void kernel_early_vmm_map_direct_region(uintptr_t start, size_t size, bool write) {
    assert(IS_ALIGNED(start, PAGE_SIZE) && IS_ALIGNED(size, PAGE_SIZE));
    for (size_t i = 0; i < size; i += PAGE_SIZE) {
        kernel_early_vmm_map_direct(start + i, write);
    }
}

void vmm_map_region(page_directory_t *pdir, uintptr_t vaddr, uintptr_t paddr, size_t size, bool present, bool write, bool user, unsigned int extra) {
    assert(IS_ALIGNED(vaddr, PAGE_SIZE));
    assert(IS_ALIGNED(paddr, PAGE_SIZE));
    assert(IS_ALIGNED(size, PAGE_SIZE));
    for (size_t i = 0; i < size; i += PAGE_SIZE) {
        vmm_map_page(pdir, vaddr + i, paddr + i, present, write, user, extra);
    }
}

/* this can be used to turn any physical address (and size) into a pointer */
void *vmmk_map_region_random(uintptr_t paddr, size_t size, bool write) {
    assert(IS_ALIGNED(paddr, PAGE_SIZE));
    assert(IS_ALIGNED(size, PAGE_SIZE));

    const uintptr_t vaddr = vmmk_find_free_region(size);

    if (vaddr == 0) {
        return NULL;
    }

    for (size_t i = 0; i < size; i += PAGE_SIZE) {
        vmmk_map_page(vaddr + i, paddr + i, true, write);
    }

    return (void *)vaddr;
}

void vmmk_unmap_region(uintptr_t vaddr, size_t size) {
    assert(IS_ALIGNED(vaddr, PAGE_SIZE));
    assert(IS_ALIGNED(size, PAGE_SIZE));

    for (size_t i = 0; i < size; i += PAGE_SIZE) {
        vmmk_unmap_page(vaddr + size);
    }
}
