#include "cpu.h"

#include <boot.h>
#include <kprintf.h>
#include <mem/pmm.h>
#include <mem/vmm.h>
#include <kernel.h>
#include <kheap.h>
#include <string.h>

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>

static_assert(PAGE_SIZE == BLOCK_SIZE, "code hasn't been tested with PAGE_SIZE != BLOCK_SIZE");

page_directory_entry_t __kernel_real_page_directory[1024] __attribute__((aligned(PAGE_SIZE)));

page_directory_real_t __kernel_directory_real = {
    .real_map = {},
    .map = {}
};

page_directory_t __kernel_directory = {
    .real = &__kernel_directory_real,
    .physical_address = (uintptr_t)&__kernel_directory_real,
    .__ref = KREF_STATIC_INIT,
};
page_directory_t *kernel_directory = &__kernel_directory;

page_directory_t *page_directory_init(void) {
    page_directory_t * const pdir = kcalloc(1, sizeof(page_directory_t));
    if (pdir == NULL) {
        return NULL;
    }

    const size_t real_size = ALIGN(sizeof(page_directory_real_t), BLOCK_SIZE);

    const uintptr_t phys = pmm_alloc(real_size);
    if (phys == 0) {
        kfree(pdir);
        return NULL;
    }

    pdir->physical_address = phys;

    page_directory_real_t * const real = vmmk_map_region_random(phys, real_size, true);
    if (real == NULL) {
        pmm_free(phys, real_size);
        kfree(pdir);
        return NULL;
    }

    memset(real, 0, real_size);

    pdir->real = real;

    kref_init(&pdir->__ref);

    return pdir;
}

void page_directory_up(page_directory_t *pdir) {
    kref_up(&pdir->__ref);
}

static void page_directory_destroy(void *_pdir) {
    page_directory_t * const pdir = _pdir;

    kfree(pdir);
}

void page_directory_down(page_directory_t *pdir) {
    kref_down(&pdir->__ref, page_directory_destroy, pdir);
}

static void page_debug(const uintptr_t vaddr, const page_t *page);

/* convert table and page index into the virtual address, you're responsible for ensuring
 * table_i and page_i are sane ! */
static __attribute__((const)) uintptr_t table_index_page_index_to_vaddr(size_t table_i, size_t page_i) {
    return ((uintptr_t)table_i << 22) | ((uintptr_t)page_i << 12);
}

__attribute__((pure)) page_table_t *pdir_get_table(page_directory_t *pdir, uintptr_t vaddr) {
    assert(pdir != NULL);
    uintptr_t index = vaddr >> 22;
    return pdir->real->map[index];
}

__attribute__((const)) page_t *get_page(page_table_t *table, uintptr_t vaddr) {
    return &table->pages[(vaddr >> 12) & 0x3FF];
}

static page_table_t *kernel_page_directory_early_get_table(size_t i) {
    return (void *)(kernel_directory->real->real_map[i].phys * PAGE_SIZE);
}

/* allocate all required memory for the kernel page directory */
void vmm_init(void) {
    for (size_t i = 0; i < array_size(kernel_directory->real->real_map); i++) {
        const uintptr_t phys = pmm_alloc(sizeof(page_table_t));
        assert(phys != 0);
        memset((void *)phys, 0, sizeof(page_table_t));
        kernel_directory->real->map[i] = (void *)phys;
        kernel_directory->real->real_map[i] = (page_directory_entry_t){
            .present = true,
            .write = true,
            .user = false,
            .write_through = true,
            .cache_disable = true,
            .accessed = true,
            .zero = 0,
            .pat = 0,
            .global = false,
            .avail = 0,
            .phys = phys / PAGE_SIZE,
        };
    }

    /* setup 0 page */
    kernel_directory->real->map[0]->pages[0] = (page_t){
        .present = false,
        .write = true,
        .user = false,
        .write_through = true,
        .cache_disable = true,
        .accessed = true,
        .dirty = true,
        .global = true,
        .avail = 0,
        .phys = 0,
    };
}

/* map a page into kernel space while paging is still disabled (ie. assume that the
 * kernel page directory isn't active and access all tables by their physical address) */
void kernel_early_vmm_map(uintptr_t vaddr, uintptr_t paddr, bool present, bool write) {
    assert(IS_ALIGNED(vaddr, PAGE_SIZE));
    assert(IS_ALIGNED(paddr, PAGE_SIZE));
    assert(pmm_is_used(paddr));

    const uintptr_t table_index = vaddr >> 22;

    page_table_t * const table = kernel_page_directory_early_get_table(table_index);
    page_t * const page = get_page(table, vaddr);
    page_t new_page = (page_t){
        .present = present,
        .write = write,
        .user = false,
        .write_through = false,
        .cache_disable = false,
        .accessed = true,
        .dirty = false,
        .global = false,
        .avail = 0,
        .phys = paddr / PAGE_SIZE,
    };

    if ((page->__raw != 0) && (page->__raw != new_page.__raw)) {
        kprintf("WARN: early map override:\n");
        kprintf("old:");
        page_debug(vaddr, page);
        kprintf("new:");
        page_debug(vaddr, &new_page);
    }

    *page = new_page;
}

static uintptr_t kernel_early_vmm_first_free(void) {
    for (size_t table_i = 0; table_i < array_size(kernel_directory->real->map); table_i++) {
        const page_table_t *table = kernel_page_directory_early_get_table(table_i);
        for (size_t page_i = 0; page_i < array_size(table->pages); page_i++) {
            const page_t *page = &table->pages[page_i];
            if (page->__raw == 0) {
                return table_index_page_index_to_vaddr(table_i, page_i);
            }
        }
    }

    return 0;
}

/* map all page tables into kernel space (at some random point) */
static void kernel_early_vmm_map_tables(void) {
    for (size_t i = 0; i < array_size(kernel_directory->real->map); i++) {
        const uintptr_t phys = kernel_directory->real->real_map[i].phys * PAGE_SIZE;
        const uintptr_t vaddr = kernel_early_vmm_first_free();
        assert(vaddr != 0);
        kernel_early_vmm_map(vaddr, phys, true, true);
        kernel_directory->real->map[i] = (void *)vaddr;
    }
}

/**/

uintptr_t vmm_unmap_page(page_directory_t *pdir, uintptr_t vaddr) {
    page_table_t *table = pdir_get_table(pdir, vaddr);
    page_t *page = get_page(table, vaddr);

    uintptr_t old_phys = page->phys;
    page_t new_page = {
        .present = false,
        .write = false,
        .user = false,
        .write_through = false,
        .cache_disable = false,
        .accessed = false,
        .dirty = false,
        .global = false,
        .avail = 0,
        .phys = 0,
    };
    *page = new_page;
    return old_phys * PAGE_SIZE;
}

void vmmk_map_page(uintptr_t vaddr, uintptr_t paddr, bool present, bool write) {
    page_table_t * const table = pdir_get_table(kernel_directory, vaddr);
    page_t * const page = get_page(table, vaddr);

    assert(page->__raw == 0);

    *page = (page_t){
        .present = present,
            .write = write,
            .user = false,
            .write_through = false,
            .cache_disable = false,
            .accessed = false,
            .dirty = false,
            .global = false,
            .avail = 0,
            .phys = paddr / PAGE_SIZE,
    };
}

void vmm_map_page(page_directory_t *pdir, uintptr_t vaddr, uintptr_t paddr, bool present, bool write, bool user, unsigned int extra) {
    assert(pdir != kernel_directory);

    page_table_t *table = pdir_get_table(pdir, vaddr);

    if (table == NULL) {
        const uintptr_t phys = pmm_alloc(sizeof(page_table_t));
        assert(phys != 0);

        const size_t i = vaddr >> 22;

        pdir->real->real_map[i] = (page_directory_entry_t){
            .present = true,
            .write = true,
            .user = true,
            .write_through = true,
            .cache_disable = true,
            .accessed = true,
            .zero = 0,
            .pat = 0,
            .global = false,
            .avail = 0,
            .phys = phys / PAGE_SIZE,
        };

        table = vmmk_map_region_random(phys, sizeof(page_table_t), true);
        memset(table, 0, sizeof(page_table_t));

        pdir->real->map[i] = table;
    }

    page_t *page = get_page(table, vaddr);
    assert(page->__raw == 0);
    assert(extra <= 3);

    *page = (page_t){
        .present = present,
        .write = write,
        .user = user,
        .write_through = false,
        .cache_disable = false,
        .accessed = false,
        .dirty = false,
        .global = false,
        .avail = extra,
        .phys = paddr / PAGE_SIZE,
    };
}

/* 0 = error */
static __attribute__((pure)) uintptr_t page_directory_find_region(page_directory_t *pdir, size_t size, bool dma) {
    assert(pdir != NULL);
    assert(IS_ALIGNED(size, PAGE_SIZE));

    size_t region_ti = 0;
    size_t region_pi = 0;
    size_t region_size = 0;
    bool need_region = true;

    for (size_t table_i = 0; table_i < array_size(pdir->real->map); table_i++) {
        if (need_region) {
            region_ti = table_i;
        }

        const page_table_t * const table = pdir->real->map[table_i];

        for (size_t page_i = 0; page_i < array_size(table->pages); page_i++) {
            /* avoid special NULL page */
            if ((table_i == 0) && (page_i == 0)) {
                continue;
            }

            if (need_region) {
                region_pi = page_i;
            }

            const page_t * const page = &table->pages[page_i];

            const uintptr_t vaddr = table_index_page_index_to_vaddr(table_i, page_i);

            if ((page->__raw == 0) && (!dma || !pmm_is_used(vaddr))) {
                need_region = false;
                region_size += PAGE_SIZE;

                if (region_size >= size) {
                    return table_index_page_index_to_vaddr(region_ti, region_pi);
                }
            } else {
                need_region = true;
                region_size = 0;
            }
        }
    }

    /* out of memory */
    return 0;
}

__attribute__((pure)) uintptr_t vmmk_find_free_region(size_t size) {
    return page_directory_find_region(kernel_directory, size, false);
}

__attribute__((pure)) uintptr_t page_directory_find_free_region(page_directory_t *pdir, size_t size) {
    return page_directory_find_region(pdir, size, false);
}

/* virtual memory management kernel */

uintptr_t vmmk_unmap_page(uintptr_t vaddr) {
    assert(IS_ALIGNED(vaddr, PAGE_SIZE));
    uintptr_t v = vmm_unmap_page(kernel_directory, vaddr);
    cpu_invalidate_page(vaddr);
    return v;
}

static_assert(PAGE_SIZE == BLOCK_SIZE, "vmmk_find_dma_region does not work correctly if PAGE_SIZE != BLOCK_SIZE");
__attribute__((pure)) uintptr_t vmmk_find_dma_region(size_t size) {
    return page_directory_find_region(kernel_directory, size, true);
}

__attribute__((pure)) uintptr_t vmmk_get_phys(uintptr_t vaddr) {
    assert(vaddr != 0); /* probably a bug */
    assert(IS_ALIGNED(vaddr, PAGE_SIZE));

    page_table_t *table = pdir_get_table(kernel_directory, vaddr);
    page_t *page = get_page(table, vaddr);

    if (page->present) {
        return page->phys * PAGE_SIZE;
    }

    return 0;
}

/* miscellaneous helpers */
static void page_debug(const uintptr_t vaddr, const page_t *page) {
    kprintf(" %#.8x => %#.8x (%c%c%c%c%c%c%c%c%c avail: %#.1x)\n",
        vaddr, page->__raw,
        page->present ? 'p' : '-',
        page->write ? 'w' : 'r',
        page->user ? 'u' : 'k',
        page->write_through ? 'W' : '-',
        page->cache_disable ? 'C' : '-',
        page->accessed ? 'a' : '-',
        page->dirty ? 'd' : '-',
        page->pat ? 'B' : '-',
        page->global ? 'G' : '-',
        (unsigned int)page->avail);

}

static void page_table_debug(const uintptr_t base, const page_table_t *table) {
    for (size_t j = 0; j < array_size(table->pages); j++) {
        const page_t *page = &table->pages[j];
        if (page->present) {
            const uintptr_t vaddr = base + j * PAGE_SIZE;
            page_debug(vaddr, page);
        }
    }
}

void page_directory_debug(const page_directory_t *pdir) {
    kprintf("---- page directory (virt: %p, phys: %p) ----\n", pdir, (void *)pdir->physical_address);
    for (size_t i = 0; i < array_size(pdir->real->map); i++) {
        const page_table_t *table = pdir->real->map[i];
        if (table == NULL) {
            continue;
        }

        const uintptr_t table_vaddr = i << 22;
        const page_directory_entry_t *entry = &pdir->real->real_map[i];
        kprintf("%#.8x => %#.8x (%c%c%c%c%c%c)\n", table_vaddr, entry->__raw,
                entry->present ? 'p' : '-',
                entry->write ? 'w' : 'r',
                entry->user ? 'u' : 'k',
                entry->write_through ? 'W' : '-',
                entry->cache_disable ? 'C' : '-',
                entry->zero ? 'B' : '-');
        if (entry->present) {
            page_table_debug(table_vaddr, table);
        }
    }
    kprintf("---- end ----\n");
}

void vmm_finalize(void) {
    /* after we have mapped everything important (with known addresses), map the kernel
     * page tables somewhere (we don't really care where) */
    kernel_early_vmm_map_tables();

    kprintf("%s: loading page directory %p\n", __func__, (void *)kernel_directory->physical_address);
    cpu_load_page_directory(kernel_directory->physical_address);
    cpu_enable_paging();
}
