#!/usr/bin/env -S make -f

ARCH_OBJS :=\
	$(LOCAL)boot.o

ARCH_SRCS_C :=\
	$(LOCAL)../x86-family/gdt.c \
	$(LOCAL)../x86-family/idt.c \
	$(LOCAL)../x86-family/pit.c \
	$(LOCAL)../x86-family/pic.c \
	$(LOCAL)../x86-family/paging_common.c \
	$(LOCAL)paging.c \
	$(LOCAL)stacktrace.c

ARCH_CPPFLAGS := -I $(LOCAL) -I $(LOCAL)../x86-family/

ARCH_LINK_SCRIPT := $(LOCAL)/link.ld

ARCH_CFLAGS := -fno-omit-frame-pointer

# our kernel should be at the 1mb boundary (after everything somewhat important for the BIOS / whatever)
ARCH_LDFLAGS := -Ttext=0x100000
