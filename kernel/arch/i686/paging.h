#ifndef __ARCH_I686_PAGING_H
#define __ARCH_I686_PAGING_H 1

#include <kref.h>

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define PAGE_SIZE ((size_t)4096)

typedef union __attribute__((packed)) {
    struct {
        bool present:1;
        bool write:1;
        bool user:1;
        bool write_through:1;
        bool cache_disable:1;
        bool accessed:1;
        bool dirty:1;
        bool pat:1;
        bool global:1;
        unsigned int avail:3;
        unsigned int phys:20;
    };
    uint32_t __raw;
} page_t;
static_assert(sizeof(page_t) == 4, "page_t size is incorrect");

typedef struct __attribute__((packed)) __attribute__((aligned(PAGE_SIZE))) page_table {
    page_t pages[1024];
} page_table_t;
static_assert(sizeof(page_table_t) == 4096, "page_table_t size is incorrect");

typedef union __attribute__((packed)) {
    struct {
        bool present:1;
        bool write:1;
        bool user:1;
        bool write_through:1;
        bool cache_disable:1;
        bool accessed:1;
        bool zero:1;
        bool pat:1;
        bool global:1;
        unsigned int avail:3;
        unsigned int phys:20;
    };
    uint32_t __raw;
} page_directory_entry_t;
static_assert(sizeof(page_directory_entry_t) == 4, "page_directory_t size is incorrect");

typedef struct __attribute__((packed)) __attribute__((aligned(PAGE_SIZE))) {
    page_directory_entry_t real_map[1024];
    page_table_t *map[1024];
} page_directory_real_t;

typedef struct page_directory {
    page_directory_real_t *real;

    /* physical address of *real_map which can be used to load the page directory */
    uintptr_t physical_address;

    kref_t __ref;
} page_directory_t;

#endif
