#!/usr/bin/env -S make -f

ARCH_OBJS := \
	$(LOCAL)boot32.o \
	$(LOCAL)boot.o

ARCH_SRCS_C := \
	$(LOCAL)../x86-family/gdt.c \
	$(LOCAL)../x86-family/idt.c \
	$(LOCAL)../x86-family/pit.c \
	$(LOCAL)../x86-family/pic.c \
	$(LOCAL)../x86-family/paging_common.c \
	$(LOCAL)paging.c \
	$(LOCAL)stacktrace.c

ARCH_CPPFLAGS := -I $(LOCAL) -I $(LOCAL)../x86-family/

# disable various problematic optimisations
ARCH_CFLAGS := -fno-omit-frame-pointer -mno-sse -mno-red-zone

ARCH_LINK_SCRIPT := $(LOCAL)/link.ld

AS32 ?= $(AS)

# see kernel/Makefile and boot32.S for details
$(LOCAL)boot32.o: $(LOCAL)/boot32._s | $(AS32)
	@echo AS32 $@
	$(AS32) $(ASFLAGS) $< -o $@
