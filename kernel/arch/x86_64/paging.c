#include "cpu.h"
#include "paging.h"

#include <boot.h>
#include <kernel.h>
#include <kheap.h>
#include <kprintf.h>
#include <mem/pmm.h>
#include <mem/vmm.h>
#include <string.h>

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>

enum {
    /* 12 + 9 + 9 + 9 + 9 = 48 virtual bits */
    VADDR_PML4_INDEX_SHIFT = 39,
    VADDR_PML4_BITMASK     = 0x1FF,
    VADDR_PML3_INDEX_SHIFT = 30,
    VADDR_PML3_BITMASK     = 0x1FF,
    VADDR_PML2_INDEX_SHIFT = 21,
    VADDR_PML2_BITMASK     = 0x1FF,
    VADDR_PML1_INDEX_SHIFT = 12,
    VADDR_PML1_BITMASK     = 0x1FF,
};

static_assert(PAGE_SIZE == BLOCK_SIZE, "code hasn't been tested with PAGE_SIZE != BLOCK_SIZE");

/* helpers for page_directory_t */
page_directory_t *page_directory_init(void) {
    page_directory_t *pdir = kcalloc(1, sizeof(page_directory_t));
    if (pdir == NULL) {
        return NULL;
    }

    const size_t pml4_size = ALIGN(sizeof(pml4_t), BLOCK_SIZE);

    const uintptr_t phys = pmm_alloc(pml4_size);
    if (phys == 0) {
        kfree(pdir);
        return NULL;
    }

    pdir->physical_address = phys;

    pml4_t * const pml4 = vmmk_map_region_random(phys, pml4_size, true);
    if (pml4 == NULL) {
        pmm_free(phys, pml4_size);
        kfree(pdir);
        return NULL;
    }

    memset(pml4, 0, sizeof(pml4_t));

    pdir->pml4 = pml4;

    kref_init(&pdir->__ref);

    return pdir;
}

static void page_directory_destroy(void *_pdir) {
    page_directory_t *pdir = _pdir;

    for (size_t pml4i = 0; pml4i < array_size(pdir->pml4->real_map); pml4i++) {
        assert(pdir->pml4->real_map[pml4i].__raw == 0);
    }

    const size_t pml4_size = ALIGN(sizeof(pml4_t), BLOCK_SIZE);
    pmm_free(pdir->physical_address, pml4_size);

    kfree(pdir);
}

void page_directory_up(page_directory_t *pdir) {
    assert(pdir != NULL);
    kref_up(&pdir->__ref);
}

void page_directory_down(page_directory_t *pdir) {
    assert(pdir != NULL);
    kref_down(&pdir->__ref, page_directory_destroy, pdir);
}

/* helpers */
static __attribute__((const)) uintptr_t vaddr_to_phys(uintptr_t vaddr) {
    return vaddr / PAGE_SIZE;
}

static __attribute__((const)) uintptr_t phys_to_vaddr(uintptr_t phys) {
    return phys * PAGE_SIZE;
}

static __attribute__((const)) size_t vaddr_to_pml4i(uintptr_t vaddr) {
    return (vaddr >> VADDR_PML4_INDEX_SHIFT) & VADDR_PML4_BITMASK;
}

static __attribute__((const)) uintptr_t pml4i_to_vaddr(size_t pml4i) {
    return (uintptr_t)(pml4i << VADDR_PML4_INDEX_SHIFT);
}

static __attribute__((const)) size_t vaddr_to_pml3i(uintptr_t vaddr) {
    return (vaddr >> VADDR_PML3_INDEX_SHIFT) & VADDR_PML3_BITMASK;
}

static __attribute__((const)) uintptr_t pml3i_to_vaddr(size_t pml3i) {
    return (uintptr_t)(pml3i << VADDR_PML3_INDEX_SHIFT);
}

static __attribute__((const)) size_t vaddr_to_pml2i(uintptr_t vaddr) {
    return (vaddr >> VADDR_PML2_INDEX_SHIFT) & VADDR_PML2_BITMASK;
}

static __attribute__((const)) uintptr_t pml2i_to_vaddr(size_t pml2i) {
    return (uintptr_t)(pml2i << VADDR_PML2_INDEX_SHIFT);
}

static __attribute__((const)) size_t vaddr_to_pml1i(uintptr_t vaddr) {
    return (vaddr >> VADDR_PML1_INDEX_SHIFT) & VADDR_PML1_BITMASK;
}

static __attribute__((const)) uintptr_t pml1i_to_vaddr(size_t pml1i) {
    return (uintptr_t)(pml1i << VADDR_PML1_INDEX_SHIFT);
}

/**/

pml4_t __kernel_directory_pml4 = {};

page_directory_t __kernel_directory_real = {
    .pml4 = &__kernel_directory_pml4,
    .physical_address = (uintptr_t)&__kernel_directory_pml4,
    .__ref = KREF_STATIC_INIT,
};
page_directory_t *kernel_directory = &__kernel_directory_real;

/* helpers for the kernel page directory */
/* XXX: you need to check if a page table is present before dereferencing
 * or passing it to another function ! */
static __attribute__((const)) pml3_t *kernel_pml4_get(pml4_t *pml4, size_t pml4i) {
    const uintptr_t base = (uintptr_t)pml4 & ~((512 * PAGE_SIZE) - 1);

    return (void *)(base + pml4i * PAGE_SIZE);
}

static pml3_t *kernel_early_pml4_get(pml4_t *pml4, size_t pml4i) {
    if (pml4->map[pml4i] == NULL) {
        const size_t pml3_size = ALIGN(sizeof(pml3_t), BLOCK_SIZE);

        const uintptr_t phys = pmm_alloc(pml3_size);
        assert(phys != 0);
        memset((void *)phys, 0, pml3_size);

        pml4->real_map[pml4i] = (pml4_entry_t){
            .present = true,
            .write = true,
            .user = false,
            .write_through = false,
            .cache_disable = false,
            .accessed = false,
            .zero = 0,
            .avail = 0,
            .phys = vaddr_to_phys(phys),
            .avail2 = 0,
            .no_execute = false,
        };

        pml4->map[pml4i] = (void *)phys;
    }

    return pml4->map[pml4i];
}

__attribute__((const)) pml2_t *kernel_pml3_get(pml3_t *pml3, size_t pml3i) {
    const size_t offset = (uintptr_t)pml3 & ((512 * PAGE_SIZE) - 1); /* -> pml4i * PAGE_SIZE */
    const uintptr_t base = (uintptr_t)pml3 & ~((512 * 512 * PAGE_SIZE) - 1);

    return (void *)(base + offset * 512 + pml3i * PAGE_SIZE);
}

static pml2_t *kernel_early_pml3_get(pml3_t *pml3, size_t pml3i) {
    if (pml3->map[pml3i] == NULL) {
        const size_t pml2_size = ALIGN(sizeof(pml2_t), BLOCK_SIZE);

        const uintptr_t phys = pmm_alloc(pml2_size);
        assert(phys != 0);
        memset((void *)phys, 0, pml2_size);

        pml3->real_map[pml3i] = (pml3_entry_t){
            .present = true,
            .write = true,
            .user = false,
            .write_through = true,
            .cache_disable = true,
            .accessed = false,
            .dirty = false,
            .zero = 0,
            .avail = 0,
            .phys = vaddr_to_phys(phys),
            .avail2 = 0,
            .no_execute = false,
        };

        pml3->map[pml3i] = (void *)phys;
    }

    return pml3->map[pml3i];
}

__attribute__((const)) pml1_t *kernel_pml2_get(pml2_t *pml2, size_t pml2i) {
    const size_t offset = (uintptr_t)pml2 & ((512 * 512 * PAGE_SIZE) - 1); /* pm4i * PAGE_SIZE * 512 + pm3i * PAGE_SIZE */
    const uintptr_t base = (uintptr_t)pml2 & ~((512 * 512 * 512 * PAGE_SIZE) - 1);

    return (void *)base + offset * 512 + pml2i * PAGE_SIZE;
}

static pml1_t *kernel_early_pml2_get(pml2_t *pml2, size_t pml2i) {
    if (pml2->map[pml2i] == NULL) {
        const size_t pml1_size = ALIGN(sizeof(pml1_t), BLOCK_SIZE);

        const uintptr_t phys = pmm_alloc(pml1_size);
        assert(phys != 0);
        memset((void *)phys, 0, pml1_size);

        pml2->real_map[pml2i] = (pml2_entry_t){
            .present = true,
            .write = true,
            .user = false,
            .write_through = true,
            .cache_disable = true,
            .accessed = false,
            .dirty = false,
            .avail = 0,
            .phys = vaddr_to_phys(phys),
            .avail2 = 0,
            .no_execute = false,
        };

        pml2->map[pml2i] = (void *)phys;
    }

    return pml2->map[pml2i];
}

/* helpers for normal page directories */
__attribute__((const)) pml3_t *pml4_get(pml4_t *pml4, size_t pml4i) {
    return pml4->map[pml4i];
}
__attribute__((const)) pml3_t *pml4_get_const(const pml4_t *pml4, size_t pml4i) {
    return pml4->map[pml4i];
}
__attribute__((const)) pml2_t *pml3_get(pml3_t *pml3, size_t pml3i) {
    return pml3->map[pml3i];
}
__attribute__((const)) const pml2_t *pml3_get_const(const pml3_t *pml3, size_t pml3i) {
    return pml3->map[pml3i];
}
__attribute__((const)) pml1_t *pml2_get(pml2_t *pml2, size_t pml2i) {
    return pml2->map[pml2i];
}
__attribute__((const)) const pml1_t *pml2_get_const(const pml2_t *pml2, size_t pml2i) {
    return pml2->map[pml2i];
}

/* helper for all page directories */
__attribute__((pure)) pml1_entry_t *pml1_get(pml1_t *pml1, size_t pml1i) {
    assert(pml1 != NULL);

    return &pml1->real_map[pml1i];
}

/* XXX: you're responsible for ensuring pml?_index are sane values */
static __attribute__((const)) uintptr_t index_to_vaddr(size_t pml4i, size_t pml3i, size_t pml2i, size_t pml1i) {
    uintptr_t v = pml4i_to_vaddr(pml4i) | pml3i_to_vaddr(pml3i) | pml2i_to_vaddr(pml2i) | pml1i_to_vaddr(pml1i);

    if (v & (2^47)) {
        return v + 0xFFFF000000000000;
    } else {
        return v;
    }
}

/* kernel helpers for page directory manipulation before paging is active */

/* map a page into kernel space while paging is still disabled (ie. assume that the
 * kernel page directory isn't active and access all tables by their physical address) */
void kernel_early_vmm_map(uintptr_t vaddr, uintptr_t paddr, bool present, bool write) {
    assert(vaddr != 0);
    assert(IS_ALIGNED(vaddr, PAGE_SIZE));
    assert(IS_ALIGNED(paddr, PAGE_SIZE));
    assert(pmm_is_used(paddr));

    const size_t pml4i = vaddr_to_pml4i(vaddr);
    const size_t pml3i = vaddr_to_pml3i(vaddr);
    const size_t pml2i = vaddr_to_pml2i(vaddr);
    const size_t pml1i = vaddr_to_pml1i(vaddr);

    pml3_t * const pml3 = kernel_early_pml4_get(kernel_directory->pml4, pml4i);
    pml2_t * const pml2 = kernel_early_pml3_get(pml3, pml3i);
    pml1_t * const pml1 = kernel_early_pml2_get(pml2, pml2i);

    pml1_entry_t *page = &pml1->real_map[pml1i];

    const pml1_entry_t new_page = (pml1_entry_t){
        .present = present,
        .write = write,
        .user = false,
        .write_through = false,
        .cache_disable = false,
        .accessed = false,
        .dirty = false,
        .huge_page = false,
        .global = false,
        .avail = 0,
        .phys = vaddr_to_phys(paddr),
        .avail2 = 0,
        .no_execute = false,
    };

#if 0
    if ((page->__raw != 0) && (page->__raw != new_page.__raw)) {
        kprintf("WARN: early map override:\n");
        kprintf("old:");
        page_debug(vaddr, page);
        kprintf("new:");
        page_debug(vaddr, &new_page);
    }
#endif

    *page = new_page;
}

/* setup recursive mapping of the page tables (see https://wiki.osdev.org/Page_Tables#Recursive%20mapping for the virtual space wasted) */
void kernel_early_init_recursive_mapping(page_directory_t *pdir) {
    assert(pdir->pml4->real_map[511].__raw == 0);
    pdir->pml4->real_map[511] = (pml4_entry_t){
        .present = true,
            .write = true,
            .write_through = false,
            .cache_disable = false,
            .accessed = true,
            .dirty = true,
            .zero = 0,
            .avail = 0,
            .phys = vaddr_to_phys(pdir->physical_address),
            .avail2 = 0,
            .no_execute = false,
    };
}

/* helpers for all other page directories */
uintptr_t vmm_unmap_page(page_directory_t *pdir, uintptr_t vaddr) {
    assert(pdir != kernel_directory);

    pml4_t *pml4 = pdir->pml4;
    assert(vaddr != 0);
    assert(IS_ALIGNED(vaddr, PAGE_SIZE));

    const size_t pml4i = vaddr_to_pml4i(vaddr);
    const size_t pml3i = vaddr_to_pml3i(vaddr);
    const size_t pml2i = vaddr_to_pml2i(vaddr);
    const size_t pml1i = vaddr_to_pml1i(vaddr);

    assert(pml4->real_map[pml4i].present);
    pml3_t * const pml3 = pml4_get(pml4, pml4i);
    assert(pml3 != NULL);

    assert(pml3->real_map[pml3i].present);
    pml2_t * const pml2 = pml3_get(pml3, pml3i);
    assert(pml2 != NULL);

    assert(pml2->real_map[pml2i].present);
    pml1_t * const pml1 = pml2_get(pml2, pml2i);
    assert(pml1 != NULL);

    pml1_entry_t * const page = &pml1->real_map[pml1i];

    uintptr_t v = phys_to_vaddr(page->phys);

    const pml1_entry_t new_page = (pml1_entry_t){
        .present = false,
        .write = false,
        .user = false,
        .write_through = false,
        .cache_disable = false,
        .accessed = false,
        .huge_page = false,
        .global = false,
        .avail = 0,
        .phys = 0,
        .avail2 = 0,
        .no_execute = false,
    };
    *page = new_page;

    // FIXME: try to free empty tables

    return v;
}

/* TODO: this contains critical sections */
void vmm_map_page(page_directory_t *pdir, uintptr_t vaddr, uintptr_t paddr, bool present, bool write, bool user, unsigned int extra) {
    assert(pdir != kernel_directory);

    pml4_t *pml4 = pdir->pml4;

    assert(vaddr != 0);
    assert(IS_ALIGNED(vaddr, PAGE_SIZE));
    assert(IS_ALIGNED(vaddr, PAGE_SIZE));
    assert(pmm_is_used(paddr));

    const size_t pml4i = vaddr_to_pml4i(vaddr);
    const size_t pml3i = vaddr_to_pml3i(vaddr);
    const size_t pml2i = vaddr_to_pml2i(vaddr);
    const size_t pml1i = vaddr_to_pml1i(vaddr);

    if (! pml4->real_map[pml4i].present) {
        const size_t pml3_size = ALIGN(sizeof(pml3_t), BLOCK_SIZE);

        const uintptr_t phys = pmm_alloc(pml3_size);
        assert(phys != 0);

        assert(pml4->real_map[pml4i].__raw == 0);
        pml4->real_map[pml4i] = (pml4_entry_t){
            .present = true,
            .write = true,
            .user = true,
            .write_through = true,
            .cache_disable = true,
            .accessed = false,
            .dirty = false,
            .zero = 0,
            .avail = 0,
            .phys = vaddr_to_phys(phys),
            .avail2 = 0,
            .no_execute = false,
        };

        void * const pml3 = vmmk_map_region_random(phys, pml3_size, true);
        assert(pml3 != NULL);

        memset(pml3, 0, pml3_size);
        pml4->map[pml4i] = pml3;
    }
    pml3_t * const pml3 = pml4_get(pml4, pml4i);

    if (! pml3->real_map[pml3i].present) {
        const size_t pml2_size = ALIGN(sizeof(pml2_t), BLOCK_SIZE);

        const uintptr_t phys = pmm_alloc(pml2_size);
        assert(phys != 0);

        assert(pml3->real_map[pml3i].__raw == 0);
        pml3->real_map[pml3i] = (pml3_entry_t){
            .present = true,
            .write = true,
            .user = true,
            .write_through = true,
            .cache_disable = true,
            .accessed = false,
            .dirty = false,
            .zero = 0,
            .avail = 0,
            .phys = vaddr_to_phys(phys),
            .avail2 = 0,
            .no_execute = false,
        };

        void * const pml2 = vmmk_map_region_random(phys, pml2_size, true);
        assert(pml2 != NULL);

        memset(pml2, 0, pml2_size);
        pml3->map[pml3i] = pml2;
    }
    pml2_t * const pml2 = pml3_get(pml3, pml3i);

    if (! pml2->real_map[pml2i].present) {
        const size_t pml1_size = ALIGN(sizeof(pml1_t), BLOCK_SIZE);

        const uintptr_t phys = pmm_alloc(pml1_size);
        assert(phys != 0);

        assert(pml2->real_map[pml2i].__raw == 0);
        pml2->real_map[pml2i] = (pml2_entry_t){
            .present = true,
            .write = true,
            .user = true,
            .write_through = true,
            .cache_disable = true,
            .accessed = false,
            .dirty = false,
            .avail = 0,
            .phys = vaddr_to_phys(phys),
            .avail2 = 0,
            .no_execute = false,
        };

        void * const pml1 = vmmk_map_region_random(phys, pml1_size, true);
        assert(pml1 != NULL);

        memset(pml1, 0, pml1_size);
        pml2->map[pml2i] = pml1;
    }

    pml1_t * const pml1 = pml2_get(pml2, pml2i);
    pml1_entry_t *page = &pml1->real_map[pml1i];

    (void)extra;
    const pml1_entry_t new_page = (pml1_entry_t){
        .present = present,
        .write = write,
        .user = user,
        .write_through = false,
        .cache_disable = false,
        .accessed = false,
        .dirty = false,
        .huge_page = false,
        .global = false,
        .avail = 0,
        .phys = vaddr_to_phys(paddr),
        .avail2 = 0,
        .no_execute = false,
    };
    *page = new_page;
}

/* kernel page directory helpers */
static_assert(PAGE_SIZE == BLOCK_SIZE, "vmmk_find_region assumes that PAGE_SIZE == BLOCK_SIZE");
__attribute__((pure)) uintptr_t vmmk_find_region(size_t size, bool dma) {
    /* #lizard forgives the complexity */
    pml4_t *pml4 = kernel_directory->pml4;

    /* TODO: optimise for huge pages */

    /* current candidate */
    size_t region_pml4i = 0;
    size_t region_pml3i = 0;
    size_t region_pml2i = 0;
    size_t region_pml1i = 0;
    size_t region_size = 0;
    bool need_region = true;

    /* first layer */
    /* XXX: we assume that pml4i=0,pml3i=0,pml2i=0,pml1i=0 is already mapped as non-present to
     * simplify the logic
     * XXX: skip the last pml4 since it's the recursve mapping of the page table and we don't
     * want to map *anything* there */
    for (size_t pml4i = 0; pml4i < (array_size(pml4->real_map) - 1); pml4i++) {
        if (need_region) {
            region_pml4i = pml4i;
        }

        if (pml4->real_map[pml4i].present) {
            pml3_t * const pml3 = kernel_pml4_get(pml4, pml4i);

            for (size_t pml3i = 0; pml3i < array_size(pml3->real_map); pml3i++) {
                if (need_region) {
                    region_pml3i = pml3i;
                }

                if (pml3->real_map[pml3i].present) {
                    pml2_t * const pml2 = kernel_pml3_get(pml3, pml3i);

                    for (size_t pml2i = 0; pml2i < array_size(pml2->real_map); pml2i++) {
                        if (need_region) {
                            region_pml2i = pml2i;
                        }

                        if (pml2->real_map[pml2i].present) {
                            pml1_t * const pml1 = kernel_pml2_get(pml2, pml2i);

                            for (size_t pml1i = 0; pml1i < array_size(pml1->real_map); pml1i++) {
                                /* now we need to check for the special NULL page */
                                if ((pml4i == 0) && (pml3i == 0) && (pml2i == 0) && (pml1i == 0)) {
                                    continue;
                                }

                                if (need_region) {
                                    region_pml1i = pml1i;
                                }

                                const uintptr_t vaddr = index_to_vaddr(pml4i, pml3i, pml2i, pml1i);

                                if ((pml1->real_map[pml1i].__raw == 0) && (!dma || !pmm_is_used(vaddr))) {
                                    need_region = false;
                                    region_size += PAGE_SIZE;
                                } else {
                                    need_region = true;
                                    region_size = 0;
                                }

                                if (region_size >= size) {
                                    break;
                                }
                            }
                        } else {
                            need_region = false;
                            region_size += PAGE_SIZE * 512;
                        }

                        if (region_size >= size) {
                            break;
                        }
                    }
                } else {
                    need_region = false;
                    region_size += PAGE_SIZE * 512 * 512;
                }

                if (region_size >= size) {
                    break;
                }
            }
        } else {
            need_region = false;
            region_size += PAGE_SIZE * 512 * 512 * 512;
        }

        if (region_size >= size) {
            return index_to_vaddr(region_pml4i, region_pml3i, region_pml2i, region_pml1i);
        }
    }

    /* out of memory */
    return 0;
}

static __attribute__((pure)) uintptr_t vmm_find_region(const page_directory_t *pdir, size_t size, bool dma) {
    /* #lizard forgives the complexity */
    assert(pdir != kernel_directory);

    const pml4_t * const pml4 = pdir->pml4;

    /* TODO: optimise for huge pages */

    /* current candidate */
    size_t region_pml4i = 0;
    size_t region_pml3i = 0;
    size_t region_pml2i = 0;
    size_t region_pml1i = 0;
    size_t region_size = 0;
    bool need_region = true;

    /* first layer */
    /* XXX: we assume that pml4i=0,pml3i=0,pml2i=0,pml1i=0 is already mapped as non-present to
     * simplify the logic
     * XXX: skip the last pml4 since it's the recursve mapping of the page table and we don't
     * want to map *anything* there */
    for (size_t pml4i = 0; pml4i < (array_size(pml4->real_map) - 1); pml4i++) {
        if (need_region) {
            region_pml4i = pml4i;
        }

        if (pml4->real_map[pml4i].present) {
            const pml3_t * const pml3 = pml4_get_const(pml4, pml4i);

            for (size_t pml3i = 0; pml3i < array_size(pml3->real_map); pml3i++) {
                if (need_region) {
                    region_pml3i = pml3i;
                }

                if (pml3->real_map[pml3i].present) {
                    const pml2_t * const pml2 = pml3_get_const(pml3, pml3i);

                    for (size_t pml2i = 0; pml2i < array_size(pml2->real_map); pml2i++) {
                        if (need_region) {
                            region_pml2i = pml2i;
                        }

                        if (pml2->real_map[pml2i].present) {
                            const pml1_t * const pml1 = pml2_get_const(pml2, pml2i);

                            for (size_t pml1i = 0; pml1i < array_size(pml1->real_map); pml1i++) {
                                /* now we need to check for the special NULL page */
                                if ((pml4i == 0) && (pml3i == 0) && (pml2i == 0) && (pml1i == 0)) {
                                    continue;
                                }

                                if (need_region) {
                                    region_pml1i = pml1i;
                                }

                                const uintptr_t vaddr = index_to_vaddr(pml4i, pml3i, pml2i, pml1i);

                                if ((pml1->real_map[pml1i].__raw == 0) && (!dma || !pmm_is_used(vaddr))) {
                                    need_region = false;
                                    region_size += PAGE_SIZE;
                                } else {
                                    need_region = true;
                                    region_size = 0;
                                }

                                if (region_size >= size) {
                                    break;
                                }
                            }
                        } else {
                            need_region = false;
                            region_size += PAGE_SIZE * 512;
                        }

                        if (region_size >= size) {
                            break;
                        }
                    }
                } else {
                    need_region = false;
                    region_size += PAGE_SIZE * 512 * 512;
                }

                if (region_size >= size) {
                    break;
                }
            }
        } else {
            need_region = false;
            region_size += PAGE_SIZE * 512 * 512 * 512;
        }

        if (region_size >= size) {
            return index_to_vaddr(region_pml4i, region_pml3i, region_pml2i, region_pml1i);
        }
    }

    /* out of memory */
    return 0;

    /* out of memory */
    return 0;
}

/* 0 = error
 * simple first-fit search, not necessarily the fastes or the best */
__attribute__((pure)) uintptr_t page_directory_find_free_region(page_directory_t *pdir, size_t size) {
    return vmm_find_region(pdir, size, false);
}

/* virtual memory management kernel */
static_assert(sizeof(pml3_t) == 2 * BLOCK_SIZE, "kernel_pml4_get_alloc");
pml3_t *kernel_pml4_get_alloc(pml4_t *pml4, size_t i) {
    if (! pml4->real_map[i].present) {
        /* we only allocate the important portion ! */
        const uintptr_t phys = pmm_alloc(BLOCK_SIZE);
        assert(phys != 0);

        assert(pml4->real_map[i].__raw == 0);
        pml4->real_map[i] = (pml4_entry_t){
            .present = true,
            .write = true,
            .user = false,
            .write_through = true,
            .cache_disable = true,
            .accessed = false,
            .dirty = false,
            .zero = 0,
            .avail = 0,
            .phys = vaddr_to_phys(phys),
            .avail2 = 0,
            .no_execute = false,
        };

        pml3_t * const pml3 = kernel_pml4_get(pml4, i);
        cpu_invalidate_page((uintptr_t)pml3);

        memset(pml3, 0, BLOCK_SIZE);

        return pml3;
    }

    return kernel_pml4_get(pml4, i);
}

static_assert(sizeof(pml2_t) == 2 * BLOCK_SIZE, "kernel_pml3_get_alloc");
pml2_t *kernel_pml3_get_alloc(pml3_t *pml3, size_t i) {
    if (! pml3->real_map[i].present) {
        /* we only allocate the important portion ! */
        const uintptr_t phys = pmm_alloc(BLOCK_SIZE);
        assert(phys != 0);

        assert(pml3->real_map[i].__raw == 0);
        pml3->real_map[i] = (pml3_entry_t){
            .present = true,
            .write = true,
            .user = false,
            .write_through = true,
            .cache_disable = true,
            .accessed = false,
            .dirty = false,
            .zero = 0,
            .avail = 0,
            .phys = vaddr_to_phys(phys),
            .avail2 = 0,
            .no_execute = false,
        };

        pml2_t * const pml2 = kernel_pml3_get(pml3, i);
        cpu_invalidate_page((uintptr_t)pml2);

        memset(pml2, 0, BLOCK_SIZE);

        return pml2;
    }

    return kernel_pml3_get(pml3, i);
}

static_assert(sizeof(pml1_t) == BLOCK_SIZE, "kernel_pml2_get_alloc");
pml1_t *kernel_pml2_get_alloc(pml2_t *pml2, size_t i) {
    if (! pml2->real_map[i].present) {
        const size_t pml1_size = BLOCK_SIZE;

        const uintptr_t phys = pmm_alloc(pml1_size);
        assert(phys != 0);

        assert(pml2->real_map[i].__raw == 0);
        pml2->real_map[i] = (pml2_entry_t){
            .present = true,
            .write = true,
            .user = false,
            .write_through = true,
            .cache_disable = true,
            .accessed = false,
            .dirty = false,
            .avail = 0,
            .phys = vaddr_to_phys(phys),
            .avail2 = 0,
            .no_execute = false,
        };

        pml1_t * const pml1 = kernel_pml2_get(pml2, i);
        cpu_invalidate_page((uintptr_t)pml1);

        memset(pml1, 0, pml1_size);

        return pml1;
    }

    return kernel_pml2_get(pml2, i);
}

void vmmk_map_page(uintptr_t vaddr, uintptr_t paddr, bool present, bool write) {
    pml4_t *pml4 = kernel_directory->pml4;

    assert(vaddr != 0);
    assert(IS_ALIGNED(vaddr, PAGE_SIZE));
    assert(IS_ALIGNED(vaddr, PAGE_SIZE));
    assert(pmm_is_used(paddr));

    const size_t pml4i = vaddr_to_pml4i(vaddr);
    const size_t pml3i = vaddr_to_pml3i(vaddr);
    const size_t pml2i = vaddr_to_pml2i(vaddr);
    const size_t pml1i = vaddr_to_pml1i(vaddr);

    pml3_t * const pml3 = kernel_pml4_get_alloc(pml4, pml4i);
    pml2_t * const pml2 = kernel_pml3_get_alloc(pml3, pml3i);
    pml1_t * const pml1 = kernel_pml2_get_alloc(pml2, pml2i);

    pml1_entry_t *page = &(pml1->real_map[pml1i]);

    const pml1_entry_t new_page = (pml1_entry_t){
        .present = present,
        .write = write,
        .user = false,
        .write_through = false,
        .cache_disable = false,
        .accessed = false,
        .dirty = false,
        .huge_page = false,
        .global = false,
        .avail = 0,
        .phys = vaddr_to_phys(paddr),
        .avail2 = 0,
        .no_execute = false,
    };
    *page = new_page;

    cpu_invalidate_page(vaddr);
}

uintptr_t vmmk_unmap_page(uintptr_t vaddr) {
    assert(vaddr != 0); /* probably a bug */
    assert(IS_ALIGNED(vaddr, PAGE_SIZE));

    const size_t pml4i = vaddr_to_pml4i(vaddr);
    const size_t pml3i = vaddr_to_pml3i(vaddr);
    const size_t pml2i = vaddr_to_pml2i(vaddr);
    const size_t pml1i = vaddr_to_pml1i(vaddr);

    /* FIXME: if these tables don't exist, someone either wants to be
     * really sure that this specific page is unmapped, even if it was never
     * mapped or we've hit a bug (someone unmapping random stuff) */
    pml3_t * const pml3 = kernel_pml4_get_alloc(kernel_directory->pml4, pml4i);
    pml2_t * const pml2 = kernel_pml3_get_alloc(pml3, pml3i);
    pml1_t * const pml1 = kernel_pml2_get_alloc(pml2, pml2i);
    pml1_entry_t *page = &pml1->real_map[pml1i];

    uintptr_t v = phys_to_vaddr(page->phys);
    const pml1_entry_t new_page = (pml1_entry_t){
        .present = false,
        .write = false,
        .user = false,
        .write_through = false,
        .cache_disable = false,
        .accessed = false,
        .huge_page = false,
        .global = false,
        .avail = 0,
        .phys = 0,
        .avail2 = 0,
        .no_execute = false,
    };
    *page = new_page;

    /* TODO: try to free empty tables */

    cpu_invalidate_page(vaddr);

    return v;
}

__attribute__((pure)) uintptr_t vmmk_find_dma_region(size_t size) {
    return vmmk_find_region(size, true);
}

__attribute__((pure)) uintptr_t vmmk_find_free_region(size_t size) {
    return vmmk_find_region(size, false);
}

uintptr_t vmmk_get_phys(uintptr_t vaddr) {
    assert(vaddr != 0); /* probably a bug */
    assert(IS_ALIGNED(vaddr, PAGE_SIZE));

    const size_t pml4i = vaddr_to_pml4i(vaddr);
    const size_t pml3i = vaddr_to_pml3i(vaddr);
    const size_t pml2i = vaddr_to_pml2i(vaddr);
    const size_t pml1i = vaddr_to_pml1i(vaddr);

    pml4_t * const pml4 = kernel_directory->pml4;

    if (!pml4->real_map[pml4i].present) {
        return 0;
    }

    pml3_t * const pml3 = kernel_pml4_get(pml4, pml4i);

    if (!pml3->real_map[pml3i].present) {
        return 0;
    }

    pml2_t * const pml2 = kernel_pml3_get(pml3, pml3i);

    if (!pml2->real_map[pml2i].present) {
        return 0;
    }

    pml1_t * const pml1 = kernel_pml2_get(pml2, pml2i);

    pml1_entry_t *page = &(pml1->real_map[pml1i]);

    if (page->present) {
        return phys_to_vaddr(page->phys);
    }

    return 0;
}

void vmm_init(void) {
    /* TODO: */
}

void vmm_finalize(void) {
    kprintf("vmm finalize: pml4 (virt: %p, phys: %p)\n", kernel_directory, (void *)kernel_directory->physical_address);

#if 1
    kernel_early_init_recursive_mapping(kernel_directory);
#else
    kernel_early_map_page_tables();
#endif

    /* load the pml4, paging is already enabled */
    kprintf("%s: loading pml4 %p\n", __func__, (void *)kernel_directory->physical_address);
    cpu_load_page_directory(kernel_directory->physical_address);

#if 1
    /* finalize recursive mapping */
    kernel_directory->pml4 = (void *)0xFFFFFFFFFFFFF000;

    {
        /* a few tests to ensure my math is right */
        pml3_t *pml3 = kernel_pml4_get(kernel_directory->pml4, 511);

        {
            pml3 = kernel_pml4_get(kernel_directory->pml4, 0);
            assert(pml3 == (void *)(0xFFFFFFFFFFE00000 + 0x1000 * 0));

#if 0
            const pml2_t *pml2 = pml3_get_const(pml3, 0);
            assert(pml2 == (void *)0xFFFFFFFFC0000000);

            const pml2_t *pml2_1 = pml3_get_const(pml3, 1);
            assert(pml2_1 == (void *)(0xFFFFFFFFC0000000 + 0x200000 * 0 + 0x1000 * 1));
#endif
        }

        {
#if 0
            pml3 = pml4_get_const(kernel_directory->pml4, 1);
            assert(pml3 == (void *)(0xFFFFFFFFFFE00000 + 0x1000 * 1));

            const pml2_t *pml2 = pml3_get_const(pml3, 0);
            assert(pml2 == (void *)0xFFFFFFFFC0000000 + 0x200000 * 1 + 0x1000 * 0);

            const pml2_t *pml2_1 = pml3_get_const(pml3, 1);
            assert(pml2_1 == (void *)(0xFFFFFFFFC0000000 + 0x200000 * 1 + 0x1000 * 1));

            const pml1_t *pml1 = pml2_get_const(pml2_1, 3);
            assert(pml1 == (void *)(0xFFFFFF8000000000 + 0x40000000 * 1 + 0x200000 * 1 + 0x1000 * 3));
#endif
        }
    }
#endif
}
