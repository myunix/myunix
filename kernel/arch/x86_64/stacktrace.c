#include <kprintf.h>
#include <stacktrace.h>

#include <stdint.h>
#include <stddef.h>

struct stackframe {
    struct stackframe *next;
    uintptr_t eip;
};

static void stackframe_print(const struct stackframe *frame) {
    kprintf("eip=%p (ebp=%p)\n", (void *)frame->eip, (void *)frame->next);
}

/* TODO: check if page is actually mapped before accessing data */
void stacktrace_print(void) {
    const struct stackframe *frame = (struct stackframe *)__builtin_frame_address(0);

    for (size_t count = 0; (frame != NULL) && (frame->eip != 0); count++, frame = frame->next) {
        stackframe_print(frame);
    }
}
