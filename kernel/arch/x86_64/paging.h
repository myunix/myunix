#ifndef __ARCH_X86_64_PAGING_H
#define __ARCH_X86_64_PAGING_H 1

#include <kref.h>

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define PAGE_SIZE ((size_t)4096)

typedef union __attribute__((packed)) {
    struct {
        bool present:1;
        bool write:1;
        bool user:1;
        bool write_through:1;

        bool cache_disable:1;
        bool accessed:1;
        bool dirty:1;
        bool huge_page:1;
        bool global:1;
        unsigned int avail:3;
        uint64_t phys:40;
        unsigned int avail2:11;
        bool no_execute:1;
    };
    uint64_t __raw;
} pml1_entry_t;
static_assert(sizeof(pml1_entry_t) == sizeof(uint64_t), "pml1_entry_t size is incorrect");

typedef struct __attribute__((packed)) __attribute__((aligned(4096))) {
    pml1_entry_t real_map[512] __attribute__((aligned(4096)));
} pml1_t;

typedef union __attribute__((packed)) {
    struct {
        bool present:1;
        bool write:1;
        bool user:1;
        bool write_through:1;
        bool cache_disable:1;
        bool accessed:1;
        bool dirty:1;
        bool huge_page:1;
        bool global:1;
        unsigned int avail:3;
        uint64_t phys:40;
        unsigned int avail2:11;
        bool no_execute:1;
    };
    uint64_t __raw;
} pml2_entry_t;
static_assert(sizeof(pml2_entry_t) == sizeof(uint64_t), "pml2_entry_t size is incorrect");

typedef struct __attribute__((packed)) __attribute__((aligned(4096))) {
    pml2_entry_t real_map[512] __attribute__((aligned(4096)));

    pml1_t *map[512];
} pml2_t;

typedef union __attribute__((packed)) {
    struct {
        bool present:1;
        bool write:1;
        bool user:1;
        bool write_through:1;
        bool cache_disable:1;
        bool accessed:1;
        bool dirty:1;
        bool zero:1;
        bool global:1;
        unsigned int avail:3;
        uint64_t phys:40;
        unsigned int avail2:11;
        bool no_execute:1;
    };
    uint64_t __raw;
} pml3_entry_t;
static_assert(sizeof(pml3_entry_t) == sizeof(uint64_t), "pml3_entry_t size is incorrect");

typedef struct __attribute__((packed)) __attribute__((aligned(4096))) {
    pml3_entry_t real_map[512] __attribute__((aligned(4096)));

    pml2_t *map[512];
} pml3_t;

typedef union __attribute__((packed)) {
    struct {
        bool present:1;
        bool write:1;
        bool user:1;
        bool write_through:1;
        bool cache_disable:1;
        bool accessed:1;
        bool dirty:1;
        unsigned int zero:2;
        unsigned int avail:3;
        uint64_t phys:40;
        unsigned int avail2:11;
        bool no_execute:1;
    };
    uint64_t __raw;
} pml4_entry_t;
static_assert(sizeof(pml4_entry_t) == sizeof(uint64_t), "pml4_entry_t size is incorrect");

typedef struct __attribute__((packed)) __attribute__((aligned(4096))) {
    /* this is the real page directory (pml4) as seen by the cpu */
    pml4_entry_t real_map[512] __attribute__((aligned(4096)));

    pml3_t *map[512];
} pml4_t;

typedef struct page_directory {
    pml4_t *pml4;

    uintptr_t physical_address;

    kref_t __ref;
} page_directory_t;

#endif
