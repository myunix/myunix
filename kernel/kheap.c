#include <kheap.h>

#include <assert.h>
#include <kernel.h>
#include <kprintf.h>
#include <mem/pmm.h>
#include <mem/vmm.h>
#include <string.h>

#include <stacktrace.h>

#include <stdalign.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define KHEAP_MAGIC ((uint32_t)0xA110C000)
#define KHEAP_DEAD ((uint32_t)0xDEADDEAD)

/* minimum allocation of pages per block */
#define KHEAP_MIN_PAGE_ALLOC 16

/* header of every block (a number of allocated pages) */
struct kheap_block {
    /* linked list structure */
    struct kheap_block *prev;
    struct kheap_block *next;

    size_t size; /* size of the block in bytes */
    size_t used; /* number of used bytes in this block */
    /* the following must always hold true:
     * block->used = sizeof(struct kheap_block) + (foreach alloc in block: alloc->size)
     * block->used <= block->size
     * */

    struct kheap_alloc *first; /* pointer to the first allocation in this block */
};

struct kheap_alloc {
    /* linked list structure */
    struct kheap_alloc *prev;
    struct kheap_alloc *next;

    struct kheap_block *owner;
    uint32_t magic;

    /* XXX: size includes the size of this struct and the size of diff_info_t
     * any alignment and the actual size, therefore the following must always hold true:
     * alloc->size >= sizeof(struct kheap_alloc) + sizeof(diff_info_t)  */
    size_t size;
};

/* used to store the alignment before the pointer passed to the user
 * XXX: this type must have no alignment requirements */
typedef uint8_t diff_info_t;

/* helpers */

/* get the ptr that will be passed to the user from a kheap_allocation
 * also takes care of alignment */
static __attribute__((const)) void *kheap_ptr_from_alloc(struct kheap_alloc *alloc, size_t alignment) {
    uintptr_t alloc_start = (uintptr_t)alloc + sizeof(struct kheap_alloc);

    /* ensure we always have place to store the diff info */
    uintptr_t ptr = alloc_start + sizeof(diff_info_t);

    /* how many bytes we have too much */
    const size_t ptr_bad = ptr & (alignment - 1);
    size_t diff = 0;
    if (ptr_bad != 0) {
        diff = alignment - ptr_bad;
        ptr += diff;
    }

    diff_info_t *diff_info = (void *)(ptr - sizeof(diff_info_t));
    *diff_info = diff + sizeof(diff_info_t);

    return (void *)ptr;
}

/* get the kheap_alloc struct from a ptr this method also takes care of
 * alignment */
static __attribute__((const)) struct kheap_alloc *kheap_alloc_from_ptr(void *ptr) {
    diff_info_t *diff_info = (void *)((uintptr_t)ptr - sizeof(diff_info_t));

    return (void *)((uintptr_t)ptr - sizeof(struct kheap_alloc) - *diff_info);
}

static bool kheap_lock(void) {
    /* TODO: implement */
    return 0;
}

static bool kheap_unlock(void) {
    /* TODO: implement */
    return 0;
}

/* root of our linked list of blocks */
static struct kheap_block *kheap_root = NULL;

/* allocate a new block using pmm/vmm primitives */
static struct kheap_block *kheap_allocate_new_block(size_t size) {

    size_t alloc_size = size + sizeof(struct kheap_block) + size;
    alloc_size = MAX(ALIGN(alloc_size, PAGE_SIZE), KHEAP_MIN_PAGE_ALLOC * PAGE_SIZE);

    // FIXME: reserve vspace before continue
    const uintptr_t v_start = vmmk_find_free_region(alloc_size);
    assert(v_start != 0);
    for (size_t i = 0; i <= alloc_size; i += PAGE_SIZE) {
        uintptr_t block = pmm_alloc(1 * BLOCK_SIZE);
        assert(block != 0);
        vmmk_map_page(v_start + i, block, true, true);
    }

    struct kheap_block *block = (void *)v_start;
    block->prev = NULL;
    block->next = NULL;
    block->first = NULL;
    block->size = alloc_size;
    block->used = sizeof(struct kheap_block);
    return block;
}

/* free a block using pmm/vmm primitives */
static void kheap_free_block(struct kheap_block *block) {
    assert(block->used == sizeof(struct kheap_block));
    assert(block->first == NULL);

    const size_t alloc_size = block->size;
    assert(IS_ALIGNED(alloc_size, PAGE_SIZE));
    const uintptr_t alloc_start = (uintptr_t)block;

    for (size_t i = 0; i <= alloc_size; i += PAGE_SIZE) {
        uintptr_t block = vmmk_unmap_page(alloc_start + i);
        pmm_free(block, 1 * BLOCK_SIZE);
    }
}

void kheap_init() {
    assert(kheap_root == NULL);
    kheap_root = kheap_allocate_new_block(0);
}

static struct kheap_alloc *align_kheap_alloc_ptr(uintptr_t ptr) {
    return (void *)ALIGN(ptr, alignof(struct kheap_alloc));
}

/* debug helper: ensure that an allocation is in a valid state */
static void assert_alloc(struct kheap_alloc *alloc) {
    assert(alloc->magic == KHEAP_MAGIC);
    assert(alloc->size >= (sizeof(struct kheap_alloc) + sizeof(diff_info_t)));

    if (alloc->next != NULL) {
        assert(alloc->next->prev == alloc);
        assert((uintptr_t)alloc->next >= ((uintptr_t)alloc + alloc->size));
    }

    if (alloc->prev != NULL) {
        assert(alloc->prev->next == alloc);
        assert((uintptr_t)alloc >= ((uintptr_t)alloc->prev + alloc->prev->size));
    }
}

/* basically a simple first-fit implementation */
struct kheap_alloc *kheap_alloc(size_t size, size_t alignment) {
    assert(size != 0);

    const size_t alloc_size = sizeof(struct kheap_alloc) + sizeof(diff_info_t) + alignment + size;
    const size_t required_size = alignof(struct kheap_alloc) + alloc_size;

    for (struct kheap_block *block = kheap_root;
            block != NULL;
            block = block->next) {
        const size_t block_free = block->size - block->used;

        if (block_free < required_size) {
            /* not enough memory in this block */
            if (block->next != NULL) {
                continue;
            } else {
                /* TODO: try to append new pages to this block */
            }
        }

        /* empty block */
        assert(IS_ALIGNED((uintptr_t)block->first, alignof(struct kheap_alloc)));
        if (block->first == NULL) {
            struct kheap_alloc *alloc = align_kheap_alloc_ptr((uintptr_t)block + sizeof(struct kheap_block));
            block->first = alloc;
            assert(IS_ALIGNED((uintptr_t)block->first, alignof(struct kheap_alloc)));

            alloc->prev = NULL;
            alloc->next = NULL;
            alloc->owner = block;
            alloc->magic = KHEAP_MAGIC;
            alloc->size = alloc_size;
            block->used += alloc_size;

            assert_alloc(alloc);

            return alloc;
        }

        /* test for free space before the first allocation */
        const size_t free_before_first = (uintptr_t)block->first - (uintptr_t)block - sizeof(struct kheap_block);
        if (free_before_first >= required_size) {
            assert(IS_ALIGNED((uintptr_t)block->first, alignof(struct kheap_alloc)));
            assert(block->first->prev == NULL);
            struct kheap_alloc *alloc = align_kheap_alloc_ptr((uintptr_t)block + sizeof(struct kheap_block));

            block->first->prev = alloc;
            assert(IS_ALIGNED((uintptr_t)block->first->prev, alignof(struct kheap_alloc)));
            alloc->next = block->first;
            assert(IS_ALIGNED((uintptr_t)alloc->next, alignof(struct kheap_alloc)));
            block->first = alloc;
            assert(IS_ALIGNED((uintptr_t)block->first, alignof(struct kheap_alloc)));

            alloc->owner = block;
            alloc->magic = KHEAP_MAGIC;
            alloc->size = alloc_size;
            block->used += alloc_size;
            assert_alloc(alloc);

            return alloc;
        }

        /* this block has enough free space, check if there is enough continuous space */
        for (struct kheap_alloc *alloc = block->first;
                alloc != NULL;
                alloc = alloc->next) {
            if (alloc->next == NULL) {
                /* last allocation of the block */
                const size_t remaining_free = (uintptr_t)block + block->size - (uintptr_t)alloc - alloc->size;
                if (remaining_free >= required_size) {
                    /* enough space */
                    struct kheap_alloc *new_alloc = align_kheap_alloc_ptr((uintptr_t)alloc + alloc->size);
                    alloc->next = new_alloc;
                    new_alloc->prev = alloc;
                    new_alloc->next = NULL;
                    new_alloc->owner = block;
                    new_alloc->magic = KHEAP_MAGIC;
                    new_alloc->size = alloc_size;
                    block->used += alloc_size;

                    assert_alloc(new_alloc);

                    return new_alloc;
                }
            } else {
                /* test for space between two allocations */
                const size_t between_free = (uintptr_t)alloc->next - (uintptr_t)alloc - alloc->size;

                if (between_free >= required_size) {
                    /* enough space */
                    struct kheap_alloc *new_alloc = align_kheap_alloc_ptr((uintptr_t)alloc + alloc->size);
                    new_alloc->prev = alloc;
                    new_alloc->next = alloc->next;
                    alloc->next = new_alloc;
                    new_alloc->next->prev = new_alloc;

                    new_alloc->owner = block;
                    new_alloc->magic = KHEAP_MAGIC;
                    new_alloc->size = alloc_size;
                    block->used += alloc_size;

                    assert_alloc(new_alloc);

                    return new_alloc;
                }
            }
        }

        if (block->next == NULL) {
            /* we need to allocate more memory */

            /* allocate enough for our current size */
            block->next = kheap_allocate_new_block(required_size);
            if (block->next == NULL) {
                kprintf("%s: out of memory!\n", __func__);
                /* out of memory */
                break;
            }
            block->next->prev = block;
        }
    }

    /* out of memory */
    return NULL;
}

static void kheap_free(struct kheap_alloc *alloc) {
    assert_alloc(alloc);

    alloc->magic = KHEAP_DEAD;

    struct kheap_block *block = alloc->owner;

    block->used -= alloc->size;

    /* remove allocation from linked list */
    if (alloc->next != NULL) {
        assert(alloc->next->prev == alloc);
        alloc->next->prev = alloc->prev;
    }

    if (alloc->prev != NULL) {
        assert(alloc->prev->next == alloc);
        alloc->prev->next = alloc->next;
    }

    if (alloc->prev == NULL) {
        /* this alloc was the first allocation of this block */
        assert(alloc == block->first);
        block->first = alloc->next;
        assert(IS_ALIGNED((uintptr_t)block->first, (size_t)alignof(struct kheap_alloc)));
    }

    if (block->first == NULL) {
        /* block completely unused */
        if (kheap_root == block) {
            if (block->next == NULL) {
                /* last block, can't free */
            } else {
                kheap_root = block->next;
                assert(block->next->prev == block);
                block->next->prev = block->prev;
                assert(block->prev->next == block);
                block->prev->next = block->next;
                kheap_free_block(block);
            }
        } else {
            /* return to kernel */
            if (block->prev != NULL) {
                block->prev->next = block->next;
            }

            if (block->next != NULL) {
                block->next->prev = block->prev;
            }
            kheap_free_block(block);
        }
    }
}

void *kmalloc(size_t size) {
    kheap_lock();

    struct kheap_alloc *alloc = kheap_alloc(size, 8);

    kheap_unlock();

    if (alloc == NULL) {
        return NULL;
    }

    assert_alloc(alloc);

    void *p = kheap_ptr_from_alloc(alloc, 8);
    return p;
}

void *kmalloc_aligned(size_t size, size_t alignment) {
    assert(alignment > 1);
    kheap_lock();

    struct kheap_alloc *alloc = kheap_alloc(size, alignment);

    kheap_unlock();

    if (alloc == NULL) {
        return NULL;
    }

    assert_alloc(alloc);

    return kheap_ptr_from_alloc(alloc, alignment);
}

void kfree(void *ptr) {
    if (ptr == NULL) {
        kprintf("%s: attempt to free NULL!\n", __func__);
        stacktrace_print();

        return;
    }

    kheap_lock();
    struct kheap_alloc *alloc = kheap_alloc_from_ptr(ptr);
    assert_alloc(alloc);

    if (alloc->magic == KHEAP_DEAD) {
        kheap_unlock();

        kprintf("%s: double free!\n", __func__);
        stacktrace_print();

        return;
    } else if (alloc->magic != KHEAP_MAGIC) {
        kheap_unlock();

        kprintf("%s: magic doesn't match!\n", __func__);
        stacktrace_print();

        return;
    }

    memset(ptr, 0,
            alloc->size
            - ((uintptr_t)ptr - (uintptr_t)alloc)
            );

    kheap_free(alloc);

    kheap_unlock();
}

void *kcalloc(size_t nobj, size_t size) {
    size_t real_size = nobj * size;

    void *p = kmalloc(real_size);
    if ((p != NULL) && (real_size > 0)) {
        memset(p, 0, real_size);
    }
    return p;
}
