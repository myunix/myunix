#include <kheap.h>
#include <kprintf.h>
#include <vfs.h>

#include <assert.h>
#include <string.h>

list_t *filesystem_types;

vfs_mount_t *vfs_root_mount;

void vfs_init() {
    filesystem_types = list_init();
    assert(filesystem_types);
}

/* ========== filesystem types ======== */
bool vfs_register(const char *name, vfs_get_superblock_f get_superblock, vfs_destroy_superblock_f destroy_superblock) {
    assert(name != NULL);
    assert(get_superblock != NULL);
    assert(destroy_superblock != NULL);

    for list_each(v, filesystem_types) {
        vfs_fs_type_t *fs_type = v->value;

        if (strncmp(fs_type->name, name, VFS_FS_NAME_MAX_LEN) == 0) {
            return false;
        }
    }

    vfs_fs_type_t *fs_type = kcalloc(1, sizeof(vfs_fs_type_t));
    if (fs_type == NULL) {
        return false;
    }

    fs_type->name = strndup(name, VFS_FS_NAME_MAX_LEN);
    if (fs_type->name == NULL) {
        kfree(fs_type);
        return NULL;
    }

    fs_type->get_superblock = get_superblock;
    fs_type->destroy_superblock = destroy_superblock;

    if (list_append_value(filesystem_types, fs_type) == NULL) {
        kfree(fs_type->name);
        kfree(fs_type);
        return false;
    }

    return true;
}

/* ========== mounts ======== */
static __attribute__((pure)) vfs_fs_type_t *find_type(const char *type) {
    for list_each(v, filesystem_types) {
        vfs_fs_type_t *fs_type = v->value;

        if (strncmp(fs_type->name, type, VFS_FS_NAME_MAX_LEN) == 0) {
            return fs_type;
        }
    }

    return NULL;
}

bool vfs_mount_type(const char *type, const char *args, vfs_file_t *device, const char *mountpoint) {
    assert(type != NULL);
    assert(mountpoint != NULL);

    vfs_fs_type_t *fs_type = find_type(type);
    if (fs_type == NULL) {
        return false;
    }

    vfs_fs_superblock_t *superblock = fs_type->get_superblock(fs_type, args, device);
    if (superblock == NULL) {
        return NULL;
    }

    assert(superblock->root_inode != NULL);

    /* special case: mounting / with no previous root */
    if (vfs_root_mount == NULL) {
        if (strncmp(mountpoint, "/", VFS_FILE_NAME_MAX_LEN) == 0) {
            vfs_mount_t *new_root = kcalloc(1, sizeof(vfs_mount_t));
            if (new_root == NULL) {
                fs_type->destroy_superblock(fs_type, superblock);
                return false;
            }

            new_root->element = strndup("", 2);
            if (new_root->element == NULL) {
                kfree(new_root);
                fs_type->destroy_superblock(fs_type, superblock);
                return false;
            }

            new_root->mounts = list_init();
            if (new_root->mounts == NULL) {
                kfree(new_root->element);
                kfree(new_root);
                fs_type->destroy_superblock(fs_type, superblock);
                return false;
            }

            new_root->superblock = superblock;

            kref_init(&new_root->__ref);

            vfs_root_mount = new_root;

            return true;
        } else {
            fs_type->destroy_superblock(fs_type, superblock);
            return false;
        }
    } else {
        /* TODO: implement */

        fs_type->destroy_superblock(fs_type, superblock);
        return false;
    }

    return true;
}

/* ========== helpers for reference counting ========== */
vfs_inode_t *vfs_inode_init(vfs_fs_superblock_t *superblock) {
    vfs_inode_t *inode = kcalloc(1, sizeof(vfs_inode_t));
    if (inode == NULL) {
        return NULL;
    }

    kref_init(&inode->__ref);

    inode->superblock = superblock;

    if (superblock != NULL) {
        if (superblock->inodes != NULL) {
            list_append_value(superblock->inodes, inode);
        }
    }

    return inode;
}

static void vfs_inode_destroy(void *_inode) {
    vfs_inode_t *inode = _inode;

    if (inode->superblock != NULL) {
        if (inode->superblock->inodes != NULL) {
            list_remove_value(inode->superblock->inodes, inode);
        }
    }

    if (inode->destroy != NULL) {
        inode->destroy(inode);
    }

    kfree(inode);
}

void vfs_inode_up(vfs_inode_t *inode) {
    assert(inode != NULL);
    kref_up(&inode->__ref);
}

void vfs_inode_down(vfs_inode_t *inode) {
    assert(inode != NULL);
    kref_down(&inode->__ref, vfs_inode_destroy, inode);
}

/* XXX: "consumes" callers reference count on inode */
static vfs_file_t *vfs_inode_open(vfs_inode_t *inode) {
    vfs_file_t *file = kcalloc(1, sizeof(vfs_file_t));
    if (file == NULL) {
        return NULL;
    }

    file->inode = inode;
    file->seek = 0;
    file->operations = inode->file_operations;
    kref_init(&file->__ref);
    return file;
}


static void vfs_file_destroy(void *_file) {
    vfs_file_t *file = _file;

    vfs_inode_t *inode = file->inode;

    vfs_inode_down(inode);

    kfree(file);
}

void vfs_file_up(vfs_file_t *file) {
    assert(file != NULL);
    kref_up(&file->__ref);
}

void vfs_file_down(vfs_file_t *file) {
    assert(file != NULL);
    kref_down(&file->__ref, vfs_file_destroy, file);
}

/* ========== path / elements list helpers ========== */
/* replace every occurence of '/' with 0 add '\0\0' to the end
 * XXX: caller has to free return value if not NULL
 * */
__attribute__((used))
static char *path_tokenize(const char *path, size_t len) {
    assert(path != NULL);

    const size_t path_len = strnlen(path, len);
    char *v = kmalloc(sizeof(char) * (path_len + 2));
    if (v == NULL) {
        return NULL;
    }

    size_t v_i = 0;
    size_t path_i = 0;

    while (path_i < path_len) {
        if (path[path_i] == '/') {
            /* skip multiple / and leading / */
            if ((v_i != 0) && (v[v_i - 1] != 0)) {
                v[v_i] = 0;
                v_i++;
            }
            path_i++;
        } else {
            v[v_i] = path[path_i];
            v_i++;
            path_i++;
        }
    }

    /* fill remaining with '\0' */
    for (; v_i < path_len + 2; v_i++) {
        v[v_i] = 0;
    }

    return v;
}

static list_t *push_path_on_list(list_t *list, const char *path, size_t path_len) {
    assert(list != NULL);
    assert(path != NULL);

    const size_t initial_list_length = list->length;

    char *path_tokens = path_tokenize(path, path_len);
    if (path_tokens == NULL) {
        return NULL;
    }

    char *element = path_tokens;

    while (*element != 0) {
        const size_t element_size = strnlen(element, -1) + 1;
        if ((element[0] == '.') && (element[1] == '.') && (element[2] == 0)) {
            /* .. */
        } else if ((element[0] == '.') && (element[1] == 0)) {
            /* do nothing */
            ;
        } else {
            char *v = strndup(element, element_size);

            if (v == NULL) {
                /* FIXME: implement helper */
                while (list->length > initial_list_length) {
                    list_node_t *node = list_remove(list, list->tail);
                    assert(node != NULL);
                    kfree(node->value);
                    list_node_free(node);
                }

                return NULL;
            }
            list_append_value(list, v);
        }

        element += element_size;
    }

    return list;
}

__attribute__((used))
static list_t *path_to_canonical_list(const char *parent, const char *path, size_t path_len) {
    assert(parent != NULL);
    assert(path != NULL);

    list_t *elements = list_init();
    if (elements == NULL) {
        return NULL;
    }

    if ((strnlen(path, -1) > 0) && (path[0] != '/')) {
        /* TODO: relative path */
    }

    list_t *v = push_path_on_list(elements, path, path_len);
    if (v == NULL) {
        list_free(elements);
    }

    return v;
}

/* ========== kopen helpers ========== */
static __attribute__((pure)) vfs_mount_t *vfs_mount_get(const vfs_mount_t *mount, const char *element) {
    const size_t element_len = strnlen(element, -1);

    for list_each(node, mount->mounts) {
        vfs_mount_t *submount = node->value;
        if (strncmp(submount->element, element, element_len) == 0) {
            return submount;
        }
    }

    return NULL;
}

/* find the closest mount, move elements from elements_in to elements_out
 * as they are resolved */
static vfs_mount_t *walk_mount_graph(vfs_mount_t *root, list_t *elements_in, list_t *elements_out) {
    vfs_mount_t *v = root;
    vfs_mount_t *mount = v;

    for list_each(element, elements_in) {
        vfs_mount_t *new_mount = vfs_mount_get(mount, element->value);
        if (new_mount == NULL) {
            break;
        }

        mount = new_mount;

        /* only return "full" resolves */
        if (mount->superblock != NULL) {
            v = mount;
        }

        list_append(elements_out, list_dequeue_node(elements_in));
    }

    return v;
}

static vfs_inode_t *walk_inode_graph_simple(vfs_inode_t *root, list_t *elements_in, list_t *elements_out) {
    vfs_inode_t *inode = root;
    vfs_inode_up(inode);

    for list_each(element, elements_in) {
        assert(inode->operations != NULL);

        if ((inode->operations == NULL) || (inode->operations->lookup == NULL)) {
            break;
        }

        vfs_inode_t *next = inode->operations->lookup(inode, element->value);
        if (next == NULL) {
            break;
        }

        /* FIXME: clarify if lookup is supposed to up */
        vfs_inode_up(next);

        vfs_inode_down(inode);
        inode = next;

        list_append(elements_out, list_dequeue_node(elements_in));
    }

    return inode;
}

static vfs_inode_t *walk_inode_graph(list_t *elements_in, list_t *elements_out) {
    vfs_inode_t *inode = NULL;

    for (size_t i = 0; (i < 10) && (elements_in->length > 0); i++) {
        /* 1. find "closest" known mountpoint */
        vfs_mount_t *mount = walk_mount_graph(vfs_root_mount, elements_in, elements_out);
        inode = mount->superblock->root_inode;
        vfs_inode_up(inode);

        /* 2. find the nearest inode on the filesystem */
        vfs_inode_t *next = walk_inode_graph_simple(inode, elements_in, elements_out);
        assert(next != NULL);
        vfs_inode_down(inode);
        inode = next;

        /* 3. handle special cases: symlinks */
    }

    assert(inode != NULL);
    return inode;
}

/* ========== high-level kernel api ========== */
vfs_file_t *vfs_kopen(const char *path) {
    assert(path != NULL);

    vfs_inode_t *inode = NULL;

    assert(vfs_root_mount);

    if (memcmp(path, "/", 2) == 0) {
        if (vfs_root_mount != NULL) {
            inode = vfs_root_mount->superblock->root_inode;
            vfs_inode_up(inode);
        }
    } else {
        list_t *elements_out = list_init();
        if (elements_out == NULL) {
            return NULL;
        }

        list_t *elements_in = path_to_canonical_list("/", path, strnlen(path, -1));
        if (elements_in == NULL) {
            return NULL;
        }

        inode = walk_inode_graph(elements_in, elements_out);

        if ((elements_in->length > 0) && (inode != NULL)) {
            /* we didn't actually find what was requested */
            vfs_inode_down(inode);
            inode = NULL;
        }

        /* FIXME: move to list.c */
        while (elements_out->length > 0) {
            list_node_t *node = list_dequeue_node(elements_out);
            kfree(node->value);
            list_node_free(node);
        }
        while (elements_in->length > 0) {
            list_node_t *node = list_dequeue_node(elements_in);
            kfree(node->value);
            list_node_free(node);
        }
        list_free(elements_in);
        list_free(elements_out);
    }

    if (inode == NULL) {
        return NULL;
    }
    return vfs_inode_open(inode);
}

void vfs_kclose(vfs_file_t *file) {
    assert(file != NULL);

    vfs_file_down(file);
}

vfs_dirent_t *vfs_file_readdir(vfs_file_t *file, size_t index) {
    assert(file != NULL);
    assert(file->operations != NULL);
    assert(file->operations->readdir != NULL);
    return file->operations->readdir(file, index);
}

size_t vfs_file_read_raw(vfs_file_t *file, char *buffer, uintptr_t offset, size_t length) {
    assert(file != NULL);
    assert(file->operations != NULL);
    assert(file->operations->read_raw != NULL);
    return file->operations->read_raw(file, buffer, offset, length);
}
