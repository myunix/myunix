/* all kernel headers */
#include <kernel.h>
#include <kheap.h>
#include <kprintf.h>
#include <kref.h>
#include <list.h>
#include <mem/pmm.h>
#include <mem/vmm.h>
#include <process.h>
#include <scheduler.h>
#include <serial.h>
#include <string.h>
#include <timer.h>
#include <thread.h>
#include <vfs.h>
#include <vga_console.h>

/* arch specific headers */
#include <multiboot.h>
#include <boot.h>

/* headers supplied by the c compiler / "standard" headers */
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdnoreturn.h>

/* This beast iterates over every multiboot tag, and implements all requirements (size of tag checking, alignment)*/
#define multiboot_each_tag(tag, mbi) (\
        const struct multiboot_tag *(tag) = (void *)((uintptr_t)(mbi) + sizeof(struct multiboot_info)); \
        ((uintptr_t)(tag) + MAX(sizeof((tag)), (tag)->size)) <= ((uintptr_t)(mbi) + (mbi)->total_size); \
        (tag) = (const void *)((uintptr_t)(tag) + ALIGN((tag)->size, 8))\
        )

/* iterate over each mmap entry in a tag_mmap memory map */
#define mmap_each(v, tag_mmap) (\
        const struct multiboot_mmap_entry *(v) = (const void *)(tag_mmap)->entries; \
        (uintptr_t)(v) + sizeof((v)) < (uintptr_t)(tag_mmap) + (tag_mmap)->size; \
        (v) = (const void *)((uintptr_t)(v) + (tag_mmap)->entry_size) \
        )

static __attribute__((const)) inline bool region_overlaps(uintptr_t a1, uintptr_t a2, uintptr_t b1, uintptr_t b2) {
    return MIN(a2, b2) > MAX(a1, b1);
}

/* try to find a possible allocation, that won't overwrite important stuff
 * returns 0 if this block wasn't possible
 * the allocation will be aligned on @alloc_align
 * XXX: you can only use this function for allocations ONCE
 */
static __attribute__((pure)) uintptr_t check_block(uintptr_t block_start, size_t block_size, size_t alloc_size, size_t alloc_align, const struct multiboot_info *mbi, const struct multiboot_tag_mmap *tag_mmap) {
    /* don't allocate below @min_addr */
    const uintptr_t min_addr = 0x100000;

    const uintptr_t block_end = block_start + block_size;

    /* possible allocation */
    uintptr_t alloc_start = ALIGN(MAX(min_addr, block_start), alloc_align);

    /* check if our allocation overlaps with anything important, if it does
     * adjust the allocation and restart
     * */
    bool recheck;/* = false;*/
    do {
        const uintptr_t alloc_end = ALIGN(alloc_start + alloc_size, alloc_align);
        if (alloc_end > block_end) {
            /* the remaining block is too small to hold our allocation, it has been eliminated */
            return 0;
        }
        recheck = false;

        /* 1. kernel */
        const uintptr_t kernel_start = ALIGN_DOWN(__kernel_region_start, alloc_align);
        const uintptr_t kernel_end = ALIGN(__kernel_region_end, alloc_align);
        if (region_overlaps(alloc_start, alloc_end, kernel_start, kernel_end)) {
            alloc_start = kernel_end;
            recheck = true;
            continue;
        }

        /* 2. multiboot information */
        const uintptr_t mbi_start = ALIGN_DOWN((uintptr_t)mbi, alloc_align);
        const uintptr_t mbi_end = ALIGN((uintptr_t)mbi + mbi->total_size, alloc_align);
        if (region_overlaps(alloc_start, alloc_end, mbi_start, mbi_end)) {
            alloc_start = mbi_end;
            recheck = true;
            continue;
        }

        /* 3. memory map */
        for mmap_each(mmap, tag_mmap) {
            if (mmap->type == MULTIBOOT_MEMORY_AVAILABLE) {
                continue;
            }

            const uintptr_t mmap_start = ALIGN_DOWN((uintptr_t)mmap->addr, alloc_align);
            const uintptr_t mmap_end = ALIGN((uintptr_t)(mmap->addr + mmap->len), alloc_align);
            if (region_overlaps(alloc_start, alloc_end, mmap_start, mmap_end)) {
                alloc_start = mmap_end;
                recheck = true;
                break;
            }
        }

        if (recheck) {
            continue;
        }

        /* 4. modules */
        for multiboot_each_tag(tag, mbi) {
            if (tag->type != MULTIBOOT_TAG_TYPE_MODULE) {
                continue;
            }

            const struct multiboot_tag_module *tag_module = (const void *)tag;
            assert(tag->size >= sizeof(tag_module));

            const uintptr_t mod_start = ALIGN_DOWN(tag_module->mod_start, alloc_align);
            const uintptr_t mod_end = ALIGN(tag_module->mod_end, alloc_align);
            if (region_overlaps(alloc_start, alloc_end, mod_start, mod_end)) {
                alloc_start = mod_end;
                recheck = true;
                break; /* XXX: break inner loop, since continue won't work here */
            }
        }
    } while (recheck);

    return alloc_start;
}

static void kernel_set_region(uintptr_t region_start, uintptr_t region_end, const char *name, bool used) {
    kprintf("pmm region [%p - %p] %s (%s)\n", (void *)region_start, (void *)region_end, used ? "used" : "free", name);
    pmm_set_region(region_start, region_end - region_start, used);
}

static void kernel_map_region(uintptr_t region_start, uintptr_t region_end, const char *name, bool write) {
    kprintf("vmm map direct %8s: %p - %p %s\n", name, (void *)region_start, (void *)region_end, write ? "write" : "read-only");
    kernel_early_vmm_map_direct_region(region_start, region_end - region_start, write);
}

thread_t *kernel_thread;
thread_t *kernel_thread2;

noreturn
void kernel_thread_func(void) {
    assert(current_thread == kernel_thread);
    // assert(cpu_disable_interrupts() == true);

    kprintf("Hello world!\n");
    yield();
    yield();

    kprintf("thread 1!\n");
    yield();

    cpu_enable_interrupts();

    while (1) {
        /* cirtical section */
        scheduler_lock();
        kprintf("1");
        scheduler_unlock();

        for (int i = 0; i < 10000; i++) {
            __asm__ __volatile__("nop\nnop\nnop");
        };
    }
}

noreturn
void kernel_thread_func2(void) {
    assert(current_thread == kernel_thread2);
    kprintf("task 2!\n");

    yield();

    kprintf("task 2 again!\n");

    yield();

    cpu_enable_interrupts();

    while (1) {
        /* cirtical section */
        scheduler_lock();
        kprintf("2");
        scheduler_unlock();

        for (int i = 0; i < 10000; i++) {
            __asm__ __volatile__("nop\nnop\nnop");
        };
    }
}

noreturn void user_thread_func(void) {
    __asm__ __volatile__("int $128" : : "a"(2), "b"('@'));
    while (1) {
        __asm__ __volatile__ ("nop");
    }
}

/* tell sparse that we really need this function, even if there is no header file */
noreturn void __attribute__((used)) kmain(uintptr_t, uintptr_t, uintptr_t);

noreturn void __attribute__((used)) kmain(uintptr_t multiboot_info, uintptr_t eax, uintptr_t esp) {
    /* #lizard forgives the complexity */
    (void)esp;

    /* XXX: we don't have a console yet, but if any of these fail, we're done for anyway */
    if (eax != MULTIBOOT2_BOOTLOADER_MAGIC) {
        cpu_halt("not booted by multiboot2!");
    }

    struct serial_dev com0;
    serial_init(&com0, SERIAL_COM0_BASE_PORT);
    kprintf_set_debug_serial(&com0);
    kprintf("multiboot_info=%p uintptr_t eax=%p uintptr_t esp=%p\n", (void *)multiboot_info, (void *)eax, (void *)esp);

    idt_init();
    tss_init();

    if (multiboot_info % 0x8 != 0) {
        cpu_halt("multiboot2 info struct misaligned!");
    }

    const struct multiboot_info *mbi = (void *)multiboot_info;
    if (mbi == NULL) {
        cpu_halt("No multiboot2 info struct provided!");
    }

    const struct multiboot_tag_framebuffer *tag_fb = NULL;

    for multiboot_each_tag(tag, mbi) {
        if (tag->type != MULTIBOOT_TAG_TYPE_FRAMEBUFFER) {
            continue;
        }
        tag_fb = (const void *)tag;
        assert(tag->size >= sizeof(tag_fb));
    }

    if (tag_fb != NULL) {
        /* TODO: implement all framebuffer types */
        assert(tag_fb->common.framebuffer_type == MULTIBOOT_FRAMEBUFFER_TYPE_EGA_TEXT);

        assert(tag_fb->common.framebuffer_bpp % 8 == 0);
        vga_console_init(tag_fb->common.framebuffer_addr,
                tag_fb->common.framebuffer_width,
                tag_fb->common.framebuffer_height,
                tag_fb->common.framebuffer_bpp / 8,
                tag_fb->common.framebuffer_pitch);
        kprintf("vga text console (addr %p; width %#.8x; height %#.8x; bpp %zu; pitch %#.8x)\n",
                (void *)(uintptr_t)tag_fb->common.framebuffer_addr,
                tag_fb->common.framebuffer_width,
                tag_fb->common.framebuffer_height,
                tag_fb->common.framebuffer_bpp,
                tag_fb->common.framebuffer_pitch);
    }
    const size_t framebuffer_mem_size = tag_fb == NULL ? 0 : tag_fb->common.framebuffer_pitch * (tag_fb->common.framebuffer_width + 1);


    kprintf("multiboot information at %p (size: 0x%.8x, reserved: 0x%.8x)\n", mbi, mbi->total_size, mbi->reserved);

    const uintptr_t region_kernel_start = ALIGN_DOWN(__kernel_region_start, BLOCK_SIZE);
    const uintptr_t region_kernel_end = ALIGN(__kernel_region_end, BLOCK_SIZE);
    const uintptr_t region_mbi_start = ALIGN_DOWN(multiboot_info, BLOCK_SIZE);
    const uintptr_t region_mbi_end = ALIGN(multiboot_info + mbi->total_size, BLOCK_SIZE);

    /* we now need to find the multiboot mmap entry  */
    const struct multiboot_tag_mmap *tag_mmap = NULL;
    for multiboot_each_tag(tag, mbi) {
        if (tag->type == MULTIBOOT_TAG_TYPE_MMAP) {
            if (tag_mmap != NULL) {
                kprintf("multiboot provided multiple memory maps, only using the last one!\n");
            }
            tag_mmap = (const void *)tag;
            assert(tag->size >= sizeof(tag_mmap));
        }
    }

    if (tag_mmap == NULL) {
        kprintf("multiboot didn't provide us with a memory map, there is nothing we can do\n");
        assert(0);
    }

    uint64_t highest_memory_address = 0;
    for mmap_each(mmap, tag_mmap) {
        kprintf("mmap addr: 0x%.8x%.8x size: 0x%.8x%.8x type: %u\n",
                (uint32_t)(mmap->addr >> 32),
                (uint32_t)(mmap->addr & 0xFFFFFFFF),
                (uint32_t)(mmap->len >> 32),
                (uint32_t)(mmap->len & 0xFFFFFFFF),
                mmap->type);
        highest_memory_address = MAX(highest_memory_address, mmap->addr + mmap->len);
    }
    assert(highest_memory_address != 0);

    size_t bitmap_size = (highest_memory_address + BLOCK_SIZE - 1) / BLOCK_SIZE / 8;
    kprintf("pmm bitmap size is 0x%.8zx (highest memory_address: 0x%.8x%.8x)\n",
            bitmap_size,
            (uint32_t)(highest_memory_address >> 32),
            (uint32_t)(highest_memory_address & 0xFFFFFFFF));

    uintptr_t bitmap_ptr = 0;
    for mmap_each(mmap, tag_mmap) {
        if (mmap->type != MULTIBOOT_MEMORY_AVAILABLE) {
            continue;
        }

        /* check if this would be a possible allocation */
        uintptr_t ptr = check_block(mmap->addr, mmap->len, bitmap_size, BLOCK_SIZE, mbi, tag_mmap);
        if (ptr != 0) { /* if it is, we're done */
            bitmap_ptr = ptr;
            break;
        }
    }
    kprintf("[pmm] bitmap is at %p size: %#zx\n", (void *)bitmap_ptr, bitmap_size);
    pmm_init(bitmap_ptr, bitmap_size);

    /* populate the PMM bitmap: free memory regions */
    for mmap_each(mmap, tag_mmap) {
        if (mmap->type != MULTIBOOT_MEMORY_AVAILABLE) {
            continue;
        }

        /* if start == 0 round it up to the next block */
        const uintptr_t start = ALIGN(MAX((uintptr_t)mmap->addr, (multiboot_uint64_t)1), BLOCK_SIZE);
        const uintptr_t end = ALIGN_DOWN((uintptr_t)mmap->addr + (size_t)mmap->len, BLOCK_SIZE);
        kernel_set_region(start, end, "mmap", false);
    }

    /* populate the PMM bitmap: non-free memory regions */
    for mmap_each(mmap, tag_mmap) {
        if (mmap->type == MULTIBOOT_MEMORY_AVAILABLE) {
            continue;
        }

        /* if start == 0 round it up to the next block */
        const uintptr_t start = ALIGN_DOWN(MAX((uintptr_t)mmap->addr, (multiboot_uint64_t)1), BLOCK_SIZE);
        const uintptr_t end = ALIGN((uintptr_t)mmap->addr + (size_t)mmap->len, BLOCK_SIZE);
        kernel_set_region(start, end, "mmap", true);
    }

    if (tag_fb != NULL) {
        /* mark everything important as used */
        kernel_set_region(ALIGN_DOWN((uintptr_t)tag_fb->common.framebuffer_addr, BLOCK_SIZE),
            ALIGN((uintptr_t)tag_fb->common.framebuffer_addr + framebuffer_mem_size, BLOCK_SIZE), "fb", true);
    }

    kernel_set_region(region_kernel_start, region_kernel_end, "kernel", true);
    kernel_set_region(region_mbi_start, region_mbi_end, "mbi", true);
    kernel_set_region(bitmap_ptr, bitmap_ptr + bitmap_size, "pmm", true);
    for multiboot_each_tag(tag, mbi) {
        if (!(tag->type == MULTIBOOT_TAG_TYPE_MODULE)) {
            continue;
        }
        const struct multiboot_tag_module *tag_module = (const void *)tag;
        if (tag->size >= sizeof(tag_module)) {
            const uintptr_t start = ALIGN_DOWN(tag_module->mod_start, BLOCK_SIZE);
            const uintptr_t end = ALIGN(tag_module->mod_end, BLOCK_SIZE);
            kernel_set_region(start, end, "kmod", true);
        }
    }

    kprintf(" => %zukb free\n", pmm_count_free_blocks() * BLOCK_SIZE / 1024);

    /* we can now freely allocate physical memory */

    vmm_init();

    /* read-only: .text and multiboot information */
    kernel_map_region(ALIGN_DOWN((uintptr_t)mbi, PAGE_SIZE),
            ALIGN((uintptr_t)mbi + (size_t)mbi->total_size, PAGE_SIZE), "mbi", false);

    kernel_map_region(ALIGN_DOWN(__kernel_region_text_start, PAGE_SIZE),
            ALIGN(__kernel_region_text_end, PAGE_SIZE), ".text", false);

    /* read-write: .data .bss framebuffer pmm_bitmap */
    kernel_map_region(ALIGN_DOWN(bitmap_ptr, PAGE_SIZE),
            ALIGN(bitmap_ptr + bitmap_size, PAGE_SIZE), "pmm", true);

    kernel_map_region(ALIGN_DOWN(__kernel_region_data_start, PAGE_SIZE),
            ALIGN(__kernel_region_data_end, PAGE_SIZE), ".data", true);

    kernel_map_region(ALIGN_DOWN(__kernel_region_bss_start, PAGE_SIZE),
            ALIGN(__kernel_region_bss_end, PAGE_SIZE), ".bss", true);

    if (tag_fb != NULL) {
        /* map the VGA framebuffer */
        kernel_map_region(ALIGN_DOWN(tag_fb->common.framebuffer_addr, PAGE_SIZE),
                ALIGN(tag_fb->common.framebuffer_addr + framebuffer_mem_size, PAGE_SIZE), "fb", true);
    }

    /* enable paging, if something above didn't work correctly, the machine will most
     * likely triple fault and reboot */
    vmm_finalize();

    for multiboot_each_tag(tag, mbi) {
        if (tag->type == MULTIBOOT_TAG_TYPE_CMDLINE) {
            const struct multiboot_tag_string *string_tag = (const void *)tag;
            if (tag->size >= sizeof(string_tag)) {
                kprintf("cmdline: %s\n", string_tag->string);
            }
        } else if (tag->type == MULTIBOOT_TAG_TYPE_BOOT_LOADER_NAME) {
            const struct multiboot_tag_string *string_tag = (const void *)tag;
            if (tag->size >= sizeof(string_tag)) {
                kprintf("bootloader name: %s\n", string_tag->string);
            }
        } else if (tag->type == MULTIBOOT_TAG_TYPE_MODULE) {
            const struct multiboot_tag_module *tag_module = (const void *)tag;
            if (tag->size >= sizeof(tag_module)) {
                kprintf("module(type: %#x, size: %#x): [%p - %p]: cmdline: '%s'\n", tag_module->type, tag_module->size, (void *)(uintptr_t)tag_module->mod_start, (void *)(uintptr_t)tag_module->mod_end, tag_module->cmdline);
            }
        }
    }

    kheap_init();

    {
        /* kheap tests */

        kprintf("========= kmalloc(%lu) ==========\n", PAGE_SIZE * 16);
        void *p3 = kmalloc(PAGE_SIZE * 16);
        kprintf("p3 = %p\n", p3);

        kprintf("========= kfree() ==========\n");
        kfree(p3);
    }

    {
        kprintf("========== kheap stress test ==========\n");
        /* kheap "stress" test */
        void **pts = kcalloc(256, sizeof(void *));
        assert(pts);

        for (int i = 0; i < 256; i++) {
            if (i == 0) {
                pts[i] = kmalloc(256);
            } else {
                pts[i] = kmalloc(i*i);
            }
            assert(pts[i]);
        }

        for (int i = 0; i < 256; i++) {
            kfree(pts[i]);
        }
    }

    vfs_init();

    timer_init(1000);

    cpu_enable_interrupts();

    scheduler_init();


    process_t * const user_proc = process_init();
    assert(user_proc != NULL);

    kernel_thread = thread_init();
    assert(kernel_thread != NULL);

    kernel_thread2 = thread_init();
    assert(kernel_thread2 != NULL);

    thread_setup_kernel_task(kernel_thread, kernel_thread_func);
    thread_setup_kernel_task(kernel_thread2, kernel_thread_func2);


    /*  */
    page_directory_t * const user_pdir = user_proc->pdir;
    assert(user_pdir != NULL);

    vmm_map_region(
            user_pdir,
            ALIGN_DOWN((uintptr_t)&user_thread_func, PAGE_SIZE),
            ALIGN_DOWN((uintptr_t)&user_thread_func, PAGE_SIZE),
            PAGE_SIZE,
            true, true, true, 0
            );

    cpu_disable_interrupts();
    kprintf("user page directory physical address = %p\n", (void *)user_pdir->physical_address);
    cpu_enable_interrupts();

    thread_setup_user(user_proc->thread, 0xFEE1DEAD, (uintptr_t)&user_thread_func, user_proc->pdir->physical_address);

    scheduler_add_thread(kernel_thread);
    scheduler_add_thread(kernel_thread2);
    scheduler_add_thread(user_proc->thread);

    scheduler_enable();

    assert(0);
}
