#ifndef __SERIAL_H
#define __SERIAL_H 1

#include <stdint.h>

struct serial_dev {
    uint16_t base_port;
};

#define SERIAL_COM0_BASE_PORT ((uint16_t)0x3F8)

void serial_putc(struct serial_dev *obj, uint8_t c);

void serial_init(struct serial_dev *obj, uint16_t base_port);

#endif
