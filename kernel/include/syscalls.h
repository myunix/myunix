#ifndef __SYSCALLS_H
#define __SYSCALLS_H 1

#include <cpu.h>

void syscall_isr(registers_t *regs);

#endif
