#ifndef __VFS_H
#define __VFS_H 1

#include <kref.h>
#include <list.h>

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define VFS_FS_NAME_MAX_LEN ((size_t)32)
#define VFS_FILE_NAME_MAX_LEN ((size_t)254)

typedef uint64_t vfs_ino;

/**/
typedef struct vfs_fs_type vfs_fs_type_t;
typedef struct vfs_fs_superblock vfs_fs_superblock_t;
typedef struct vfs_mount vfs_mount_t;
typedef struct vfs_inode vfs_inode_t;
typedef struct vfs_inode_operations vfs_inode_operations_t;
typedef struct vfs_file vfs_file_t;
typedef struct vfs_file_operations vfs_file_operations_t;
typedef struct vfs_dirent vfs_dirent_t;

/**/
typedef vfs_fs_superblock_t *(*vfs_get_superblock_f)(vfs_fs_type_t *type, const char *args, vfs_file_t *device);
typedef void (*vfs_destroy_superblock_f)(vfs_fs_type_t *type, vfs_fs_superblock_t *superblock);

struct vfs_fs_type {
    char *name;

    /* called by mount */
    vfs_get_superblock_f get_superblock;
    vfs_destroy_superblock_f destroy_superblock;
};

struct vfs_fs_superblock {
    vfs_fs_type_t *fs_type;
    vfs_inode_t *root_inode;
    vfs_file_t *device;

    /* list of all inodes created by this filesystem: inode->superblock == superblock */
    list_t *inodes;
};

/* ========== mount graph ========= */
struct vfs_mount {
    /* path element of this mouunt tree leave */
    char *element;

    /* filesystem associated, NULL if no mount, but leaves exist */
    vfs_fs_superblock_t *superblock;

    /* list of submounts / leaves */
    list_t *mounts;

    kref_t __ref;
};

extern vfs_mount_t *vfs_root_mount;

/* ========== inode ========== */
vfs_inode_t *vfs_inode_init(vfs_fs_superblock_t *superblock);
void vfs_inode_up(vfs_inode_t *inode);
void vfs_inode_down(vfs_inode_t *inode);

typedef void (*vfs_inode_destroy_f)(vfs_inode_t *inode);

struct vfs_inode {
    vfs_ino ino;

    vfs_fs_superblock_t *superblock;

    /* this should be more or less the same across all inodes of a filesystem */
    vfs_inode_operations_t *operations;

    /* */
    vfs_file_operations_t *file_operations;

    /* filesystem specific value */
    void *fs_value;

    /* called when last reference is downed */
    vfs_inode_destroy_f destroy;

    kref_t __ref;
};

/* lookup a file inside this directoy */
typedef vfs_inode_t *(*vfs_inode_lookup_op_f)(vfs_inode_t *inode, const char *name);

struct vfs_inode_operations {
    vfs_inode_lookup_op_f lookup;
};

/* ========== file ========= */
void vfs_file_up(vfs_file_t *file);
void vfs_file_down(vfs_file_t *file);

struct vfs_file {
    vfs_inode_t *inode;

    size_t seek;

    /* this should be more or less the same across all inodes of a filesystem */
    vfs_file_operations_t *operations;

    kref_t __ref;
};

typedef vfs_dirent_t *(*vfs_file_readdir_op_f)(vfs_file_t *dir, size_t index);
typedef size_t (*vfs_file_read_raw_op_f)(vfs_file_t *file, char *buffer, uintptr_t offset, size_t size);
typedef size_t (*vfs_file_read_op_f)(vfs_file_t *file, char *buffer, size_t size);

typedef struct vfs_file_operations {
    vfs_file_readdir_op_f readdir;
    vfs_file_read_raw_op_f read_raw;
    vfs_file_read_op_f read;
} vfs_file_operations_t;

/* ========== dirent ========= */
struct vfs_dirent {
    vfs_ino ino;

    char name[VFS_FILE_NAME_MAX_LEN + 2];
};

/* XXX: must be called before any other function of this module */
void vfs_init();

bool vfs_register(const char *name, vfs_get_superblock_f get_superblock, vfs_destroy_superblock_f destroy_superblock);
bool vfs_mount_type(const char *type, const char *args, vfs_file_t *device, const char *mountpoint);

vfs_file_t *vfs_kopen(const char *path);
void vfs_kclose(vfs_file_t *file);

/**/
vfs_dirent_t *vfs_file_readdir(vfs_file_t *file, size_t index);
size_t vfs_file_read_raw(vfs_file_t *file, char *buffer, uintptr_t offset, size_t length);

#endif
