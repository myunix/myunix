#ifndef __KERNEL_H
#define __KERNEL_H 1
/* this is the place for stuff that is used EVERYWHERE */

/* we assume that @a is a power of 2 */
#define __ALIGN_MASK(x,mask) (((x) + (mask)) & ~(mask))
#define ALIGN(x,a) __ALIGN_MASK((x), (__typeof__(x))(a) - 1)
#define ALIGN_DOWN(x,a) ALIGN((x) - ((a) - 1), (a))
/* XXX: if you want to use PTR_ALIGN you're responsible to include <stddef.h> */
#define PTR_ALIGN(p,a)  ((__typeof__(p))ALIGN((uintptr_t)(p), (a)))
#define IS_ALIGNED(x,a) (((x) & ((__typeof__(x))(a) - 1)) == 0)

/* cause a built-time error when x is true, if x is false return 0 of type size_t
 * This is useful in macros, where you can't really use assert or static_assert
 */
#define BUILD_BUG_OR_ZERO(x) (sizeof(struct { int: -!!(x); }))

/* a lot of magic, see https://stackoverflow.com/questions/19452971/array-size-macro-that-rejects-pointers for details */
#define __same_type(a, b) __builtin_types_compatible_p(__typeof__(a), __typeof__(b))
#define __must_be_array(x) BUILD_BUG_OR_ZERO(__same_type((x), &(x)[0]))

/* size of an array, useful for for loops, you're responsible for including assert.h */
#define array_size(x) (sizeof((x)) / sizeof((x)[0]) + __must_be_array((x)))

#define MAX(a, b) \
    ({ __typeof__(a) _a = (a); \
       __typeof__(b) _b = (b); \
       _a > _b ? _a : _b; })
#define MIN(a, b) \
    ({ __typeof__(a) _a = (a); \
       __typeof__(b) _b = (b); \
       _a < _b ? _a : _b; })

#endif
