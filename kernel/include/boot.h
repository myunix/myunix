#ifndef __BOOT_H
#define __BOOT_H 1

#include <stdint.h>
#include <stdnoreturn.h>

/* implemented in boot.c */
noreturn void cpu_halt(const char *msg);

/* functions provided by the boot services in arch/ */

/* XXX: use cpu_halt instead */
extern noreturn void _halt(void);

extern noreturn void __stack_chk_fail(void);

/* x86-fmaily */

void tss_init(void);
uintptr_t __attribute__((pure)) tss_get_kstack(void);
void tss_set_kstack(uintptr_t stack);

void idt_init(void);

/* start of .text (and kernel start)*/
extern const void *_start;
#define __kernel_region_start ((uintptr_t)&_start)
#define __kernel_region_text_start ((uintptr_t)&_start)

/* end of .text and beginning of .data */
extern const void *_etext;
#define __kernel_region_text_end ((uintptr_t)&_etext)
#define __kernel_region_text_size ((size_t)(__kernel_region_text_end - __kernel_region_text_start))
#define __kernel_region_data_start ((uintptr_t)&_etext)

/* end of .data and beginning of .bss */
extern const void *_edata;
#define __kernel_region_data_end ((uintptr_t)&_edata)
#define __kernel_region_data_size ((size_t)(__kernel_region_data_end - __kernel_region_data_start))
#define __kernel_region_bss_start ((uintptr_t)&_edata)

/* end of .bss (end of kernel) */
extern const void *_end;
#define __kernel_region_bss_end ((uintptr_t)&_end)
#define __kernel_region_bss_size ((size_t)(__kernel_region_bss_end - __kernel_region_bss_start))
#define __kernel_region_end ((uintptr_t)&_end)
#define __kernel_region_size ((size_t)(__kernel_region_end - __kernel_region_start))

/* code shared between kernel and user space */
extern const void *__shared_text_start;
extern const void *__shared_text_end;
#define __kernel_region_shared_text_start ((uintptr_t)&__shared_text_start)
#define __kernel_region_shared_text_end ((uintptr_t)&__shared_text_end)
#define __kernel_region_shared_text_size ((size_t)(__kernel_region_shared_text_end - __kernel_region_shared_text_start))

#endif
