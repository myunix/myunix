#ifndef __SCHEDULER_H
#define __SCHEDULER_H 1

#include <thread.h>

void scheduler_add_thread(thread_t *thread);

noreturn void scheduler_exit_thread(void);

void yield(void);

void scheduler_init(void);
noreturn void scheduler_enable(void);

void scheduler_lock(void);
void scheduler_unlock(void);

void scheduler_timer_wakeup(void);

#endif
