#ifndef __KHEAP_H
#define __KHEAP_H 1

#include <stddef.h>

void kheap_init(void);

__attribute__((malloc))
    __attribute__((alloc_size(1)))
    void *kmalloc(size_t size);

__attribute__((malloc))
    __attribute__((alloc_size (1)))
    __attribute__((alloc_align(2)))
    void *kmalloc_aligned(size_t size, size_t alignment);

__attribute__((malloc))
    __attribute__((alloc_size(1, 2)))
    void *kcalloc(size_t nobj, size_t size);

void kfree(void *ptr);

#endif
