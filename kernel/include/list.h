#ifndef __LIST_H
#define __LIST_H 1

#include <stddef.h>

typedef struct list_node {
    struct list_node *next;
    struct list_node *prev;
    void *value;
    struct list *owner;
} list_node_t;

typedef struct list {
    list_node_t *head;
    list_node_t *tail;
    size_t length;
} list_t;

list_t *list_init(void);
void list_free(list_t *list);

list_node_t *list_node_init(void *value);
void *list_node_free(list_node_t *node);

void list_append(list_t *list, list_node_t *node);
list_node_t *list_append_value(list_t *list, void *value);

list_node_t *list_remove(list_t *list, list_node_t *node);
void *list_remove_value(list_t *list, void *value);

void assert_list(const list_t *list);

list_node_t *list_dequeue_node(list_t *list);
void *list_dequeue(list_t *list);

list_node_t *list_queue(list_t *list, void *value);

#define list_each(v, list) (list_node_t *v = (list)->head; v != NULL; v = v->next)

#endif
