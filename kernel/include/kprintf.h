#ifndef __KPRINTF_H
#define __KPRINTF_H 1

#include <stdarg.h>
#include <stddef.h>

#include <serial.h>

size_t __attribute__((nonnull)) vcprintf(const char *fmt, size_t (*callback)(const char *s, size_t len), va_list args);

void kprintf_set_debug_serial(struct serial_dev *dev);
size_t kprintf_callback(const char *s, size_t l);

/* XXX: kprintf will panic if you try to feed it a malformed format string */
size_t __attribute__((format(printf,1,2))) kprintf(const char *fmt, ...);

#endif
