#ifndef __STACKTRACE_H
#define __STACKTRACE_H 1

/* implemented by kernel/arch/$arch */
void stacktrace_print(void);

#endif
