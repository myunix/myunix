#ifndef __ASSERT_H
#define __ASSERT_H

#include <stdnoreturn.h>

#ifdef __FRAMAC__
#define static_assert(exp, msg) typedef char __static_assert_##__FILE__##__LINE__[((exp) ? 1 : -1)]
#else
#define static_assert _Static_assert
#endif

noreturn void __assert_failed(const char * restrict msg, const char * restrict file, int line);

#define assert(exp) (void)((exp) ? ((void) 0) : __assert_failed(#exp, __FILE__, __LINE__))

#endif
