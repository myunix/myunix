#ifndef __THREAD_H
#define __THREAD_H 1

#include <cpu.h>

#include <stddef.h>
#include <stdnoreturn.h>

enum thread_state {
    THREAD_STATE_READY = 0,
    THREAD_STATE_RUNNING = 1,
    THREAD_STATE_TERMINATED = 2,
};

typedef struct thread_cpu_state {
    uintptr_t stack_pointer;
} thread_cpu_state_t;

typedef struct thread {
    thread_cpu_state_t cpu_state;
    enum thread_state state;

    uintptr_t kernel_stack;
} thread_t;

extern thread_t *current_thread;

/* allocate / free a stack in kernel space of a specific size
 * this also allocates 2 leading and trailing guard-pages that are not mapped
 * to prevent a stack over/under flow */
uintptr_t thread_kernel_stack_alloc(size_t size);
void thread_kernel_stack_free(uintptr_t stack, size_t size);

void thread_setup_user(thread_t * const thread, uintptr_t stack_pointer, uintptr_t instruction_pointer, uintptr_t page_directory_phyiscal_address);

typedef void kernel_thread_entrypoint_f(void);
void thread_setup_kernel_task(thread_t * const thread, kernel_thread_entrypoint_f * const entrypoint);

thread_t *thread_init(void);
void thread_destroy(thread_t *thread);

#endif
