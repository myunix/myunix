#ifndef __MEM_VMM_H
#define __MEM_VMM_H 1

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include <paging.h>

/* XXX: this needs to be called before any call to kernel_early_vmm_* */
void vmm_init(void);

/* enables pageing
 * XXX: after this kernel_early_vmm_* may not be called! */
void vmm_finalize(void);

/* XXX: you normally shouldn't have to use this directly */
extern page_directory_t *kernel_directory;

/**/
page_directory_t *page_directory_init(void);
void page_directory_up(page_directory_t *pdir);
void page_directory_down(page_directory_t *pdir);

/* XXX: these may only be used between the call to vmm_init() and vmm_finalize()
 * map a page into kernel space while paging is still disabled (ie. assume that the
 * kernel page directory isn't active and access all tables by their physical address) */
void kernel_early_vmm_map(uintptr_t vaddr, uintptr_t paddr, bool present, bool write);
void kernel_early_vmm_map_direct(uintptr_t addr, bool write);
void kernel_early_vmm_map_direct_region(uintptr_t start, size_t size, bool write);

/* XXX: don't use these on an active (kernel) page directory */
uintptr_t vmm_unmap_page(page_directory_t *pdir, uintptr_t vaddr);
void vmm_map_page(page_directory_t *pdir, uintptr_t vaddr, uintptr_t paddr, bool present, bool write, bool user, unsigned int extra);
void vmm_map_region(page_directory_t *pdir, uintptr_t vaddr, uintptr_t paddr, size_t size, bool present, bool write, bool user, unsigned int extra);

uintptr_t page_directory_find_free_region(page_directory_t *pdir, size_t size);

/* virtual memory management kernel */
void vmmk_map_page(uintptr_t vaddr, uintptr_t paddr, bool present, bool write);
void *vmmk_map_region_random(uintptr_t paddr, size_t size, bool write);

uintptr_t vmmk_unmap_page(uintptr_t vaddr);
void vmmk_unmap_region(uintptr_t vaddr, size_t size);

uintptr_t vmmk_find_free_region(size_t size);
uintptr_t vmmk_get_phys(uintptr_t vaddr);

uintptr_t vmmk_find_dma_region(size_t size);

/* miscellaneous helpers */
void page_directory_debug(const page_directory_t *pdir);

#endif
