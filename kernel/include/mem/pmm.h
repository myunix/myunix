#ifndef __MEM_PM_H
#define __MEM_PM_H 1

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#define BLOCK_SIZE ((size_t)0x1000)

void pmm_init(uintptr_t bitmap_ptr, size_t bitmap_size);

/* @offset has to be aligned to BLOCK_SIZE */
void pmm_set(uintptr_t offset, bool used);

/* @offset has to be aligned to BLOCK_SIZE */
__attribute__((pure)) bool pmm_is_used(uintptr_t offset);

/* @offset and @size have to be aligbed to BLOCK_SIZE */
void pmm_set_region(uintptr_t offset, size_t size, bool used);

__attribute__((pure)) uintptr_t pmm_find_first_free(void);
__attribute__((pure)) uintptr_t pmm_find_free_region(size_t size);

uintptr_t pmm_alloc(size_t size);
void pmm_free(uintptr_t offset, size_t size);

__attribute__((pure)) size_t pmm_count_free_blocks(void);

#endif
