#ifndef __PROCESS_H
#define __PROCESS_H 1

#include <thread.h>
#include <mem/vmm.h>
#include <kref.h>

typedef struct {
    thread_t *thread;

    page_directory_t *pdir;

    kref_t __ref;
} process_t;

process_t *process_init(void);
void process_up(process_t *proc);
void process_down(process_t *proc);

#endif
