#ifndef __KREF_H
#define __KREF_H 1

#include <assert.h>
#include <stddef.h>

typedef size_t kref_t;

#define KREF_STATIC_INIT (1)

static inline void kref_init(kref_t *kref) {
    *kref = 1;
}

static inline void kref_up(kref_t *kref) {
    (*kref)++;
}

typedef void kref_release_f(void *v);

static inline void kref_down(kref_t *kref, kref_release_f *release, void *v) {
    assert(*kref > 0);
    (*kref)--;

    if (*kref == 0) {
        release(v);
    }
}

#endif
