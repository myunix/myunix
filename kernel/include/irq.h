#ifndef __IRQ_H
#define __IRQ_H 1

#include <stdbool.h>

typedef bool (*irq_handler_t) (void);

void irq_add_handler(irq_handler_t handler, unsigned int irq);
void irq_del_handler(irq_handler_t handler, unsigned int irq);

#endif
