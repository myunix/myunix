#ifndef __STRING_H
#define __STRING_H 1

#include <stddef.h>

size_t __attribute__((nonnull)) __attribute__((pure)) strnlen(const char * restrict s, size_t max);

void * __attribute__((nonnull)) memset(void * restrict dest, int c, size_t len);

void * __attribute__((nonnull)) memcpy(void *dest, const void *src, size_t len);

void * __attribute__((nonnull)) memmove(void *dest, const void *src, size_t len);

__attribute__((pure)) int memcmp(const void *a, const void *b, size_t len);

__attribute__((pure)) int strncmp(const char *s1, const char *s2, size_t len);

char *strndup(const char *src, size_t len);

char *strncpy(char *dest, const char *src, size_t len);

#endif
