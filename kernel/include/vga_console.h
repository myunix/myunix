#ifndef __VGA_CONSOLE_H
#define __VGA_CONSOLE_H 1

#include <stddef.h>
#include <stdint.h>

void vga_console_init(uintptr_t vmem, size_t w, size_t h, size_t bpp, size_t pitch);

/* print the character 'c' to the vga console */
void putc(char c);

#endif
