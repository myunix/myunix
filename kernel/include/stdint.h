#ifdef __TINYC__
#include <stddef.h>
#else
/* when using -pendatic tell gcc to ignore the error that #include_next would cause */
#pragma GCC system_header
#include_next <stdint.h>
#endif
