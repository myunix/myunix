#include <kernel.h>
#include <kprintf.h>
#include <vga_console.h>
#include <serial.h>
#include <string.h>

#include <assert.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

// Well, this is really not correct, but it avoids having to implement 64 bit divisions (ideally this would be uintmax_t)
typedef uintptr_t uintbig_t;
#define UINTBIG_MAX UINTPTR_MAX

/* convert an integer to string */
static size_t __attribute__((nonnull)) integer_to_string(char * restrict buf, size_t buf_len, uintbig_t value, uintbig_t base, const char * restrict digits) {
    size_t length = 1;
    uintbig_t c = value;
    while (base <= c) {
        /* figure out how often we can divide */
        c /= base;
        length++;
    }

    length = MIN(length, buf_len);

    /* generate the actual string */
    for (size_t i = length; i != 0; i--) {
        buf[i - 1] = digits[value % base];
        value /= base;
    }

    return length;
}

/* apply padding for a string of length i to length w with character c using putc, no padding if i > w */
static size_t pad(size_t (*callback)(const char *s, size_t len), size_t i, size_t w, char c) {
    if (i > w) {
        return 0;
    }
    size_t l = w - i;
    for (size_t i = 0; i < l; i++) {
        if (callback(&c, 1) != 1) {
            return -1;
        }
    }
    return l;
}

/*
kprintf syntax:
    %[flags][width][.precision][length]type
*/
size_t vcprintf(const char *fmt, size_t (*callback)(const char *s, size_t len), va_list args) {
    /* #lizard forgives the complexity */
    size_t written = 0;

    while (*fmt != 0) {
        if (*fmt != '%') {
            if (callback(fmt, 1) != 1) {
                return -1;
            }
            written++;
            fmt++;
            continue;
        }

        /* we have a format character, now things get interesting */
        fmt++;
        assert(*fmt != 0);
        if (*fmt == '%') {
            /* %% doesn't accept any modifiers types or anything really */
            if (callback("%", 1) != 1) {
                return -1;
            }
            written++;
            fmt++;
            continue;
        }

        /* 1. flags: can be zero or more (in any order) of: `-+ 0#` */
        bool left_align = false;
        bool prepend_plus = false;
        bool prepend_space = false;
        bool prepend_zero = false;
        bool alternate_form = false;
        while (true) {
            assert(*fmt != 0);
            if (*fmt == '-') {
                left_align = true;
                assert(!prepend_zero);
            } else if (*fmt == '+') {
                prepend_plus = true;
            } else if (*fmt == ' ') {
                prepend_space = true;
            } else if (*fmt == '0') {
                prepend_zero = true;
                assert(!left_align);
            } else if (*fmt == '#') {
                alternate_form = true;
            } else {
                break;
            }
            fmt++;
        }

        /* 2. width field  can be '*' or any number */
        size_t width = 0; /* minimum width */
        if (*fmt == '*') {
            width = va_arg(args, unsigned int);
            fmt++;
        } else {
            while ((*fmt >= '0') && (*fmt <= '9')) {
                width = width * 10 + (*fmt - '0');
                fmt++;
            }
        }
        assert(*fmt != 0);

        /* 3. precision field starts with '.' can be '*' or any number */
        size_t precision = (size_t)-1;
        bool use_precision = false;
        if (*fmt == '.') {
            use_precision = true;
            fmt++;
            assert(*fmt != 0);
            if (*fmt == '*') {
                precision = va_arg(args, size_t);
                fmt++;
            } else {
                precision = 0;
                while ((*fmt >= '0') && (*fmt <= '9')) {
                    precision = precision * 10 + (*fmt - '0');
                    fmt++;
                }
            }
        }
        assert(*fmt != 0);

        /* 4. length field */
        enum length {
            LENGTH_DEFAULT,
            LENGTH_CHAR,
            LENGTH_SHORT,
            LENGTH_LONG,
            LENGTH_LONG_LONG,
            LENGTH_LONG_DOUBLE,
            LENGTH_SIZE,
            LENGTH_INTMAX,
            LENGTH_PTRDIFF,
        };

        enum length length_type = LENGTH_DEFAULT;
        if (*fmt == 'h') {
            length_type = LENGTH_SHORT;
            fmt++;
            if (*fmt == 'h') {
                length_type = LENGTH_SHORT;
                fmt++;
            }
        } else if (*fmt == 'l') {
            length_type = LENGTH_LONG;
            fmt++;
            if (*fmt == 'h') {
                length_type = LENGTH_LONG_LONG;
                fmt++;
            }
        } else if (*fmt == 'L') {
            length_type = LENGTH_LONG_DOUBLE;
            fmt++;
        } else if (*fmt == 'z') {
            length_type = LENGTH_SIZE;
            fmt++;
        } else if (*fmt == 'j') {
            length_type = LENGTH_INTMAX;
            fmt++;
        } else if (*fmt == 't') {
            length_type = LENGTH_PTRDIFF;
            fmt++;
        }
        assert(*fmt != 0);

        /* 5. type field */
        if (*fmt == 'c') { /* print a single character */
            assert(!prepend_zero);
            assert(!alternate_form);
            assert(!prepend_space);
            assert(!prepend_plus);

            char c;
            if (length_type == LENGTH_DEFAULT) {
                c = va_arg(args, int);
            } else {
                assert(0);
            }

            if (!left_align) {
                written += pad(callback, 1, width, ' ');
            }

            if (callback(&c, 1) != 1) {
                return -1;
            }
            written++;

            if (left_align) {
                written += pad(callback, 1, width, ' ');
            }
        } else if (*fmt == 's') { /* print a string */
            assert(!prepend_zero);
            assert(!alternate_form);
            assert(!prepend_space);
            assert(!prepend_plus);

            const char *string;
            if (length_type == LENGTH_DEFAULT) {
                string = va_arg(args, const char *);
            } else {
                assert(0);
            }

            if (string == NULL) {
                string = "(null)";
            }

            const size_t string_length = strnlen(string, precision);

            if (!left_align) {
                written += pad(callback, string_length, width, ' ');
            }

            if (callback(string, string_length) != string_length) {
                return -1;
            }
            written += string_length;

            if (left_align) {
                written += pad(callback, string_length, width, ' ');
            }
        } else if (*fmt == 'i' || *fmt == 'd' || *fmt == 'u' || *fmt == 'x' || *fmt == 'X' || *fmt == 'p') { /* integers */
            uintbig_t value;
            char type = *fmt;

            bool negative = false;

            // FIXME: length
            char prefix[10];
            size_t p = 0;

            if (*fmt == 'p') {
                /* print pointers in the format of *0x%.8x */
                value = (uintptr_t)va_arg(args, void *);
                type = 'x';
                prefix[p++] = '*';
                alternate_form = true;
                use_precision = true;
                precision = sizeof(uintptr_t) * 2;
            } else if (type == 'i' || type == 'd') {
                // FIXME: not portable
                int32_t value_signed;

                if ((length_type == LENGTH_DEFAULT) || (length_type == LENGTH_CHAR) || (length_type == LENGTH_SHORT)) {
                    value_signed = va_arg(args, signed int);
                } else if (length_type == LENGTH_LONG) {
                    value_signed = va_arg(args, signed long);
                } else if (length_type == LENGTH_LONG_LONG) {
                    value_signed = va_arg(args, signed long long);
                } else if (length_type == LENGTH_SIZE) {
                    // XXX: apparently ptrdiff_t and ssize_t aren't that different, but most compilers only provide ptrdiff_t
                    value_signed = va_arg(args, ptrdiff_t);
                } else if (length_type == LENGTH_INTMAX) {
                    assert(0);
                } else if (length_type == LENGTH_PTRDIFF) {
                    value_signed = va_arg(args, ptrdiff_t);
                } else {
                    assert(0);
                }

                negative = value_signed < 0;
                if (negative) {
                    value = (uintbig_t)(-value_signed);
                } else {
                    value = (uintbig_t)value_signed;
                }
            } else {
                if ((length_type == LENGTH_DEFAULT) || (length_type == LENGTH_CHAR) || (length_type == LENGTH_SHORT)) {
                    value = va_arg(args, unsigned int);
                } else if (length_type == LENGTH_LONG) {
                    value = va_arg(args, unsigned long);
                } else if (length_type == LENGTH_LONG_LONG) {
                    value = va_arg(args, unsigned long long);
                } else if (length_type == LENGTH_SIZE) {
                    value = va_arg(args, size_t);
                } else if (length_type == LENGTH_INTMAX) {
                    assert(0);
                } else if (length_type == LENGTH_PTRDIFF) {
                    value = (uintbig_t)va_arg(args, ptrdiff_t);
                } else {
                    assert(0);
                }
                assert(!prepend_space);
                assert(!prepend_plus);
            }

            const char* digits = (type == 'X' ? "0123456789ABCDEF" : "0123456789abcdef");
            const unsigned int base = (type == 'x' || type == 'X') ? 16 : (type == 'o' ? 8 : 10);

            if (negative) {
                prefix[p++] = '-';
            } else if (prepend_plus) {
                assert(!prepend_space);
                prefix[p++] = '+';
            } else if (prepend_space) {
                assert(!prepend_plus);
                prefix[p++] = ' ';
            }

            if (alternate_form) {
                if (type == 'x') {
                    prefix[p++] = '0';
                    prefix[p++] = 'x';
                } else if (type == 'X') {
                    prefix[p++] = '0';
                    prefix[p++] = 'X';
                } else if (type == 'o') {
                    prefix[p++] = '0';
                } else {
                    assert(0);
                }
            }

            // FIXME: length
            char buffer[64];
            size_t buffer_length = integer_to_string(buffer, sizeof(buffer), value, base, digits);

            const size_t buffer_length_precision = use_precision ? precision : buffer_length;
            const size_t prefix_length = p;
            const size_t total_length = buffer_length_precision + prefix_length;

            /* 1. print left space padding */
            if (!left_align && !prepend_zero) {
                written += pad(callback, total_length, width, ' ');
            }

            /* 2. print prefix */
            if (callback(prefix, prefix_length) != prefix_length) {
                return -1;
            }
            written += prefix_length;

            /* 3. print zero pad */
            if (prepend_zero) {
                assert(!use_precision);
                assert(!left_align);
                assert(width > 0);
                written += pad(callback, total_length, width, '0');
            } else if (use_precision) {
                written += pad(callback, buffer_length, precision, '0');
            }

            /* 4. print buffer (precision) */
            if (callback(buffer, buffer_length) != buffer_length) {
                return -1;
            }
            written += buffer_length;

            /* 5. print right pad */
            if (left_align) {
                written += pad(callback, total_length, width, ' ');
            }
        } else {
            assert(0);
        }

        fmt++;
    }

    return written;
}


static struct serial_dev *debug_out = NULL;
void kprintf_set_debug_serial(struct serial_dev *dev) {
    assert(debug_out == NULL);
    debug_out = dev;
}

size_t kprintf_callback(const char *s, size_t l) {
    if (debug_out == NULL) {
        for (size_t i = 0; i < l; i++) {
            putc(s[i]);
        }
    } else {
        for (size_t i = 0; i < l; i++) {
            putc(s[i]);
            serial_putc(debug_out, s[i]);
        }
    }
    return l;
}

size_t kprintf(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);

    size_t v = vcprintf(fmt, kprintf_callback, args);
    va_end(args);
    return v;
}
