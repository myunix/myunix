#include <vga_console.h>

#include <assert.h>
#include <stdint.h>

enum vga_color {
    VGA_COLOR_BLACK        = 0x0,
    VGA_COLOR_BLUE         = 0x1,
    VGA_COLOR_GREEN        = 0x2,
    VGA_COLOR_CYAN         = 0x3,
    VGA_COLOR_RED          = 0x4,
    VGA_COLOR_PURPLE       = 0x5,
    VGA_COLOR_BROWN        = 0x6,
    VGA_COLOR_GRAY         = 0x7,
    VGA_COLOR_DARK_GRAY    = 0x8,
    VGA_COLOR_LIGHT_BLUE   = 0x9,
    VGA_COLOR_LIGHT_GREEN  = 0xA,
    VGA_COLOR_LIGHT_CYAN   = 0xB,
    VGA_COLOR_LIGHT_RED    = 0xC,
    VGA_COLOR_LIGHT_PURPLE = 0xD,
    VGA_COLOR_YELLOW       = 0xE,
    VGA_COLOR_WHITE        = 0xF,
};

#define ANSI_ARGS_MAX 8

enum ansi_mode {
    ANSI_MODE_NONE = 0,
    ANSI_MODE_CSI = 1,
    ANSI_MODE_COMMAND = 2,
};

typedef struct {
    size_t x, y;
    size_t w, h;
    size_t bpp; /* bytes per pixel */
    size_t pitch; /* bytes per row */
    uintptr_t vmem;
    uint8_t attribute;

    /* ansi state machine */
    enum ansi_mode ansi_mode;
    unsigned int ansi_args[ANSI_ARGS_MAX];
    unsigned int ansi_argc;
} vga_display_t;

static vga_display_t main_vga_display;

static inline void vga_console_set(vga_display_t *display, size_t x, size_t y, size_t w, size_t h, uint16_t v) {
    assert(x + w <= display->w);
    assert(y + h <= display->h);
    for (size_t ny = 0; ny < h; ny++) {
        for (size_t nx = 0; nx < w; nx++) {
            volatile uint16_t *dst = (void *)(display->vmem + (y + ny) * display->pitch + (nx + x) * display->bpp);
            *dst = v;
        }
    }
}

static inline void vga_copy_line(vga_display_t *display, size_t s, size_t d) {
    for (size_t x = 0; x < display->w; x++) {
        volatile uint16_t *src = (void *)(display->vmem + s * display->pitch + x * display->bpp);
        volatile uint16_t *dst = (void *)(display->vmem + d * display->pitch + x * display->bpp);
        *dst = *src;
    }
}

static void vga_scroll(vga_display_t *display) {
    for (size_t y = 0; y < (display->h - 1); y++) {
        vga_copy_line(display, y + 1, y);
    }

    vga_console_set(display, 0, display->h - 1, display->w, 1, (display->attribute << 8) | ' ');
}

void vga_console_init(uintptr_t vmem, size_t w, size_t h, size_t bpp, size_t pitch) {
    assert(bpp == 2);
    main_vga_display.x = 0;
    main_vga_display.y = 0;
    main_vga_display.w = w;
    main_vga_display.h = h;
    main_vga_display.bpp = bpp;
    main_vga_display.pitch = pitch;
    main_vga_display.vmem = vmem;
    main_vga_display.attribute = VGA_COLOR_BLUE << 4 | VGA_COLOR_WHITE;
    main_vga_display.ansi_mode = ANSI_MODE_NONE;
    main_vga_display.ansi_args[0] = 0;
    main_vga_display.ansi_argc = 0;
    vga_console_set(&main_vga_display, 0, 0, w, h, main_vga_display.attribute << 8 | ' ');
}

static enum ansi_mode ansi_runcmd(vga_display_t *display, char c);
static enum ansi_mode ansi_putc(vga_display_t *display, char c) {
    if (display->ansi_mode == ANSI_MODE_CSI) {
        if (c == '[') {
            return ANSI_MODE_COMMAND;
        } else {
            /* TODO: implement */
            /* XXX: fall through */
        }
    } else if (display->ansi_mode == ANSI_MODE_COMMAND) {
        if (c >= '0' && c <= '9') {
            /* parameter */
            display->ansi_args[display->ansi_argc] *= 10;
            display->ansi_args[display->ansi_argc] += (c - '0');
            return display->ansi_mode;
        } else if (c == ';') {
            /* new parameter */
            if (display->ansi_argc >= ANSI_ARGS_MAX) {
                return display->ansi_mode;
            } else {
                display->ansi_argc += 1;
                display->ansi_args[display->ansi_argc] = 0;
                return display->ansi_mode;
            }
        } else if (64 <= c && c <= 126) {
            return ansi_runcmd(display, c);
        }
    }

    /* invalid sequence */
    return ANSI_MODE_NONE;
}

/* convert ansi color index to vga attribute */
static const unsigned int color_ansi_to_vga[8] = {
    VGA_COLOR_BLACK,
    VGA_COLOR_RED,
    VGA_COLOR_GREEN,
    VGA_COLOR_YELLOW,
    VGA_COLOR_BLUE,
    VGA_COLOR_PURPLE,
    VGA_COLOR_CYAN,
    VGA_COLOR_WHITE,
};

static enum ansi_mode ansi_runcmd(vga_display_t *display, char c) {
    /* #lizard forgives the complexity */
    switch(c) {
        case 'A': { /* cursor up */
            unsigned int dist = display->ansi_argc > 0 ? display->ansi_args[0] : 1;

            if (display->y >= dist) {
                display->y -= dist;
            } else {
                display->y = 0;
            }

            } break;
        case 'B': { /* cursor down */
            unsigned int dist = display->ansi_argc > 0 ? display->ansi_args[0] : 1;

            if (display->y + dist > display->h) {
                display->y = display->h - 1;
            } else {
                display->y -= dist;
            }

            } break;
        case 'C': { /* cursor forward */
            unsigned int dist = display->ansi_argc > 0 ? display->ansi_args[0] : 1;

            if (display->x + dist > display->w) {
                display->x = display->w - 1;
            } else {
                display->x += dist;
            }

            } break;
        case 'D': { /* cursor backward */
            unsigned int dist = display->ansi_argc > 0 ? display->ansi_args[0] : 1;

            if (display->x < dist) {
                display->x = 0;
            } else {
                display->x -= dist;
            }

            } break;
        case 'H':
        case 'f': { /* move cursor to line Y, char X */
            unsigned int y = display->ansi_argc > 0 ? display->ansi_args[0] - 1 : 0;
            unsigned int x = display->ansi_argc > 1 ? display->ansi_args[1] - 1 : 0;
            if (x > display->w) {
                x = display->w - 1;
            }

            if (y > display->h) {
                y = display->h - 1;
            }

            display->x = x;
            display->y = y;
            } break;
        case 'J': /* erase parts of the screen */
            /* TODO: implement */
            break;
        case 'K': /* erase parts of the line */
            /* TODO: implement */
            break;
        case 'm': { /* color change */
            if (display->ansi_argc == 0) {
                display->ansi_args[0] = 0;
                display->ansi_argc += 1;
            }

            for (size_t i = 0; i <= display->ansi_argc; i++) {
                unsigned int color = display->ansi_args[i];

                if (color == 0) {
                    display->attribute = 0x12;
                } else if (30 <= color && color <= 37) {
                    /* foreground */
                    display->attribute = (display->attribute & 0xF0) | (color_ansi_to_vga[color - 30]);
                } else if (40 <= color && color <= 47) {
                    display->attribute = (display->attribute & 0x0F) | (color_ansi_to_vga[color - 30]) << 8;
                }
            }
            } break;
        case 's': /* save attributes */
        case 'u': /* restore attributes */
            break;
        default:
            /* unhandled */
            break;
    }

    return ANSI_MODE_NONE;
}

static void vga_putc(vga_display_t *display, char c) {
    if (display->ansi_mode != ANSI_MODE_NONE) {
        display->ansi_mode = ansi_putc(display, c);
        return;
    }

    if (c == '\b') {
        /* TODO: implement */
    } else if (c == '\n') {
        display->x = 0;
        display->y++;
    } else if (c == '\r') {
        display->x = 0;
    } else if (c == '\t') {
        /* TODO: implement */
    } else if (c == '\e') {
        /* reset the state machine */
        display->ansi_argc = 0;
        display->ansi_args[0] = 0;
        display->ansi_mode = ANSI_MODE_CSI;
    } else {
        volatile uint16_t *dst = (void *)(display->vmem + display->y * display->pitch + display->x * display->bpp);
        *dst = (display->attribute << 8) | c;
        display->x++;
    }

    if (display->x >= display->w) {
        display->x = 0;
        display->y += 1;
    }

    if (display->y >= display->h) {
        vga_scroll(display);
        display->x = 0;
        display->y -= 1;
    }
}

void putc(char c) {
    if (main_vga_display.vmem == 0) {
        return;
    }

    return vga_putc(&main_vga_display, c);
}
