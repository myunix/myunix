#include <kheap.h>
#include <string.h>

#include <assert.h>

size_t __attribute__((nonnull)) __attribute__((pure)) strnlen(const char * restrict s, size_t max) {
    assert(s != NULL);

    size_t l;
    for (l = 0; (l < max) && (s[l] != 0); l++);
    return l;
}

void * __attribute__((nonnull)) memset(void * restrict dest, int c, size_t len) {
    assert(dest != NULL);
    assert(len != 0);
    char *d = dest;
    while (len--) {
        *d++ = (char)c;
    }
    return dest;
}

void * __attribute__((nonnull)) memcpy(void *dest, const void *src, size_t len) {
    assert(dest != NULL);
    assert(src != NULL);

    const char *s = src;
    char *d = dest;

    for (size_t i = 0; i < len; i++) {
        d[i] = s[i];
    }

    return dest;
}

void * __attribute__((nonnull)) memmove(void *dest, const void *src, size_t len) {
    if (src != dest) {
        if (dest < src) {
            return memcpy(dest, src, len);
        } else {
            const char *s = src;
            char *d = dest;
            for (size_t i = len; i > 0; i--) {
                d[i - 1] = s[i - 1];
            }
        }
    }

    return dest;
}

__attribute__((pure)) int memcmp(const void *a, const void *b, size_t len) {
    assert(a != NULL);
    assert(b != NULL);

    const char *s1 = a;
    const char *s2 = b;

    for (size_t i = 0; i < len; i++) {
        if (s1[i] != s2[i]) {
            return s1[i] - s2[i];
        }
    }

    return 0;
}

__attribute__((pure)) int strncmp(const char *s1, const char *s2, size_t len) {
    assert(s1 != NULL);
    assert(s2 != NULL);

    for (size_t i = 0; i < len; i++) {
        if (s1[i] != s2[i]) {
            return s1[i] - s2[i];
        }

        if (s1[i] == 0) {
            return 0;
        }
    }

    return 0;
}

char *strndup(const char *src, size_t len) {
    const size_t dest_len = strnlen(src, len);

    char *dest = kmalloc(sizeof(char) * (dest_len + 1));
    if (dest == NULL) {
        return NULL;
    }

    memcpy(dest, src, dest_len + 1);
    dest[dest_len] = 0;

    return dest;
}

char *strncpy(char *dest, const char *src, size_t len) {
    for (size_t i = 0; (i < len) && (src[i] != 0); i++) {
        dest[i] = src[i];
    }

    return dest;
}
