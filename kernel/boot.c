#include <boot.h>
#include <kprintf.h>

#include <stdnoreturn.h>

noreturn void cpu_halt(const char *msg) {
    kprintf("halting the cpu: %s\n", msg);
    _halt();
}
