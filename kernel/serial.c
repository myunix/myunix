#include <serial.h>
#include <string.h>

#include "arch/x86-family/cpu.h"

#include <stdbool.h>

static void serial_ctrl(struct serial_dev *obj, uint16_t reg, uint8_t value) {
    return cpu_outb(obj->base_port + reg, value);
}

static uint8_t serial_read_ctrl(struct serial_dev *obj, uint16_t reg) {
    return cpu_inb(obj->base_port + reg);
}

static bool serial_is_transmit_ready(struct serial_dev *obj) {
    return serial_read_ctrl(obj, 5) & 0x20;
}

void serial_putc(struct serial_dev *obj, uint8_t c) {
    if (c == '\n') {
        serial_putc(obj, '\r');
    }
    while (!serial_is_transmit_ready(obj)) {
        __asm__ __volatile__("");
    }
    serial_ctrl(obj, 0, c);
}

void serial_init(struct serial_dev *obj, uint16_t base_port) {
    memset(obj, 0, sizeof(*obj));
    obj->base_port = base_port;

    serial_ctrl(obj, 1, 0x00); // disable all interrupts
    serial_ctrl(obj, 3, 0x80); // enable DLAB
    serial_ctrl(obj, 0, 0x03); // 38400 baud => divisor low
    serial_ctrl(obj, 1, 0x00); // divisor high
    serial_ctrl(obj, 3, 0x03); // disable DLAB, 8 bits, no parity, one stop
    serial_ctrl(obj, 2, 0xC7); // enable FIFO, clear FIFO, 14 bytes threshold
    serial_ctrl(obj, 4, 0x0B); // enable IRQs
    serial_ctrl(obj, 1, 0x01); // enable data-received IRQ
    // serial_ctrl(obj, 1, 0x0F); // enable all IRQs
}
