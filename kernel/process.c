#include <kernel.h>
#include <boot.h>
#include <kheap.h>
#include <process.h>

#include <assert.h>

static void process_map_shared(process_t *proc);

/* ========= process_t helpers ========== */
process_t *process_init(void) {
    process_t * const proc = kcalloc(1, sizeof(process_t));
    if (proc == NULL) {
        return NULL;
    }

    proc->thread = thread_init();
    if (proc->thread == NULL) {
        kfree(proc);
        return NULL;
    }

    proc->pdir = page_directory_init();
    if (proc->pdir == NULL) {
        page_directory_down(proc->pdir);
        thread_destroy(proc->thread);
        kfree(proc);
        return NULL;
    }

    kref_init(&proc->__ref);

    process_map_shared(proc);

    return proc;
}

static void process_drestroy(void *_proc) {
    process_t * const proc = _proc;

    thread_destroy(proc->thread);

    kfree(proc);
}

void process_up(process_t *proc) {
    assert(proc != NULL);
    kref_up(&proc->__ref);
}

void process_down(process_t *proc) {
    assert(proc != NULL);
    kref_down(&proc->__ref, process_drestroy, proc);
}

/* ========== helpers ========== */

/* map the shared region (gdt, idt, etc...) and the first page of the
 * kernel stack into the same address as in the kernel into user address space */
static void process_map_shared(process_t *proc) {
    assert(IS_ALIGNED(__kernel_region_shared_text_start, PAGE_SIZE));
    assert(IS_ALIGNED(__kernel_region_shared_text_end, PAGE_SIZE));

    /* gdt, idt, etc... */
    vmm_map_region(
            proc->pdir,
            ALIGN_DOWN(__kernel_region_shared_text_start, PAGE_SIZE),
            ALIGN_DOWN(__kernel_region_shared_text_start, PAGE_SIZE),
            ALIGN(__kernel_region_shared_text_end, PAGE_SIZE) -
            ALIGN_DOWN(__kernel_region_shared_text_start, PAGE_SIZE),
            true, false, false, 0);

    /* we only map the first 4kb of the kernel stack, since the switch to the
     * kernel directory happens befor using up the stack */
    const uintptr_t kernel_stack_top_page = proc->thread->kernel_stack - PAGE_SIZE;
    const uintptr_t kernel_stack_top_phys = vmmk_get_phys(kernel_stack_top_page);
    assert(kernel_stack_top_phys != 0);
    vmm_map_region(
            proc->pdir,
            kernel_stack_top_page,
            kernel_stack_top_phys,
            PAGE_SIZE,
            true, true, false, 0);
}
