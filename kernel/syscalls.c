#include <kernel.h>
#include <kprintf.h>
#include <scheduler.h>
#include <syscalls.h>

#include <assert.h>

typedef uint32_t (*syscall_f)(uint32_t arg0, uint32_t arg1, uint32_t arg2);

__attribute__((const))
uint32_t sys_exit(uint32_t arg0, uint32_t arg1, uint32_t arg2) {
    (void)arg0;
    (void)arg1;
    (void)arg2;
    return 0;
}

uint32_t sys_test(uint32_t arg0, uint32_t arg1, uint32_t arg2) {
    (void)arg1;
    (void)arg2;
    scheduler_lock();
    kprintf("%c", (char)arg0);
    scheduler_unlock();
    return 0;
}

static syscall_f syscalls[] = {
    [0x0000] = NULL,     /**/
    [0x0001] = sys_exit, /**/
    [0x0002] = sys_test, /**/
};

void syscall_isr(registers_t *regs) {
#if defined(__i386__)
    const size_t syscall_number = regs->eax;
    if (syscall_number < array_size(syscalls)) {
        syscall_f syscall = syscalls[syscall_number];
        if (syscall != NULL) {
            regs->eax = syscall(regs->ebx, regs->ecx, regs->edx);
        } else {
            regs->eax = -1;
        }
    }
#elif defined(__x86_64__)
    const size_t syscall_number = regs->rax;
    if (syscall_number < array_size(syscalls)) {
        syscall_f syscall = syscalls[syscall_number];
        if (syscall != NULL) {
            regs->rax = syscall(regs->rbx, regs->rcx, regs->rdi);
        } else {
            regs->rax = -1;
        }
    }
#else
#error "not implemented"
#endif
}
