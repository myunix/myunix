#!/usr/bin/env make
SRCS_C += $(LOCAL)ubsan.c

# XXX: technically we're not allowed to modify CFLAGS in here, but it's the only sensible way
CFLAGS += -fsanitize=undefined
