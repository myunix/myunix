#include "ubsan.h"

#include <kernel.h>
#include <stacktrace.h>
#include <kprintf.h>
#include <boot.h>

#include <stdarg.h>
#include <stdnoreturn.h>
#include <stddef.h>

static const char *type_check_kind_to_str(unsigned char type_check_kind) {
    static const char *type_check_kinds[] = {
        "load of", "store to", "reference binding to", "member access within",
        "member call on", "constructor call on", "downcast of", "downcast of",
        "upcast of", "cast to virtual base of", "_Nonnull binding to",
        "dynamic operation on"
    };

    if (type_check_kind < array_size(type_check_kinds)) {
        return type_check_kinds[type_check_kind];
    } else {
        return "unknown";
    }
}

static noreturn void ubsan_panic(const struct ubsan_source_location *loc, const char *violation_format, ...) {
    kprintf("[ubsan] %s:%u:%uc violation : ", loc->filename == NULL ? "(unknown)" : loc->filename, loc->line, loc->column);
    va_list args;
    va_start(args, violation_format);
    vcprintf(violation_format, kprintf_callback, args);
    va_end(args);
    kprintf("\n");
    stacktrace_print();
    cpu_halt("ubsan violation");
}

static const char *type_name(const struct ubsan_type_mismatch_data *data) {
    return data == NULL
        ? "(data == NULL)"
        : (data->type == NULL ? "(type == NULL)" : data->type->type_name);
}

/* most of the following code can be found (in C++) in gcc/libsanitizer/ubsan/ubsan_handlers.cc */

noreturn void __ubsan_handle_type_mismatch_v1(const struct ubsan_type_mismatch_data *data, ubsan_value_handle ptr) {
    const uintptr_t alignment = (uintptr_t)1 << data->alignment;

    if (ptr == 0) { /* NULL derference */
        ubsan_panic(&data->loc, "%s null pointer of type %s", type_check_kind_to_str(data->type_check_kind), type_name(data));
    } else if (ptr & (alignment - 1)) { /* misaligned pointer use */
        ubsan_panic(&data->loc,
                "%s misaligned address %p for type %s which requires %u byte alignment",
                type_check_kind_to_str(data->type_check_kind),
                (void *)ptr,
                type_name(data),
                alignment);
    } else {
        ubsan_panic(&data->loc,
                "%s address %p with insufficient space for an object of type %s",
                type_check_kind_to_str(data->type_check_kind),
                (void *)ptr,
                type_name(data));
    }
}

noreturn void __ubsan_handle_add_overflow(const struct ubsan_overflow_data *data, ubsan_value_handle lhs, ubsan_value_handle rhs) {
    (void)lhs;
    (void)rhs;
    ubsan_panic(&data->loc, "%s", __func__);
}

noreturn void __ubsan_handle_sub_overflow(const struct ubsan_overflow_data *data, ubsan_value_handle lhs, ubsan_value_handle rhs) {
    (void)lhs;
    (void)rhs;
    ubsan_panic(&data->loc, "%s", __func__);
}

noreturn void __ubsan_handle_mul_overflow(const struct ubsan_overflow_data *data, ubsan_value_handle lhs, ubsan_value_handle rhs) {
    (void)lhs;
    (void)rhs;
    ubsan_panic(&data->loc, "%s", __func__);
}

noreturn void __ubsan_handle_negate_overflow(const struct ubsan_overflow_data *data, ubsan_value_handle oldval) {
    (void)oldval;
    ubsan_panic(&data->loc, "%s", __func__);
}

noreturn void __ubsan_handle_divrem_overflow(const struct ubsan_overflow_data *data, ubsan_value_handle lhs, ubsan_value_handle rhs) {
    (void)lhs;
    (void)rhs;
    ubsan_panic(&data->loc, "%s", __func__);
}

noreturn void __ubsan_handle_shift_out_of_bounds(const struct ubsan_shift_out_of_bounds_data *data, ubsan_value_handle lhs, ubsan_value_handle rhs) {
    (void)lhs;
    (void)rhs;
    ubsan_panic(&data->loc, "%s", __func__);
}

noreturn void __ubsan_handle_out_of_bounds(const struct ubsan_out_of_bounds_data *data, ubsan_value_handle index) {
    ubsan_panic(&data->loc, "index %s %u out of bounds for type %s",
            data->index_type->type_name,
            index, data->array_type->type_name);
}

noreturn void __ubsan_handle_builtin_unreachable(const struct ubsan_unreachable_data *data) {
    ubsan_panic(&data->loc, "reached an unreachable point");
}

noreturn void __ubsan_handle_missing_return(const struct ubsan_unreachable_data *data) {
    ubsan_panic(&data->loc, "execution reached the end of a non-void function without returning a value");
}

noreturn void __ubsan_handle_vla_bound_not_positive(const struct ubsan_vla_bound_data *data, ubsan_value_handle bound) {
    /* there's probably already something wrong if you're using VLA */
    ubsan_panic(&data->loc, "%s bound = %u", __func__, bound);
}

noreturn void __ubsan_handle_float_cast_overflow(const struct ubsan_float_cast_overflow_data *data, ubsan_value_handle from) {
    (void)from;
    ubsan_panic(&data->loc, "%s", __func__);
}

noreturn void __ubsan_handle_load_invalid_value(const struct ubsan_invalid_value_data *data, ubsan_value_handle value) {
    ubsan_panic(&data->loc, "load of value %u which is not valid for type %s", value, data->type->type_name);
}

noreturn void __ubsan_handle_implicit_conversion(const struct ubsan_implicit_conversion_data *data, ubsan_value_handle src, ubsan_value_handle dst) {
    (void)src;
    (void)dst;
    ubsan_panic(&data->loc, "%s", __func__);
}

noreturn void __ubsan_handle_invalid_builtin(const struct ubsan_invalid_builtin_data *data) {
    ubsan_panic(&data->loc, "passing zero to builtin function, which is not a valid argument");
}

noreturn void __ubsan_handle_function_type_mismatch(const struct ubsan_function_type_mismatch_data *data, ubsan_value_handle func) {
    (void)func;
    ubsan_panic(&data->loc, "%s", __func__);
}


noreturn void __ubsan_handle_nonnull_return_v1(const struct ubsan_nonnull_arg_data *data, const struct ubsan_source_location *loc_ptr) {
    (void)loc_ptr;
    ubsan_panic(&data->loc, "%s", __func__);
}

noreturn void __ubsan_handle_nullability_return_v1(const struct ubsan_nonnull_arg_data *data, const struct ubsan_source_location *loc_ptr) {
    (void)loc_ptr;
    ubsan_panic(&data->loc, "%s", __func__);
}

noreturn void __ubsan_handle_nonnull_arg(const struct ubsan_nonnull_arg_data *data) {
    ubsan_panic(&data->loc, "%s", __func__);
}

noreturn void __ubsan_handle_pointer_overflow(const struct ubsan_pointer_overflow_data *data, ubsan_value_handle base, ubsan_value_handle result) {
    (void)base;
    (void)result;
    ubsan_panic(&data->loc, "%s", __func__);
}

/* TODO: we might need CFI handlers */
