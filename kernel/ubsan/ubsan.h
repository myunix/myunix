#ifndef __UBSAN_H
#define __UBSAN_H 1

#include <stdint.h>
#include <stdbool.h>
#include <stdnoreturn.h>

/* this header is mostly useless, but it fixes sparse emitting warnings about
 * unused functions and gcc link time optimisation
 * */

/* gcc/libsanitizer/ubsan/ubsan_value.h: class SourceLocation */
struct ubsan_source_location {
    const char *filename;
    uint32_t line;
    uint32_t column;
};

/* gcc/libsanitizer/ubsan/ubsan_value.h: class TypeDescriptor */
struct ubsan_type_descriptor {
    uint16_t type_kind;
    uint16_t type_info;
    const char type_name[1];
};

typedef uintptr_t ubsan_value_handle;

/* handlers */

struct ubsan_type_mismatch_data {
    const struct ubsan_source_location loc;
    const struct ubsan_type_descriptor *type;
    unsigned char alignment;
    unsigned char type_check_kind;
};

noreturn void __ubsan_handle_type_mismatch_v1(const struct ubsan_type_mismatch_data *data, ubsan_value_handle ptr);

struct ubsan_overflow_data {
    const struct ubsan_source_location loc;
    const struct ubsan_type_descriptor *type;
};

noreturn void __ubsan_handle_add_overflow(const struct ubsan_overflow_data *data, ubsan_value_handle lhs, ubsan_value_handle rhs);

noreturn void __ubsan_handle_sub_overflow(const struct ubsan_overflow_data *data, ubsan_value_handle lhs, ubsan_value_handle rhs);

noreturn void __ubsan_handle_mul_overflow(const struct ubsan_overflow_data *data, ubsan_value_handle lhs, ubsan_value_handle rhs);

noreturn void __ubsan_handle_negate_overflow(const struct ubsan_overflow_data *data, ubsan_value_handle oldval);

noreturn void __ubsan_handle_divrem_overflow(const struct ubsan_overflow_data *data, ubsan_value_handle lhs, ubsan_value_handle rhs);

struct ubsan_shift_out_of_bounds_data {
    const struct ubsan_source_location loc;
    const struct ubsan_type_descriptor *lhs;
    const struct ubsan_type_descriptor *rhs;
};

noreturn void __ubsan_handle_shift_out_of_bounds(const struct ubsan_shift_out_of_bounds_data *data, ubsan_value_handle lhs, ubsan_value_handle rhs);

struct ubsan_out_of_bounds_data {
    const struct ubsan_source_location loc;
    const struct ubsan_type_descriptor *array_type;
    const struct ubsan_type_descriptor *index_type;
};

noreturn void __ubsan_handle_out_of_bounds(const struct ubsan_out_of_bounds_data *data, ubsan_value_handle index);

struct ubsan_unreachable_data {
    const struct ubsan_source_location loc;
};

noreturn void __ubsan_handle_builtin_unreachable(const struct ubsan_unreachable_data *data);
noreturn void __ubsan_handle_missing_return(const struct ubsan_unreachable_data *data);

struct ubsan_vla_bound_data {
    const struct ubsan_source_location loc;
    const struct ubsan_type_descriptor *type;
};

noreturn void __ubsan_handle_vla_bound_not_positive(const struct ubsan_vla_bound_data *data, ubsan_value_handle bound);

/* XXX: we only implement version 2 */
struct ubsan_float_cast_overflow_data {
    const struct ubsan_source_location loc;
    const struct ubsan_type_descriptor *from_type;
    const struct ubsan_type_descriptor *to_type;
};

noreturn void __ubsan_handle_float_cast_overflow(const struct ubsan_float_cast_overflow_data *data, ubsan_value_handle from);

struct ubsan_invalid_value_data {
    const struct ubsan_source_location loc;
    const struct ubsan_type_descriptor *type;
};

noreturn void __ubsan_handle_load_invalid_value(const struct ubsan_invalid_value_data *data, ubsan_value_handle value);

struct ubsan_implicit_conversion_data {
    const struct ubsan_source_location loc;
    const struct ubsan_type_descriptor *from_type;
    const struct ubsan_type_descriptor *to_type;
};

noreturn void __ubsan_handle_implicit_conversion(const struct ubsan_implicit_conversion_data *data, ubsan_value_handle src, ubsan_value_handle dst);

struct ubsan_invalid_builtin_data {
    const struct ubsan_source_location loc;
    const unsigned char kind;
};

noreturn void __ubsan_handle_invalid_builtin(const struct ubsan_invalid_builtin_data *data);

struct ubsan_function_type_mismatch_data {
    const struct ubsan_source_location loc;
    const struct ubsan_type_decriptor *type;
};

noreturn void __ubsan_handle_function_type_mismatch(const struct ubsan_function_type_mismatch_data *data, ubsan_value_handle func);

struct ubsan_nonnull_arg_data {
    const struct ubsan_source_location loc;
    const struct ubsan_source_location attr_loc;
    const int arg_index;
};

noreturn void __ubsan_handle_nonnull_return_v1(const struct ubsan_nonnull_arg_data *data, const struct ubsan_source_location *loc_ptr);
noreturn void __ubsan_handle_nullability_return_v1(const struct ubsan_nonnull_arg_data *data, const struct ubsan_source_location *loc_ptr);
noreturn void __ubsan_handle_nonnull_arg(const struct ubsan_nonnull_arg_data *data);

struct ubsan_pointer_overflow_data {
    const struct ubsan_source_location loc;
};

noreturn void __ubsan_handle_pointer_overflow(const struct ubsan_pointer_overflow_data *data, ubsan_value_handle base, ubsan_value_handle result);

/* TODO: we might need CFI handlers */


#endif
