#include <thread.h>
#include <kprintf.h>
#include <list.h>

#include <cpu.h>
#include <boot.h>

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>

#define assert_interrupts_disabled() (assert(cpu_disable_interrupts() == false))

thread_t *current_thread = NULL;
list_t *ready_queue = NULL;

bool scheduler_ready = false;

bool postponed_schedule = false;
bool scheduler_enable_interrupts_on_unlock = false;
volatile unsigned int scheduler_lock_count;

/* provided by arch */
// XXX: we need to use pointers, so if we resume the same thread as ourselfs we restore something sensible
extern void __switch_thread(uintptr_t *save_stack_pointer, uintptr_t *next_stack_pointer);

#if 0
void switch_thread(thread_t *next) {
    thread_t *self = current_thread;
    self->state = THREAD_STATE_READY;

    /* set the current thread before switching, so we don't need special init code for
     * new threads */
    current_thread = next;
    current_thread->state = THREAD_STATE_RUNNING;

    tss_set_kstack(current_thread->kernel_stack);

    __switch_thread(&self->cpu_state.stack_pointer, &next->cpu_state.stack_pointer);

    assert(current_thread == self);
}
#endif

extern noreturn void __restore_thread_frame(uintptr_t stack_pointer);

void scheduler_init(void) {
    ready_queue = list_init();
    assert(ready_queue != NULL);
}

void __schedule(void) {
    assert_interrupts_disabled();
    assert(scheduler_lock_count == 0);

    /* we shouldn't be called in this case */
    assert(current_thread != NULL);

    thread_t *self = current_thread;

    thread_t *next_thread = list_dequeue(ready_queue);
    if (next_thread == NULL) {
        /* no other task is waiting for cpu time */

        if (current_thread->state == THREAD_STATE_RUNNING) {
            /* case 1: we're the only task running, continue */
            next_thread = current_thread;
        } else {
            assert(0);
        }
    }

    if (current_thread != next_thread) {
        /* we need to switch context */

        /* sanity checks */
        assert(next_thread->state == THREAD_STATE_READY);

        if (current_thread->state == THREAD_STATE_RUNNING) {
            /* case 1: current task got preempted */
            current_thread->state = THREAD_STATE_READY;
            list_queue(ready_queue, current_thread);
        } else if (current_thread->state == THREAD_STATE_TERMINATED) {
            /* case 2: current task is terminating, perform destructive context switch */
            current_thread = next_thread;
            current_thread->state = THREAD_STATE_RUNNING;

            tss_set_kstack(next_thread->kernel_stack);

            // kprintf("before __restore_thread_frame due to terminating !\n");

            __restore_thread_frame(next_thread->cpu_state.stack_pointer);
        } else {
            /* case 3: current task blocked voluntarely and is in some list already */
            assert(0);
        }

        /* there can only be one of us .... */
        assert(self->state != THREAD_STATE_RUNNING);

        /* perform a normal context switch */

        /* set the current thread before switching, so we don't need special init code for
         * new threads */
        current_thread = next_thread;
        next_thread->state = THREAD_STATE_RUNNING;

        tss_set_kstack(next_thread->kernel_stack);

        __switch_thread(&self->cpu_state.stack_pointer, &next_thread->cpu_state.stack_pointer);
    }

    /* sanity checks */
    assert(current_thread == self);
    assert(current_thread->state == THREAD_STATE_RUNNING);
}

void scheduler_lock(void) {
    bool interrupts_enabled = cpu_disable_interrupts();

    if (scheduler_lock_count == 0) {
        assert(postponed_schedule == false);
        // assert(enable_interrupts == false);
        scheduler_enable_interrupts_on_unlock = interrupts_enabled;
    }

    scheduler_lock_count++;
}

void scheduler_unlock(void) {
    assert(scheduler_lock_count > 0);
    scheduler_lock_count--;
    if (scheduler_lock_count == 0) {
        /* XXX: this might be changed once we call __schedule */
        bool enable_interrupts = scheduler_enable_interrupts_on_unlock;
        scheduler_enable_interrupts_on_unlock = false;

        if (postponed_schedule) {
            postponed_schedule = false;
            __schedule();
        }

        if (enable_interrupts) {
            cpu_enable_interrupts();
        }
    }
}

void yield(void) {
    scheduler_lock();
    assert_interrupts_disabled();
    assert(scheduler_ready);
    postponed_schedule = true;
    scheduler_unlock();
}

void scheduler_timer_wakeup(void) {
    scheduler_lock();
    assert_interrupts_disabled();
    if (scheduler_ready) {
        postponed_schedule = true;
    }
    scheduler_unlock();
}

void scheduler_add_thread(thread_t *thread) {
    scheduler_lock();
    assert(thread != NULL);
    thread->state = THREAD_STATE_READY;
    list_queue(ready_queue, thread);
    scheduler_unlock();
}

noreturn void scheduler_exit_thread(void) {
    scheduler_lock();
    assert_interrupts_disabled();
    assert(scheduler_lock_count == 1);
    assert(scheduler_ready == true);
    postponed_schedule = true;
    scheduler_unlock();
    while (1) { assert(0); };
}

noreturn void scheduler_enable(void) {
    cpu_disable_interrupts();
    assert(scheduler_lock_count == 0);

    assert(scheduler_ready == false);
    scheduler_ready = true;

    current_thread = list_dequeue(ready_queue);
    assert(current_thread != NULL);

    scheduler_enable_interrupts_on_unlock = false;
    postponed_schedule = false;

    tss_set_kstack(current_thread->kernel_stack);

    kprintf("first thread stack_pointer = %p\n", (void *)current_thread->cpu_state.stack_pointer);
    assert(current_thread->cpu_state.stack_pointer != 0);

    current_thread->state = THREAD_STATE_RUNNING;

    __restore_thread_frame(current_thread->cpu_state.stack_pointer);

    assert(0);
}
