#!/usr/bin/env -S make -f
.SUFFIXES:

# TODO: build with TARGET_ARCH in file names

PART := $(LOCAL)

# default values
CFLAGS ?= -O3 -g -Werror
CPPFLAGS ?=
LDFLAGS ?= -g
KERNEL_MODULES ?= ubsan
# LIBS: libraries linked to the kernel
LIBS ?=
# SRCS_C: c source files, will be automatically added to objects
SRCS_C ?=

# required values
CFLAGS := $(CFLAGS) -std=gnu11
# -Wno-nonnull-compare: don't complain if we assert a value that has the attribute nonnull
CFLAGS += -Wall \
		  -Wextra \
		  -Wcast-align=strict -Wcast-qual \
		  -Wvla \
		  -Wnonnull -Wno-nonnull-compare \
		  -Wformat=2 -Wformat-signedness \
		  -Wduplicated-cond -Wduplicated-branches \
		  -Wlogical-op \
		  -Wnull-dereference \
		  -Wjump-misses-init \
		  -Wdouble-promotion \
		  -Wwrite-strings
# let gcc suggest attributes (if gcc suggests noreturn for a function that should return, there is most likely an assert that will fail)
CFLAGS += -Wsuggest-attribute=pure -Wsuggest-attribute=const -Wsuggest-attribute=noreturn -Wsuggest-attribute=format
# __DATE__, __TIME__ and __TIMESTAMP__ hurt reproducibility, warn if we use them
CFLAGS += -Wdate-time
# -ffreestanding: important: disables all stdlibc and what not of gcc
CFLAGS += -ffreestanding
# enable stack smashing protection
CFLAGS += -fstack-protector-all
CLFAGS += -mno-red-zone
# link time optimisation
#CFLAGS += -flto

CPPFLAGS := $(CPPFLAGS) -I $(PART)/include -D__myunix__kernel__
LDFLAGS := $(LDFLAGS) -nostdinc -static
KERNEL_MODULES := $(KERNEL_MODULES) .
SRCS_C := $(SRCS_C)

LIBS := $(LIBS) -nostdlib

# arch is a special "module", it **shouldn't** be enabled by adding it to MODULES
# arch/*/arch.mk is required to define the following variables:
# ARCH_OBJS: architecture specific objects that need to be build and linked before any other
# ARCH_SRCS_C: c source files that will be added to OBJS after ARCH_OBJS but before modules
# ARCH_LIBS: architecture specific libraries
# ARCH_CFLAGS: architecture specific CFLAGS
# ARCH_CPPFLAGS: architecture specific CPPFLAGS
# ARCH_LDFLAGS: architecture specific LDFLAGS
# It may also append to the following variables if needed:
# CFLAGS, CPPFLAGS, LDFLAGS, ASFLAGS
ARCH_DIR := $(PART)/arch/$(HOST_ARCH)
include $(ARCH_DIR)/arch.mk

# Kernel Modules
# module/module.mk can append to the following variables:
# SRCS_C: c files that should be compiled and linked into the kernel
include $(addprefix $(PART)/,$(addsuffix /module.mk,$(KERNEL_MODULES)))

# arch is special, it always comes first
CPPFLAGS := $(ARCH_CPPFLAGS) $(CPPFLAGS)
CFLAGS := $(ARCH_CFLAGS) $(CFLAGS)
LDFLAGS := $(ARCH_LDFLAGS) $(LDFLAGS)
SRCS_C := $(ARCH_SRCS_C) $(SRCS_C)
LIBS := $(ARCH_LIBS) $(LIBS)
OBJS := $(ARCH_OBJS) $(SRCS_C:.c=.o) $(OBJS)
DEPS := $(OBJS:.o=.d)

.PHONY: kernel-all
kernel-all: kernel-info $(PART)/kernel.elf

.PHONY: kernel-clean
kernel-clean:
	rm -f $(PART)/kernel.elf
	rm -f $(OBJS) $(DEPS)
	rm -f $(PART)/*._s $(PART)/*/*._s $(PART)/*/*/*._s
	rm -f $(PART)/*.o $(PART)/*/*.o $(PART)/*/*/*.o
	rm -f $(PART)/*.d $(PART)/*/*.d $(PART)/*/*/*.d

.PHONY: kernel-info
kernel-info:
	@echo "kernel: building the myunix kernel on $(BUILD) for $(HOST)"
	@echo "kernel: enabled modules: $(KERNEL_MODULES)"

$(PART)/%.o: $(PART)/%.c $(PART)/%.d | $(CC)
	@echo CC $@
	$(CC) $(CPPFLAGS) $(CFLAGS) -MD -MP -c $< -o $@

.PRECIOUS: $(PART)/%._s
$(PART)/%._s: $(PART)/%.S $(PART)/%.d | $(CC)
	@echo CC $@
	$(CC) $(CPPFLAGS) $(CFLAGS) -MD -MP -E $< -o $@

$(PART)/%.o: $(PART)/%._s | $(AS)
	@echo AS $@
	$(AS) $(ASFLAGS) $< -o $@

$(PART)/kernel.elf: $(ARCH_LINK_SCRIPT) $(OBJS) | $(LD)
	@echo LD $@
	$(LD) $(LDFLAGS) -T $(ARCH_LINK_SCRIPT) -o $@ $(OBJS) $(LIBS)

# code sanity
.PHONY: kernel-cppcheck
kernel-cppcheck: $(TOOLCHAIN_INSTALL)/lib/tcc/include
	cppcheck --quiet --enable=all -I $(TOOLCHAIN_INSTALL)/lib/tcc/include $(CPPFLAGS) $(CPPCHECKFLAGS) $(SRCS_C) --check-config

.PHONY: kernel-sparse
kernel-sparse:
	sparse -Wsparse-all $(CPPFLAGS) $(CFLAGS) $(SPARSEFLAGS) $(SRCS_C)

.PHONY: kernel-complexity
kernel-complexity:
	complexity -h -c $(SRCS_C)

.PHONY: kernel-lizard
kernel-lizard:
	lizard $(PART)

# automatic dependency handleing
.PRECIOUS: $(PART)/%.d

$(PART)/%.d: ;

ifneq ($(MAKECMDGOALS),clean)
-include $(DEPS)
endif
