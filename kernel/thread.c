/* common implementation of kernel and userspace tasks */

#include <cpu.h>
#include <kernel.h>
#include <kheap.h>
#include <kprintf.h>
#include <list.h>
#include <mem/pmm.h>
#include <mem/vmm.h>
#include <scheduler.h>
#include <thread.h>

#include <boot.h>

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>

#define KERNEL_STACK_SIZE ((size_t)(PAGE_SIZE * 16))
#define GUARD_PAGE_VALUE ((uintptr_t)0xFFFF0000)

uintptr_t thread_kernel_stack_alloc(size_t size) {
    assert(IS_ALIGNED(size, PAGE_SIZE));

    uintptr_t start = vmmk_find_free_region(size + 4 * PAGE_SIZE);

    if (start == 0) {
        return 0;
    }

    vmmk_map_page(start + PAGE_SIZE * 0, GUARD_PAGE_VALUE, false, false);
    vmmk_map_page(start + PAGE_SIZE * 1, GUARD_PAGE_VALUE, false, false);

    for (size_t i = 0; i < size; i += PAGE_SIZE) {
        uintptr_t vaddr = start + PAGE_SIZE * 2 + i;
        uintptr_t paddr = pmm_alloc(PAGE_SIZE);
        assert(paddr != 0); /* FIXME: handle gracefully */

        vmmk_map_page(vaddr, paddr, true, true);
    }

    vmmk_map_page(start + size + PAGE_SIZE * 2, GUARD_PAGE_VALUE, false, false);
    vmmk_map_page(start + size + PAGE_SIZE * 3, GUARD_PAGE_VALUE, false, false);

    return start + PAGE_SIZE * 2;
}

void thread_kernel_stack_free(uintptr_t stack, size_t size) {
    assert(IS_ALIGNED(size, PAGE_SIZE));

    const uintptr_t base = stack - PAGE_SIZE * 2;

    assert(vmmk_unmap_page(base + PAGE_SIZE * 0) == GUARD_PAGE_VALUE);
    assert(vmmk_unmap_page(base + PAGE_SIZE * 1) == GUARD_PAGE_VALUE);

    for (size_t i = 0; i < size; i += PAGE_SIZE) {
        uintptr_t vaddr = stack + i;
        uintptr_t paddr = vmmk_unmap_page(vaddr);

        pmm_free(paddr, PAGE_SIZE);
    }

    assert(vmmk_unmap_page(stack + size + PAGE_SIZE * 0) == GUARD_PAGE_VALUE);
    assert(vmmk_unmap_page(stack + size + PAGE_SIZE * 1) == GUARD_PAGE_VALUE);
}

thread_t *thread_init(void) {
    thread_t *thread = kcalloc(1, sizeof(thread_t));
    if (thread == NULL) {
        return NULL;
    }

    thread->state = THREAD_STATE_READY;
    thread->kernel_stack = thread_kernel_stack_alloc(KERNEL_STACK_SIZE);
    if (thread->kernel_stack == 0) {
        kfree(thread);
        return NULL;
    }

    thread->kernel_stack += KERNEL_STACK_SIZE;

    return thread;
}

void thread_destroy(thread_t *thread) {
    assert(thread->state != THREAD_STATE_RUNNING);
    thread_kernel_stack_free(thread->kernel_stack, KERNEL_STACK_SIZE);
    kfree(thread);
}

static noreturn void kernel_thread_exit(thread_t *thread) {
    kprintf("thread exited: %p\n", thread);
    thread->state = THREAD_STATE_TERMINATED;
    scheduler_exit_thread();
}

/* arch isr */
extern void __restore_registers(void);

static void setup_thread_frame(thread_frame_t * const thread_frame) {
#if defined(__i386__)
    thread_frame->edi = 0;
    thread_frame->esi = 0;
    thread_frame->ebx = 0;
    thread_frame->ebp = 0;
    thread_frame->eip = (uintptr_t)&__restore_registers;
#elif defined(__x86_64__)
    thread_frame->r15 = 0;
    thread_frame->r14 = 0;
    thread_frame->r13 = 0;
    thread_frame->r12 = 0;
    thread_frame->rbx = 0;
    thread_frame->rbp = 0;
    thread_frame->rip = (uintptr_t)&__restore_registers;
#else
#error "unsupported arch"
#endif
}

void thread_setup_user(thread_t * const thread, uintptr_t stack_pointer, uintptr_t instruction_pointer, uintptr_t page_directory_phyiscal_address) {
    /* we built a fake interrupt frame on the kernel stack */
    uintptr_t kernel_stack = thread->kernel_stack;

    kernel_stack -= sizeof(registers_t);

    registers_t * const regs = (void *)kernel_stack;

    regs->pdir = page_directory_phyiscal_address;

    /* unused */
    regs->isr_num = 0;
    regs->err_num = 0;

    /* user data segment */
    regs->ss = 0x23;

    /* user code segment */
    regs->cs = 0x1B;

#if defined(__i386__)
    /* user data segment */
    regs->ds = 0x23;
    regs->es = 0x23;
    regs->fs = 0x23;
    regs->gs = 0x23;

    /* zero all unused registers */
    regs->edi = 0;
    regs->esi = 0;
    regs->ebp = 0;
    regs->ebx = 0;
    regs->edx = 0;
    regs->ecx = 0;
    regs->eax = 0;

    regs->eip = instruction_pointer;

    /* enable interrupts */
    regs->eflags = 0x200;

    regs->esp = stack_pointer;
#elif defined(__x86_64__)
    /* zero all unused registers */
    regs->rdi = 0;
    regs->rsi = 0;
    regs->rbp = 0;
    regs->rcx = 0;
    regs->rbx = 0;
    regs->rax = 0;
    regs->r8 = 0;
    regs->r9 = 0;
    regs->r10 = 0;
    regs->r11 = 0;
    regs->r12 = 0;
    regs->r13 = 0;
    regs->r14 = 0;
    regs->r15 = 0;

    regs->rip = instruction_pointer;

    /* enable interrupts */
    regs->rflags = 0x200;

    regs->rsp = stack_pointer;
#else
#error "unsupported arch"
#endif

    /* setup a thread frame to jump into the restore part of the
     * interrupt handler */
    kernel_stack -= sizeof(thread_frame_t);
    setup_thread_frame((void *)kernel_stack);

    thread->cpu_state.stack_pointer = kernel_stack;
}

void thread_setup_kernel_task(thread_t * const thread, kernel_thread_entrypoint_f * const entrypoint) {
    /* build a fake stack frame of an interrupt that called __switch_thread directly */
    uintptr_t stack = thread->kernel_stack;

    /* build a fake return to kernel_thread_exit */

    /* arg 0 to kernel_thread_exit: pointer to the thread itself */
    stack -= sizeof(uintptr_t);
    *((uintptr_t *)stack) = (uintptr_t)thread;

    /* arg 0 to the kernel thread entrypoint: pointer to the thread itself
     * this also doubles as the return address for the "return call" to
     * kernel_thread_exit that shall never exit */

    stack -= sizeof(uintptr_t);
    *((uintptr_t *)stack) = (uintptr_t)thread;

    /* kernel thread entrypoint return address */
    stack -= sizeof(uintptr_t);
    *((uintptr_t *)stack) = (uintptr_t)&kernel_thread_exit;

    /* setup a fake interrupt stack frame */
    stack -= sizeof(registers_t);

#if defined(__i386__)
    /* on i386 iret doesn't pop %esp, %ss if returning to the same privilege level,
     * thanks intel */
    stack += sizeof(uintptr_t) * 2;
#endif

    registers_t * const regs = (void *)stack;

    regs->pdir = kernel_directory->physical_address;

    /* unused */
    regs->isr_num = 0;
    regs->err_num = 0;

    /* kernel code segment */
    regs->cs = 0x08;

#if defined(__i386__)
    /* XXX: don't set ss or esp, since that will override other important parts! */

    /* kernel data segment */
    regs->ds = 0x10;
    regs->es = 0x10;
    regs->fs = 0x10;
    regs->gs = 0x10;

    /* zero everything else */
    regs->edi = 0;
    regs->esi = 0;
    regs->ebp = 0;
    regs->ebx = 0;
    regs->edx = 0;
    regs->ecx = 0;
    regs->eax = 0;

    regs->eip = (uintptr_t)entrypoint;

    regs->eflags = 0;
#elif defined(__x86_64__)
    /* kernel data segment */
    regs->ss = 0x10;

    regs->rdi = 0;
    regs->rsi = 0;
    regs->rbp = 0;
    regs->rcx = 0;
    regs->rbx = 0;
    regs->rax = 0;
    regs->r8 = 0;
    regs->r9 = 0;
    regs->r10 = 0;
    regs->r11 = 0;
    regs->r12 = 0;
    regs->r13 = 0;
    regs->r14 = 0;
    regs->r15 = 0;

    regs->isr_num = 0;
    regs->err_num = 0;

    regs->rip = (uintptr_t)entrypoint;
    regs->rflags = 0;

    regs->rsp = ((uintptr_t)regs) - sizeof(registers_t);
#else
#error "unsupported arch"
#endif

    stack -= sizeof(thread_frame_t);
    setup_thread_frame((void *)stack);

    thread->cpu_state.stack_pointer = stack;
}
