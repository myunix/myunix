#!/usr/bin/env -S make -f

# The main kernel module, this is included by Makefile for code simplicity
SRCS_C += \
		  $(LOCAL)assert.c \
		  $(LOCAL)boot.c \
		  $(LOCAL)kernel.c \
		  $(LOCAL)kheap.c \
		  $(LOCAL)kprintf.c \
		  $(LOCAL)list.c \
		  $(LOCAL)pmm.c \
		  $(LOCAL)process.c \
		  $(LOCAL)scheduler.c \
		  $(LOCAL)serial.c \
		  $(LOCAL)string.c \
		  $(LOCAL)syscalls.c \
		  $(LOCAL)thread.c \
		  $(LOCAL)vfs.c \
		  $(LOCAL)vga_console.c
