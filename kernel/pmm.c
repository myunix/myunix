#include <boot.h>
#include <mem/pmm.h>
#include <string.h>
#include <kernel.h>

#include <assert.h>
#include <stdbool.h>

#define PMM_BIT_MAX (sizeof(uint32_t) * 8)

// 1 = used, 0 = free
static uint32_t *pmm;
static size_t pmm_size;

static inline __attribute__((const)) uint32_t pmm_offset_to_block(uintptr_t offset) {
    // XXX: undefined behaviour, don't allow it to happen
    assert(IS_ALIGNED(offset, BLOCK_SIZE));
    return offset / BLOCK_SIZE;
}

static inline __attribute__((const)) uintptr_t pmm_block_to_offset(uint32_t block) {
    return block * BLOCK_SIZE;
}

static inline __attribute__((const)) uint32_t pmm_block_to_index(uint32_t block) {
    return block / PMM_BIT_MAX;
}

static inline __attribute__((const)) uint32_t pmm_block_to_bit(uint32_t block) {
    return (uint32_t)1 << (block % PMM_BIT_MAX);
}

void pmm_init(uintptr_t bitmap_ptr, size_t bitmap_size) {
    memset((void *)bitmap_ptr, 0xFF, bitmap_size);
    pmm = (void *)bitmap_ptr;
    pmm_size = bitmap_size / sizeof(uint32_t);
}

static inline void pmm_set_block(uint32_t block, bool used) {
    const uint32_t index = pmm_block_to_index(block);
    const uint32_t bit = pmm_block_to_bit(block);
    assert(index < pmm_size);
    if (used) {
        pmm[index] |= bit;
    } else {
        pmm[index] &= ~bit;
    }
}

static inline __attribute__((pure)) bool pmm_is_used_block(uint32_t block) {
    uint32_t index = pmm_block_to_index(block);
    uint32_t bit = pmm_block_to_bit(block);
    return (pmm[index] & bit) != 0;
}

static void pmm_set_region_blocks(uint32_t region_start, size_t region_size, bool used) {
    assert(region_size != 0);
    for (size_t i = 0; i < region_size; i++) {
        pmm_set_block(region_start + i, used);
    }
}

static __attribute__((pure)) uint32_t pmm_find_first_free_block(uint32_t start) {
    for (size_t i = start; i < pmm_size; i++) {
        if (pmm[i] == 0xFFFFFFFF) {
            continue;
        }

        for (unsigned int j = 0; j < PMM_BIT_MAX; j++) {
            const uint32_t bit = pmm_block_to_bit(j);
            if ((pmm[i] & bit) == 0) {
                return i * PMM_BIT_MAX + j;
            }
        }
    }

    return 0;
}

static __attribute__((pure)) uint32_t pmm_find_free_region_blocks(size_t size) {
    assert(size != 0);

    for (uint32_t block = pmm_find_first_free_block(0);
         block != 0;
         block = pmm_find_first_free_block(block)) {
        /* check if this region would be big enough */
        const uint32_t region_start = block;
        size_t region_size;
        for (region_size = 0; region_size < size; region_size++) {
            const uint32_t region_block = region_start + region_size;
            const uint32_t index = pmm_block_to_index(region_block);
            const uint32_t bit = pmm_block_to_bit(region_block);
            if (index >= pmm_size) {
                /* we're out of memory */
                return 0;
            }

            if ((pmm[index] & bit) != 0) {
                break;
            }
        }

        if (region_size == size) {
            return region_start;
        }

        /* skip the region we already checked */
        block += region_size;
    }

    /* we're out of memory */
    return 0;
}

static uint32_t pmm_alloc_blocks(size_t size_blocks) {
    uint32_t region_start = pmm_find_free_region_blocks(size_blocks);
    if (region_start == 0) {
        return 0;
    }
    pmm_set_region_blocks(region_start, size_blocks, true);
    return region_start;
}

static void pmm_free_blocks(uint32_t region_start, size_t region_size) {
    for (size_t i = 0; i < region_size; i++) {
        const uint32_t block = region_start + i;
        assert(pmm_is_used_block(block) == true);
    }

    return pmm_set_region_blocks(region_start, region_size, false);
}

/* public functions (that expose "portable" apis) */

void pmm_set(uintptr_t offset, bool used) {
    assert(offset != 0);
    const uint32_t offset_block = pmm_offset_to_block(offset);
    return pmm_set_block(offset_block, used);
}

__attribute__((pure)) bool pmm_is_used(uintptr_t offset) {
    assert(offset != 0);
    const uint32_t offset_block = pmm_offset_to_block(offset);
    return pmm_is_used_block(offset_block);
}

void pmm_set_region(uintptr_t offset, size_t size, bool used) {
    assert(size != 0);
    assert(IS_ALIGNED(size, BLOCK_SIZE));
    const uint32_t offset_block = pmm_offset_to_block(offset);
    const uint32_t size_blocks = pmm_offset_to_block(size);
    return pmm_set_region_blocks(offset_block, size_blocks, used);
}

__attribute__((pure)) uintptr_t pmm_find_first_free(void) {
    const uint32_t ff_block = pmm_find_first_free_block(0);
    return pmm_block_to_offset(ff_block);
}


__attribute__((pure)) uintptr_t pmm_find_free_region(size_t size) {
    assert(size != 0);
    assert(IS_ALIGNED(size, BLOCK_SIZE));
    const uint32_t size_blocks = pmm_offset_to_block(size);
    const uint32_t free_block = pmm_find_free_region_blocks(size_blocks);
    return pmm_block_to_offset(free_block);
}


uintptr_t pmm_alloc(size_t size) {
    assert(size != 0);
    assert(IS_ALIGNED(size, BLOCK_SIZE));
    const uint32_t size_blocks = pmm_offset_to_block(size);
    const uint32_t alloc_block = pmm_alloc_blocks(size_blocks);
    return pmm_block_to_offset(alloc_block);
}


void pmm_free(uintptr_t offset, size_t size) {
    assert(size != 0);
    assert(offset != 0);
    const uint32_t offset_block = pmm_offset_to_block(offset);
    const uint32_t size_blocks = pmm_offset_to_block(size);
    return pmm_free_blocks(offset_block, size_blocks);
}

__attribute__((pure)) size_t pmm_count_free_blocks(void) {
    size_t v = 0;
    for (size_t i = 0; i < pmm_size; i++) {
        if (pmm[i] == 0) {
            v += PMM_BIT_MAX;
            continue;
        }

        for (size_t j = 0; j < PMM_BIT_MAX; j++) {
            if ((pmm[i] & pmm_block_to_bit(j)) == 0) {
                v++;
            }
        }
    }
    return v;
}
