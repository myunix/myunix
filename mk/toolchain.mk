#!/usr/bin/env -S make -f

## Trouble if we allow any of these out
unexport CC
unexport LD
unexport CFLAGS
unexport CPPFLAGS
unexport LDFLAGS
unexport LIBS

# TOOLCHAIN_TARGET: the target triplet of the toolchains target
TOOLCHAIN_TARGET := $(HOST)

# Versions of the components of the toolchain
TOOLCHAIN_BINUTILS_VERSION := 2.32
# TOOLCHAIN_GCC_VERSION: currently supported: 8.3.0, 9.1.0
TOOLCHAIN_GCC_VERSION := 9.1.0
# TOOLCHAIN_TINYCC_VERSION: currently supported: anything you can pass to git-checkout
TOOLCHAIN_TINYCC_VERSION := c092f2ed6102131fc02523db8b87482e7a5b9f10

TOOLCHAIN_DIR ?= toolchain
# TOOLCHAIN_DOWNLOAD: downloaded source code bundles will be placed here
TOOLCHAIN_DOWNLOAD := $(TOOLCHAIN_DIR)/download
# TOOLCHAIN_SRC: sources will be extracted into this directory
TOOLCHAIN_SRC := $(TOOLCHAIN_DIR)/src
# TOOLCHAIN_BUILD: directory in which all builds will be performend
TOOLCHAIN_BUILD := $(TOOLCHAIN_DIR)/build/$(TOOLCHAIN_TARGET)
# TOOLCHAIN_INSTALL: the final destination of the toolchain
TOOLCHAIN_INSTALL := $(TOOLCHAIN_DIR)/prefix/$(TOOLCHAIN_TARGET)
# TOOLCHAIN_PATCHES: location of patches to be applied to gcc and binutils
TOOLCHAIN_PATCHES := $(TOOLCHAIN_DIR)/patches

# The files we need to download (used by toolchain-download-all)
TOOLCHAIN_DOWNLOADS := \
	binutils-$(TOOLCHAIN_BINUTILS_VERSION).tar.xz \
	binutils-$(TOOLCHAIN_BINUTILS_VERSION).tar.xz.sig \
	gcc-$(TOOLCHAIN_GCC_VERSION).tar.xz \
	gcc-$(TOOLCHAIN_GCC_VERSION).tar.xz.sig \
	gnu-keyring.gpg \
	tinycc

# these will be build and installed into $(TOOLCHAIN_INSTALL)
TOOLCHAIN_COMPONENTS := \
	binutils-$(TOOLCHAIN_BINUTILS_VERSION) \
	gcc-$(TOOLCHAIN_GCC_VERSION) \
	tinycc

# TODO: depend on versions using: https://stackoverflow.com/questions/11647859/make-targets-depend-on-variables

.PHONY: toolchain-info
toolchain-info:
	@echo "the toolchain will be build on $(BUILD) targeting $(TOOLCHAIN_TARGET)"
	@echo "binutils: $(TOOLCHAIN_BINUTILS_VERSION)"
	@echo "gcc:      $(TOOLCHAIN_GCC_VERSION)"
	@echo "tinycc:   $(TOOLCHAIN_TINYCC_VERSION)"
	@echo "the toolchain will be installed in \"$(TOOLCHAIN_INSTALL)\""

.PHONY: toolchain-donwload-all
toolchain-download-all: $(addprefix $(TOOLCHAIN_DOWNLOAD)/,$(TOOLCHAIN_DOWNLOADS))

.PHONY: toolchain-all
toolchain-all: $(addprefix $(TOOLCHAIN_BUILD)/,$(addsuffix /.installed,$(TOOLCHAIN_COMPONENTS)))
	@echo "the toolchain has been installed in \"$(TOOLCHAIN_INSTALL)\""
	@echo "Please confirm that it works!"

.PHONY: toolchain-clean
toolchain-clean:
	rm -rf $(TOOLCHAIN_INSTALL)
	rm -rf $(TOOLCHAIN_BUILD)
	rm -rf $(TOOLCHAIN_SRC)

.PHONY: toolchain-download-clean
toolchain-download-clean:
	rm -rf $(TOOLCHAIN_DOWNLOAD)

.PHONY: toolchain-distclean
toolchain-distclean: toolchain-clean toolchain-download-clean

# Inform the user of missing patches
.PHONY: $(TOOLCHAIN_PATCHES)/%.patch
$(TOOLCHAIN_PATCHES)/%.patch:
	$(error $@ is missing, perhaps you changed the version of gcc / binutils and forgot to provided a patch ?)

.PRECIOUS: $(TOOLCHAIN_DOWNLOAD)/%
$(TOOLCHAIN_DOWNLOAD)/binutils-%: | $(TOOLCHAIN_DOWNLOAD)
	curl -f "ftp://ftp.gnu.org/gnu/binutils/$(@F)" -o $@
$(TOOLCHAIN_DOWNLOAD)/gcc-%: | $(TOOLCHAIN_DOWNLOAD)
	# FIXME: not really correct
	curl -f "ftp://ftp.gnu.org/gnu/gcc/gcc-$(TOOLCHAIN_GCC_VERSION)/$(@F)" -o $@
$(TOOLCHAIN_DOWNLOAD)/gnu-keyring.gpg: | $(TOOLCHAIN_DOWNLOAD)
	curl -f "https://ftp.gnu.org/gnu/$(@F)" -o $@
$(TOOLCHAIN_DOWNLOAD)/tinycc:
	rm -rf $@ $@.tmp
	git clone "https://repo.or.cz/tinycc.git" $@.tmp
	mv $@.tmp $@

# verify the signature of every downloaded file
$(TOOLCHAIN_DOWNLOAD)/%-verified.tar.xz: $(TOOLCHAIN_DOWNLOAD)/%.tar.xz $(TOOLCHAIN_DOWNLOAD)/%.tar.xz.sig $(TOOLCHAIN_DOWNLOAD)/gnu-keyring.gpg
	gpg --verify --keyring $(TOOLCHAIN_DOWNLOAD)/gnu-keyring.gpg $(TOOLCHAIN_DOWNLOAD)/$*.tar.xz.sig $<
	ln -s $*.tar.xz $@
	touch $@

# extract
.PRECIOUS: $(TOOLCHAIN_SRC)/%

$(TOOLCHAIN_SRC)/tinycc-%: $(TOOLCHAIN_DOWNLOAD)/tinycc \
	$(TOOLCHAIN_PATCHES)/tinycc-added-mp-option.patch \
	$(TOOLCHAIN_PATCHES)/tinycc-configure-add-disable-bcheck.patch \
	$(TOOLCHAIN_PATCHES)/tinycc-Always-link-libtcc-even-with-nostdlib-since-it-is-ne.patch \
	$(TOOLCHAIN_PATCHES)/tinycc-generate-make-file-unconditionally-of-output-type.patch | $(TOOLCHAIN_SRC)
	rm -rf $@ $@.tmp
	git clone --local $< $@.tmp
	git -C $@.tmp checkout $*
	cat $(TOOLCHAIN_PATCHES)/tinycc-added-mp-option.patch \
		$(TOOLCHAIN_PATCHES)/tinycc-configure-add-disable-bcheck.patch \
		$(TOOLCHAIN_PATCHES)/tinycc-Always-link-libtcc-even-with-nostdlib-since-it-is-ne.patch \
		$(TOOLCHAIN_PATCHES)/tinycc-generate-make-file-unconditionally-of-output-type.patch \
		| git -C $@.tmp apply
	# TODO: here we can apply patches if needed (output bss (see myunix2), add myunix target)
	mv $@.tmp $@

# unpack and patch every source archive
$(TOOLCHAIN_SRC)/%: $(TOOLCHAIN_DOWNLOAD)/%-verified.tar.xz $(TOOLCHAIN_PATCHES)/%.patch | $(TOOLCHAIN_SRC)
	rm -rf $@ $@.tmp
	mkdir -p $@.tmp
	tar -xf $< -C $@.tmp --strip-components=1
	patch -d $@.tmp -p1 < $(TOOLCHAIN_PATCHES)/$*.patch
	mv $@.tmp -T $@
	touch $@

# configure
.PRECIOUS: $(TOOLCHAIN_BUILD)/%

$(TOOLCHAIN_BUILD)/tinycc-%/Makefile: $(TOOLCHAIN_SRC)/tinycc-%
	rm -rf $(@D)
	mkdir -p $(@D)
	cd $(@D) && $(abspath $</configure) \
		--triplet="$(TOOLCHAIN_TARGET)" \
		--prefix="$(abspath $(TOOLCHAIN_INSTALL))" \
		--sysroot="/none" \
		--cpu="$(firstword $(subst -, ,$(TOOLCHAIN_TARGET)))" \
		--config-musl \
		--disable-bcheck
	touch $@

$(TOOLCHAIN_BUILD)/binutils-%/Makefile: $(TOOLCHAIN_SRC)/binutils-%
	rm -rf $(@D)
	mkdir -p $(@D)
	cd $(@D) && $(abspath $</configure) \
		--target="$(TOOLCHAIN_TARGET)" \
		--prefix="$(abspath $(TOOLCHAIN_INSTALL))" \
		--enable-static \
		--disable-shared \
		--with-sysroot \
		--disable-nls \
		--disable-werror \
		--with-system-zlib
	touch $@

$(TOOLCHAIN_BUILD)/gcc-%/Makefile: $(TOOLCHAIN_SRC)/gcc-% $(TOOLCHAIN_BUILD)/binutils-$(TOOLCHAIN_BINUTILS_VERSION)/.installed
	rm -rf $(@D)
	mkdir -p $(@D)
	cd $(@D) && export PATH="$(PATH):$(abspath $(TOOLCHAIN_INSTALL))/bin" && \
		$(abspath $</configure) \
		--target="$(TOOLCHAIN_TARGET)" \
		--prefix="$(abspath $(TOOLCHAIN_INSTALL))" \
		--enable-deterministic-archives \
		--disable-nls \
		--enable-languages=c \
		--without-headers \
		--with-system-zlib
	touch $@

# build
.PRECIOUS: $(TOOLCHAIN_BUILD)/%
$(TOOLCHAIN_BUILD)/gcc-%/.built: $(TOOLCHAIN_BUILD)/gcc-%/Makefile $(TOOLCHAIN_BUILD)/binutils-$(TOOLCHAIN_BINUTILS_VERSION)/.installed
	+$(MAKE) -C $(@D) PATH="$(PATH):$(abspath $(TOOLCHAIN_INSTALL)/bin)" all-gcc all-target-libgcc
	touch $@

$(TOOLCHAIN_BUILD)/binutils-%/.built: $(TOOLCHAIN_BUILD)/binutils-%/Makefile
	+$(MAKE) -C $(@D)
	touch $@

$(TOOLCHAIN_BUILD)/tinycc-%/.built: $(TOOLCHAIN_BUILD)/tinycc-%/Makefile
	+$(MAKE) -C $(@D)
	touch $@

# install
$(TOOLCHAIN_BUILD)/gcc-%/.installed: $(TOOLCHAIN_BUILD)/gcc-%/.built
	+$(MAKE) -C $(@D) install-gcc install-target-libgcc
	touch $@

$(TOOLCHAIN_BUILD)/%/.installed: $(TOOLCHAIN_BUILD)/%/.built
	+$(MAKE) -C $(@D) install
	touch $@

# Glue to allow dependency on $(CC), $(AS) and $(LD)
# FIXME: these should be added as products of the .installed rules
$(TOOLCHAIN_INSTALL)/bin/$(TOOLCHAIN_TARGET)-gcc: $(TOOLCHAIN_BUILD)/gcc-$(TOOLCHAIN_GCC_VERSION)/.installed
	touch $@

$(TOOLCHAIN_INSTALL)/bin/$(TOOLCHAIN_TARGET)-as: $(TOOLCHAIN_BUILD)/binutils-$(TOOLCHAIN_BINUTILS_VERSION)/.installed
	touch $@
$(TOOLCHAIN_INSTALL)/bin/$(TOOLCHAIN_TARGET)-ld: $(TOOLCHAIN_BUILD)/binutils-$(TOOLCHAIN_BINUTILS_VERSION)/.installed
	touch $@

$(TOOLCHAIN_INSTALL)/bin/tcc: $(TOOLCHAIN_BUILD)/tinycc-$(TOOLCHAIN_TINYCC_VERSION)/.installed
	touch $@

# Ignore "broken" dependencies on $(prefix)/lib/gcc/$(target)/$(version)/include ie. headers provided by gcc
$(abspath $(TOOLCHAIN_INSTALL)/lib/gcc/$(TOOLCHAIN_TARGET)/$(TOOLCHAIN_GCC_VERSION)/include)/%: ;

# for cppcheck
$(TOOLCHAIN_INSTALL)/lib/tcc/include: $(TOOLCHAIN_BUILD)/tinycc-$(TOOLCHAIN_TINYCC_VERSION)/.installed
	touch $@

# create directories if needed
$(TOOLCHAIN_DOWNLOAD) $(TOOLCHAIN_SRC) $(TOOLCHAIN_BUILD):
	mkdir -p $@

# You can provide your own toolchain by exporting TOOLCHAIN to the environment
TOOLCHAIN ?= $(TOOLCHAIN_INSTALL)
TOOL_PREFIX ?= $(TOOLCHAIN)/bin/$(HOST)-

ifneq ($(call undefined_or_default,CC),)
	CC := $(TOOL_PREFIX)gcc
endif
ifneq ($(call undefined_or_default,LD),)
	LD := $(TOOL_PREFIX)ld
endif
ifneq ($(call undefined_or_default,AS),)
	AS := $(TOOL_PREFIX)as
endif

# if the user set CC we need it to be an absolute path for order-only dependencies to work
# FIXME: we might want to warn the user if the command doesn't exist
default_value = $(if $(1),$(1),$(2))
resolve_command = $(call default_value,$(shell which $(1)),$(1))

override CC := $(call resolve_command,$(CC))
override LD := $(call resolve_command,$(LD))
override AS := $(call resolve_command,$(AS))

# TODO: Set OBJCOPY and other needed tools (if we ever need them)
