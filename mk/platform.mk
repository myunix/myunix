#!/usr/bin/env -S make -f
# BUILD: the machine we're running on
# HOST: the machine we want to run on

BUILD_OS?=$(shell uname -s | tr '[:upper:]' '[:lower:]')
BUILD_ARCH?=$(shell uname -m)
# BUILD eg. x86_64-linux
BUILD?=$(BUILD_ARCH)-$(BUILD_OS)

HOST_OS?=myunix
HOST_ARCH?=$(BUILD_ARCH)
# HOST eg. x86_64-myunix
HOST?=$(HOST_ARCH)-$(HOST_OS)
