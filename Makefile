#!/usr/bin/env -S make -f
.SUFFIXES:

.DEFAULT: help

.PHONY: help
help:
	@echo "Available targets: all info clean myunix.iso kernel-all"
	@echo "If \$$(TOOLCHAIN) is not set, a toolchain will be build"
	@echo "You might want to set \$$(HOST_ARCH) to one of the supported architectures:"
	@echo "  * x86_64"
	@echo "  * i686"

# Helper macros
include mk/macros.mk
# sets BUILD and HOST
include mk/platform.mk
# sets CC, LD and AS and builds the toolchain if needed
include mk/toolchain.mk

# every $(PART)/Makefile should define the following targets
# $(PART)-all: build everything
# $(PART)-info: print some general information on how $(PART) is configured
# $(PART)-clean: clean build files
# It should not use .DEFAULT, instead the default target should come first
# NOTE: if a rule uses $(CC), $(LD) or $(AS) it should have a order-only dependency on it

PARTS = \
	  kernel

define INCLUDE_PART
# $(info including part $(PART)/Makefile)
include $(PART)/Makefile
endef

$(foreach PART,$(PARTS),$(eval $(INCLUDE_PART)))

.PHONY: all
all: info myunix.iso

.PHONY: info
info:
	@echo "Building myunix system on $(BUILD) for $(HOST)"
	@echo "The following PARTS have been enabled: $(PARTS)"

.PHONY: clean
clean: $(addsuffix -clean,$(PARTS))

myunix.iso: iso/kernel.elf iso/boot/grub/grub.cfg
	@echo "making iso"
	grub-mkrescue -o $@ --product-name "myunix" iso

iso/kernel.elf: kernel/kernel.elf
	@echo "copying iso/kernel.elf"
	cp $< $@

# misc. helpers
.PHONY: lines
lines:
	cloc --vcs=git
